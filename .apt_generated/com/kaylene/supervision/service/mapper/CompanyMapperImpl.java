package com.kaylene.supervision.service.mapper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Generated;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kaylene.supervision.domain.Company;
import com.kaylene.supervision.domain.Site;
import com.kaylene.supervision.domain.User;
import com.kaylene.supervision.service.dto.CompanyDTO;
import com.kaylene.supervision.service.dto.SiteDTO;
import com.kaylene.supervision.service.dto.UserDTO;

@Generated(

    value = "org.mapstruct.ap.MappingProcessor",

    date = "2017-11-29T20:33:29+0000",

    comments = "version: 1.1.0.Final, compiler: Eclipse JDT (IDE) 3.13.50.v20171007-0855, environment: Java 1.8.0_131 (Oracle Corporation)"

)

@Component

public class CompanyMapperImpl implements CompanyMapper {
	
	@Autowired
    private SiteMapper siteMapper;
	
	@Autowired
	UserMapper userMapper;

    @Override

    public CompanyDTO toDto(Company entity) {

        if ( entity == null ) {

            return null;
        }

        CompanyDTO companyDTO = new CompanyDTO();

        companyDTO.setId( entity.getId() );

        companyDTO.setZabbixLogin( entity.getZabbixLogin() );
        
        companyDTO.setZabbixPassword(entity.getZabbixPassword());

        companyDTO.setName( entity.getName() );

        companyDTO.setWebsite( entity.getWebsite() );

        companyDTO.setAddress( entity.getAddress() );

        companyDTO.setPhone( entity.getPhone() );
        
        companyDTO.setMainContact( entity.getMainContact() );
        
        companyDTO.setMainEmail( entity.getMainEmail());
        
        Set<SiteDTO> set = siteSetToSiteDTOSet(entity.getSites());
        
        if ( set != null ) {

            companyDTO.setSites( set );
        }
        
        if (entity.getUsers() != null)
        {
            
            List<User> userList = new ArrayList<User>(entity.getUsers());
            
    		List<UserDTO> userDTOList = userMapper.usersToUserDTOs(userList);
    		
    		Set<UserDTO> userDTOSet = new HashSet<UserDTO>(userDTOList);
    		
    		//companyDTO.setUsers(userDTOSet);
        }

        return companyDTO;
    }

    @Override

    public List<Company> toEntity(List<CompanyDTO> dtoList) {

        if ( dtoList == null ) {

            return null;
        }

        List<Company> list = new ArrayList<Company>();

        for ( CompanyDTO companyDTO : dtoList ) {

            list.add( toEntity( companyDTO ) );
        }

        return list;
    }

    @Override

    public List<CompanyDTO> toDto(List<Company> entityList) {

        if ( entityList == null ) {

            return null;
        }

        List<CompanyDTO> list = new ArrayList<CompanyDTO>();

        for ( Company company : entityList ) {

            list.add( toDto( company ) );
        }

        return list;
    }

    @Override

    public Company toEntity(CompanyDTO companyDTO) {

        if ( companyDTO == null ) {

            return null;
        }

        Company company_ = new Company();

        company_.setId( companyDTO.getId() );
        
        company_.setZabbixLogin(companyDTO.getZabbixLogin());
        
        company_.setZabbixPassword(companyDTO.getZabbixPassword());
        
        company_.setSites(toSiteEntities (companyDTO.getSites()));
        
//        company_.setUsers(toUsersEntities(companyDTO.getUsers()));
     

        company_.setName( companyDTO.getName() );

        company_.setWebsite( companyDTO.getWebsite() );

        company_.setAddress( companyDTO.getAddress() );

        company_.setPhone( companyDTO.getPhone() );
        
        company_.setMainContact( companyDTO.getMainContact() );
        
        company_.setMainEmail( companyDTO.getMainEmail() );

        return company_;
    }


	@Override
	public Site toSiteEntity(SiteDTO siteDTO) {
		if ( siteDTO == null ) {

            return null;
        }

        Site site = new Site();

        site.setCompany( fromId( siteDTO.getCompanyId() ) );

        site.setId( siteDTO.getId() );

        site.setZabbixProxyId( siteDTO.getZabbixProxyId() );

        site.setLabel( siteDTO.getLabel() );

        site.setAddress( siteDTO.getAddress() );

        site.setLatitude( siteDTO.getLatitude() );

        site.setLongitude( siteDTO.getLongitude() );

        return site;
	}

	@Override
	public Set<Site> toSiteEntities(Set<SiteDTO> siteDTO) {
		if ( siteDTO == null ) {

            return null;
        }

        Set<Site> set = new HashSet<>();

        for ( SiteDTO siteDTO_ : siteDTO ) {

            set.add( toSiteEntity( siteDTO_ ) );
        }

        return set;
	}
	
	protected Set<SiteDTO> siteSetToSiteDTOSet(Set<Site> set) {

        if ( set == null ) {

            return null;
        }

        Set<SiteDTO> set_ = new HashSet<SiteDTO>();

        for ( Site site : set ) {

            set_.add( siteMapper.toDto( site ) );
        }

        return set_;
    }

	@Override
	public User toUserEntity(UserDTO userDTO) {
		if ( userDTO == null ) {

            return null;
        }

        User user = new User();
        
        user = userMapper.userDTOToUser(userDTO);

        return user;
	}

	@Override
	public Set<User> toUsersEntities(Set<UserDTO> userDTO) {
		if ( userDTO == null ) {

            return null;
        }

        Set<User> set = new HashSet<>();

        for ( UserDTO userDTO_ : userDTO ) {

            set.add( toUserEntity( userDTO_ ) );
        }

        return set;
	}

	
}

