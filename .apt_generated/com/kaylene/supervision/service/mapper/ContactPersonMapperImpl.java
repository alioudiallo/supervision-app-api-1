package com.kaylene.supervision.service.mapper;

import com.kaylene.supervision.domain.Company;

import com.kaylene.supervision.domain.ContactPerson;

import com.kaylene.supervision.service.dto.ContactPersonDTO;

import java.util.ArrayList;

import java.util.List;

import javax.annotation.Generated;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;

@Generated(

    value = "org.mapstruct.ap.MappingProcessor",

    date = "2017-11-29T20:33:29+0000",

    comments = "version: 1.1.0.Final, compiler: Eclipse JDT (IDE) 3.13.50.v20171007-0855, environment: Java 1.8.0_131 (Oracle Corporation)"

)

@Component

public class ContactPersonMapperImpl implements ContactPersonMapper {

    @Autowired

    private CompanyMapper companyMapper;

    @Override

    public List<ContactPerson> toEntity(List<ContactPersonDTO> dtoList) {

        if ( dtoList == null ) {

            return null;
        }

        List<ContactPerson> list = new ArrayList<ContactPerson>();

        for ( ContactPersonDTO contactPersonDTO : dtoList ) {

            list.add( toEntity( contactPersonDTO ) );
        }

        return list;
    }

    @Override

    public List<ContactPersonDTO> toDto(List<ContactPerson> entityList) {

        if ( entityList == null ) {

            return null;
        }

        List<ContactPersonDTO> list = new ArrayList<ContactPersonDTO>();

        for ( ContactPerson contactPerson : entityList ) {

            list.add( toDto( contactPerson ) );
        }

        return list;
    }

    @Override

    public ContactPersonDTO toDto(ContactPerson contactPerson) {

        if ( contactPerson == null ) {

            return null;
        }

        ContactPersonDTO contactPersonDTO_ = new ContactPersonDTO();

        contactPersonDTO_.setCompanyId( contactPersonCompanyId( contactPerson ) );

        contactPersonDTO_.setId( contactPerson.getId() );

        contactPersonDTO_.setFirtsname( contactPerson.getFirtsname() );

        contactPersonDTO_.setLastname( contactPerson.getLastname() );

        contactPersonDTO_.setEmail( contactPerson.getEmail() );

        contactPersonDTO_.setMobilePhone( contactPerson.getMobilePhone() );

        contactPersonDTO_.setFixedPhone( contactPerson.getFixedPhone() );

        return contactPersonDTO_;
    }

    @Override

    public ContactPerson toEntity(ContactPersonDTO contactPersonDTO) {

        if ( contactPersonDTO == null ) {

            return null;
        }

        ContactPerson contactPerson_ = new ContactPerson();

        contactPerson_.setCompany( companyMapper.fromId( contactPersonDTO.getCompanyId() ) );

        contactPerson_.setId( contactPersonDTO.getId() );

        contactPerson_.setFirtsname( contactPersonDTO.getFirtsname() );

        contactPerson_.setLastname( contactPersonDTO.getLastname() );

        contactPerson_.setEmail( contactPersonDTO.getEmail() );

        contactPerson_.setMobilePhone( contactPersonDTO.getMobilePhone() );

        contactPerson_.setFixedPhone( contactPersonDTO.getFixedPhone() );

        return contactPerson_;
    }

    private Long contactPersonCompanyId(ContactPerson contactPerson) {

        if ( contactPerson == null ) {

            return null;
        }

        Company company = contactPerson.getCompany();

        if ( company == null ) {

            return null;
        }

        Long id = company.getId();

        if ( id == null ) {

            return null;
        }

        return id;
    }
}

