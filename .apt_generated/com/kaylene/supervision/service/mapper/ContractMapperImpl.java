package com.kaylene.supervision.service.mapper;

import com.kaylene.supervision.domain.Company;

import com.kaylene.supervision.domain.Contract;

import com.kaylene.supervision.domain.Offer;

import com.kaylene.supervision.service.dto.ContractDTO;

import java.util.ArrayList;

import java.util.List;

import javax.annotation.Generated;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;

@Generated(

    value = "org.mapstruct.ap.MappingProcessor",

    date = "2017-11-29T20:33:27+0000",

    comments = "version: 1.1.0.Final, compiler: Eclipse JDT (IDE) 3.13.50.v20171007-0855, environment: Java 1.8.0_131 (Oracle Corporation)"

)

@Component

public class ContractMapperImpl implements ContractMapper {

    @Autowired

    private CompanyMapper companyMapper;

    @Autowired

    private OfferMapper offerMapper;

    @Override

    public List<Contract> toEntity(List<ContractDTO> dtoList) {

        if ( dtoList == null ) {

            return null;
        }

        List<Contract> list = new ArrayList<Contract>();

        for ( ContractDTO contractDTO : dtoList ) {

            list.add( toEntity( contractDTO ) );
        }

        return list;
    }

    @Override

    public List<ContractDTO> toDto(List<Contract> entityList) {

        if ( entityList == null ) {

            return null;
        }

        List<ContractDTO> list = new ArrayList<ContractDTO>();

        for ( Contract contract : entityList ) {

            list.add( toDto( contract ) );
        }

        return list;
    }

    @Override

    public ContractDTO toDto(Contract contract) {

        if ( contract == null ) {

            return null;
        }

        ContractDTO contractDTO_ = new ContractDTO();

        contractDTO_.setOfferId( contractOfferId( contract ) );

        contractDTO_.setCompanyId( contractCompanyId( contract ) );

        contractDTO_.setId( contract.getId() );

        contractDTO_.setCurrentStatus( contract.getCurrentStatus() );

        contractDTO_.setCreateDate( contract.getCreateDate() );

        contractDTO_.setBeginDate( contract.getBeginDate() );

        contractDTO_.setEndDate( contract.getEndDate() );

        return contractDTO_;
    }

    @Override

    public Contract toEntity(ContractDTO contractDTO) {

        if ( contractDTO == null ) {

            return null;
        }

        Contract contract_ = new Contract();

        contract_.setOffer( offerMapper.fromId( contractDTO.getOfferId() ) );

        contract_.setCompany( companyMapper.fromId( contractDTO.getCompanyId() ) );

        contract_.setId( contractDTO.getId() );

        contract_.setCurrentStatus( contractDTO.getCurrentStatus() );

        contract_.setCreateDate( contractDTO.getCreateDate() );

        contract_.setBeginDate( contractDTO.getBeginDate() );

        contract_.setEndDate( contractDTO.getEndDate() );

        return contract_;
    }

    private Long contractOfferId(Contract contract) {

        if ( contract == null ) {

            return null;
        }

        Offer offer = contract.getOffer();

        if ( offer == null ) {

            return null;
        }

        Long id = offer.getId();

        if ( id == null ) {

            return null;
        }

        return id;
    }

    private Long contractCompanyId(Contract contract) {

        if ( contract == null ) {

            return null;
        }

        Company company = contract.getCompany();

        if ( company == null ) {

            return null;
        }

        Long id = company.getId();

        if ( id == null ) {

            return null;
        }

        return id;
    }
}

