package com.kaylene.supervision.service.mapper;

import com.kaylene.supervision.domain.Demand;

import com.kaylene.supervision.domain.DemandStory;

import com.kaylene.supervision.domain.User;

import com.kaylene.supervision.service.dto.DemandStoryDTO;

import java.util.ArrayList;

import java.util.List;

import javax.annotation.Generated;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;

@Generated(

    value = "org.mapstruct.ap.MappingProcessor",

    date = "2017-11-29T20:33:29+0000",

    comments = "version: 1.1.0.Final, compiler: Eclipse JDT (IDE) 3.13.50.v20171007-0855, environment: Java 1.8.0_131 (Oracle Corporation)"

)

@Component

public class DemandStoryMapperImpl implements DemandStoryMapper {

    @Autowired

    private UserMapper userMapper;

    @Autowired

    private DemandMapper demandMapper;

    @Override

    public List<DemandStory> toEntity(List<DemandStoryDTO> dtoList) {

        if ( dtoList == null ) {

            return null;
        }

        List<DemandStory> list = new ArrayList<DemandStory>();

        for ( DemandStoryDTO demandStoryDTO : dtoList ) {

            list.add( toEntity( demandStoryDTO ) );
        }

        return list;
    }

    @Override

    public List<DemandStoryDTO> toDto(List<DemandStory> entityList) {

        if ( entityList == null ) {

            return null;
        }

        List<DemandStoryDTO> list = new ArrayList<DemandStoryDTO>();

        for ( DemandStory demandStory : entityList ) {

            list.add( toDto( demandStory ) );
        }

        return list;
    }

    @Override

    public DemandStoryDTO toDto(DemandStory demandStory) {

        if ( demandStory == null ) {

            return null;
        }

        DemandStoryDTO demandStoryDTO_ = new DemandStoryDTO();

        demandStoryDTO_.setUserId( demandStoryAdminId( demandStory ) );

        demandStoryDTO_.setDemandId( demandStoryDemandId( demandStory ) );

        demandStoryDTO_.setId( demandStory.getId() );

        demandStoryDTO_.setCreatedDate( demandStory.getCreatedDate() );

        demandStoryDTO_.setStatus( demandStory.getStatus() );

        return demandStoryDTO_;
    }

    @Override

    public DemandStory toEntity(DemandStoryDTO demandStoryDTO) {

        if ( demandStoryDTO == null ) {

            return null;
        }

        DemandStory demandStory_ = new DemandStory();

        //demandStory_.setAdmin( userMapper.userFromId( demandStoryDTO.getAdminId() ) );
        
        demandStory_.setUserId( demandStoryDTO.getUserId() );
        
        //demandStory_.setDemandId( demandMapper.fromId( demandStoryDTO.getDemandId() ) );

        demandStory_.setDemandId(  demandStoryDTO.getDemandId()  );
        
        demandStory_.setId( demandStoryDTO.getId() );

        demandStory_.setCreatedDate( demandStoryDTO.getCreatedDate() );

        demandStory_.setStatus( demandStoryDTO.getStatus() );

        return demandStory_;
    }

    private Long demandStoryAdminId(DemandStory demandStory) {

        if ( demandStory == null ) {

            return null;
        }

//        User admin = demandStory.getAdmin();
//
//        if ( admin == null ) {
//
//            return null;
//        }

        Long id = demandStory.getUserId();

        if ( id == null ) {

            return null;
        }

        return id;
    }

    private Long demandStoryDemandId(DemandStory demandStory) {

        if ( demandStory == null ) {

            return null;
        }

        Long demandId = demandStory.getDemandId();
//
//        if ( demand == null ) {
//
//            return null;
//        }

        Long id = demandId;

        if ( id == null ) {

            return null;
        }

        return id;
    }
}

