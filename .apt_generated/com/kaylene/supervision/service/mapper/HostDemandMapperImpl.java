package com.kaylene.supervision.service.mapper;

import com.kaylene.supervision.domain.Demand;

import com.kaylene.supervision.domain.HostDemand;

import com.kaylene.supervision.service.dto.HostDemandDTO;

import java.util.ArrayList;

import java.util.List;

import javax.annotation.Generated;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;

@Generated(

    value = "org.mapstruct.ap.MappingProcessor",

    date = "2017-11-29T20:33:28+0000",

    comments = "version: 1.1.0.Final, compiler: Eclipse JDT (IDE) 3.13.50.v20171007-0855, environment: Java 1.8.0_131 (Oracle Corporation)"

)

@Component

public class HostDemandMapperImpl implements HostDemandMapper {

    @Autowired

    private DemandMapper demandMapper;

    @Override

    public List<HostDemand> toEntity(List<HostDemandDTO> dtoList)  {

        if ( dtoList == null ) {

            return null;
        }
       
        List<HostDemand> list = new ArrayList<HostDemand>();
        
        for ( HostDemandDTO hostDemandDTO : dtoList ) {

            list.add( toEntity( hostDemandDTO ) );
        }

        return list;
    }

    @Override

    public List<HostDemandDTO> toDto(List<HostDemand> entityList) {

        if ( entityList == null ) {

            return null;
        }

        List<HostDemandDTO> list = new ArrayList<HostDemandDTO>();
        
        for ( HostDemand hostDemand : entityList ) {

            list.add( toDto( hostDemand ) );
            
        }

        return list;
    }

    @Override

    public HostDemandDTO toDto(HostDemand hostDemand) {

        if ( hostDemand == null ) {

            return null;
        }

        HostDemandDTO hostDemandDTO_ = new HostDemandDTO();

        hostDemandDTO_.setDemandId( hostDemandDemandId( hostDemand ) );

        hostDemandDTO_.setId( hostDemand.getId() );

        hostDemandDTO_.setZabbixId( hostDemand.getZabbixId() );
        
        hostDemandDTO_.setInitialIpAddress(hostDemand.getInitialIpAddress());

        hostDemandDTO_.setLabel( hostDemand.getLabel() );
        
        hostDemandDTO_.setTypeLabel( hostDemand.getTypeLabel() );
        
        hostDemandDTO_.setInitialIpAddress( hostDemand.getInitialIpAddress() );

        hostDemandDTO_.setIpAddress( hostDemand.getIpAddress() );
        
        hostDemandDTO_.setInitialSiteId( hostDemand.getInitialIpAddress() );
        
        hostDemandDTO_.setSiteId( hostDemand.getSiteId() );

        hostDemandDTO_.setType( hostDemand.getType() );

        hostDemandDTO_.setCodeSnmp( hostDemand.getCodeSnmp() );

        hostDemandDTO_.setDateIntegration( hostDemand.getDateIntegration() );
        
        hostDemandDTO_.setIsDoneInZabbix( hostDemand.getIsDoneInZabbix() );

        return hostDemandDTO_;
    }

    @Override

    public HostDemand toEntity(HostDemandDTO hostDemandDTO) {

        if ( hostDemandDTO == null ) {

            return null;
        }

        HostDemand hostDemand_ = new HostDemand();

        hostDemand_.setDemand( demandMapper.fromId( hostDemandDTO.getDemandId() ) );

        hostDemand_.setId( hostDemandDTO.getId() );

        hostDemand_.setZabbixId( hostDemandDTO.getZabbixId() );
        
        hostDemand_.setInitialLabel( hostDemandDTO.getInitialLabel() );

        hostDemand_.setLabel( hostDemandDTO.getLabel() );
        
        hostDemand_.setInitialIpAddress( hostDemandDTO.getInitialIpAddress() );

        hostDemand_.setIpAddress( hostDemandDTO.getIpAddress() );
        
        hostDemand_.setInitialSiteId( hostDemandDTO.getInitialSiteId() );

        hostDemand_.setSiteId( hostDemandDTO.getSiteId() );

        hostDemand_.setType( hostDemandDTO.getType());
        
        hostDemand_.setTypeLabel( hostDemandDTO.getTypeLabel() );

        hostDemand_.setCodeSnmp( hostDemandDTO.getCodeSnmp() );

        hostDemand_.setDateIntegration( hostDemandDTO.getDateIntegration() );
        
        hostDemand_.setIsDoneInZabbix( hostDemandDTO.getIsDoneInZabbix() );

        return hostDemand_;
    }

    private Long hostDemandDemandId(HostDemand hostDemand) {

        if ( hostDemand == null ) {

            return null;
        }

        Demand demand = hostDemand.getDemand();

        if ( demand == null ) {

            return null;
        }

        Long id = demand.getId();

        if ( id == null ) {

            return null;
        }

        return id;
    }
}

