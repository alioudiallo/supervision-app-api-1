CREATE USER 'zabbix'@'%' IDENTIFIED BY 'zabbix';

CREATE DATABASE IF NOT EXISTS kaylene_app CHARACTER SET utf8 COLLATE utf8_bin;
GRANT ALL ON kaylene_app.* TO 'zabbix'@'%';
GRANT ALL ON kaylene_app.* TO 'zabbix'@'localhost';

GRANT ALL ON kaylene_app.* TO 'market'@'%' IDENTIFIED BY 'market';
GRANT ALL ON kaylene_app.* TO 'market'@'localhost' IDENTIFIED BY 'market';