# LAMP Stack : Debian 9 + Apache + PHP-FPM
# ============================================================================================= #

# --------------------------------------- #
# PHP-FPM 7.1 ( FastCGI Process Manager)
# --------------------------------------- #

sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
sudo sh -c 'echo "deb https://packages.sury.org/php/ stretch main" > /etc/apt/sources.list.d/php.list'
sudo apt update

sudo apt install -y php7.1-fpm php7.1-mysql
sudo apt install -y php7.1-apcu php7.1-bcmath php7.1-cli php7.1-curl php7.1-gd php7.1-intl php7.1-mcrypt php7.1-soap php7.1-xml php7.1-zip php7.1-imagick
sudo apt install -y php7.1-common php7.1-json php7.1-mbstring php7.1-opcache php7.1-readline php7.1-xmlrpc icu-doc

# enable start after reboot
sudo update-rc.d php7.1-fpm enable
sudo service php7.1-fpm restart

# check
php -v
php -m

# setup php.init
vim /etc/php/7.1/fpm/php.ini

# Setup php config
# cli
sudo sed -i "s/memory_limit = .*/memory_limit = 1024M/" /etc/php/7.1/cli/php.ini
sudo sed -i "s/;date.timezone =/date.timezone = Etc\/UTC/" /etc/php/7.1/cli/php.ini
# FPM
sudo sed -i "s/memory_limit = .*/memory_limit = 512M/" /etc/php/7.1/fpm/php.ini
sudo sed -i "s/;date.timezone =.*/date.timezone = UTC/" /etc/php/7.1/fpm/php.ini

sudo sed -i "s/upload_max_filesize = .*/upload_max_filesize = 128M/" /etc/php/7.1/fpm/php.ini
sudo sed -i "s/zlib.output_compression = .*/zlib.output_compression = on/" /etc/php/7.1/fpm/php.ini
sudo sed -i "s/max_execution_time = .*/max_execution_time = 18000/" /etc/php/7.1/fpm/php.ini

RUN sed -i "s/upload_max_filesize = .*/upload_max_filesize = 32M/" /etc/php/7.1/cli/php.ini
RUN sed -i "s/post_max_size = .*/post_max_size = 32M/" /etc/php/7.1/cli/php.ini

sudo service php7.1-fpm restart

# --------------------------------------- #
# Apache 2
# --------------------------------------- #
# with php7.1, to active fmp fastcgi, no need libapache2-mod-fastcgi and apache2-mpm-worker or libapache2-mod-fcgid
# https://askubuntu.com/questions/918596/configuring-apache-2-4-with-fastcgi-on-ubuntu-16-04-what-do-i-do-with-php7-0-f
# install
sudo apt install apache2

# enable module & conf
sudo a2dismod mpm_event
sudo a2enmod rewrite proxy proxy_fcgi actions alias mpm_worker
# Enabling conf php7.1-fpm.
sudo a2enconf php7.1-fpm
# add user to group
sudo usermod -a -G www-data ${USER}

# enable start after reboot
sudo update-rc.d apache2 enable
sudo service apache2 restart

# to custom config
sudo vim /etc/apache2/apache2.conf

# bindfs
# http://blog.netgusto.com/solving-web-file-permissions-problem-once-and-for-all/

sudo apt -y install bindfs

chown -Rf ${USER}:${USER} ~/www 
chmod -Rf 770 ~/www

sudo vim /etc/fstab
# bindfs#/var/www/html /home/citizendiop/www fuse force-user=citizendiop,force-group=citizendiop,create-for-user=www-data,create-for-group=www-data,create-with-perms=0770,chgrp-ignore,chown-ignore,chmod-ignore 0 0  

sudo mount ~/www -o nonempty
sudo chown www-data:www-data /var/www
sudo chmod 770 /var/www

# check
/usr/sbin/apache2 -v
echo '<?php phpinfo(); ?>' > ~/www/info.php

# chekc config
sudo apache2ctl configtest


# --------------------------------------- #
#  MySQL >= 5.7.8
# --------------------------------------- #
# root / mysqlsne123
wget -O mysql-apt-config.deb https://dev.mysql.com/get/mysql-apt-config_0.8.9-1_all.deb
sudo dpkg -i mysql-apt-config.deb
sudo apt update

sudo apt install -y mysql-server

# check
mysql --version
sudo service mysql status
# enable start after reboot
sudo update-rc.d mysql enable
sudo service mysql restart

# to change root password
mysql_secure_installation

# test connect
mysql -u root -p

# config
vim /etc/mysql/mysql.conf.d/mysqld.cnf
    bind-address = 0.0.0.0
    max_packet_size = 32M
    innodb_log_file_size = 256M
    innodb_file_per_table=1
    innodb_buffer_pool_size= 1GB

rm /var/lib/mysql/ib_logfile*
sudo service mysql restart

# check
# SHOW GLOBAL VARIABLES LIKE '%innodb_log%';
# show variables like "max_connections";

# data
datadir     = /var/lib/mysql
# logs 
cat /var/log/mysql/error.log

# --------------------------------------- #
# PhpMyAdmin
# --------------------------------------- #
sudo apt -y install phpmyadmin

# check
http://<IP>/phpmyadmin/

# clean
sudo rm -rf /usr/share/phpmyadmin/setup

# --------------------------------------- #
# PHP Composer
# --------------------------------------- #
# Install composer Globally
cd ~
sudo curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
sudo chmod +x /usr/local/bin/composer

# check
composer about


# --------------------------------------- #
#  HTTPS with SSL/TLS 1.2 certificat | 
# --------------------------------------- #
# Certificate Authority (CA) : let's encryp (https://letsencrypt.org)
#  ACME protocol (https://certbot.eff.org/)

# install
# https://certbot.eff.org/#debianstretch-apache

sudo apt-get update
sudo apt -y install python-certbot-apache 

# start
sudo a2enmod ssl

# vhost
sudo certbot --apache -d vps480420.ovh.net
sudo service apache2 restart

# Auto Renewal
sudo certbot renew --dry-run
sudo certbot renew 
# check
sudo crontab -e

