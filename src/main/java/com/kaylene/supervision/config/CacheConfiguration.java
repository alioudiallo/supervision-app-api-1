package com.kaylene.supervision.config;

import io.github.jhipster.config.JHipsterProperties;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
@EnableCaching
@AutoConfigureAfter(value = { MetricsConfiguration.class })
@AutoConfigureBefore(value = { WebConfigurer.class, DatabaseConfiguration.class })
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache("users", jcacheConfiguration);
            cm.createCache("dashboardimages", jcacheConfiguration); 
            cm.createCache(com.kaylene.supervision.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.Company.class.getName(), jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.Company.class.getName() + ".contactPersons", jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.Company.class.getName() + ".contracts", jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.Company.class.getName() + ".sites", jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.ContactPerson.class.getName(), jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.Contract.class.getName(), jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.Contract.class.getName() + ".hosts", jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.HostContract.class.getName(), jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.Offer.class.getName(), jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.Site.class.getName(), jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.Demand.class.getName(), jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.Demand.class.getName() + ".hostsToAdds", jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.DemandStory.class.getName(), jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.HostDemand.class.getName(), jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.Ticket.class.getName(), jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.TicketStory.class.getName(), jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.TicketStory.class.getName() + ".tickets", jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.HostType.class.getName(), jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.DashboardItem.class.getName(), jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.IncidentType.class.getName(), jcacheConfiguration);
            cm.createCache(com.kaylene.supervision.domain.TicketMessage.class.getName(), jcacheConfiguration);
        };
    }
}
