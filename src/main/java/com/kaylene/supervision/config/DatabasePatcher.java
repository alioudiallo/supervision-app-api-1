package com.kaylene.supervision.config;

import com.kaylene.supervision.domain.Company;
import com.kaylene.supervision.domain.Site;
import com.kaylene.supervision.repository.CompanyRepository;
import com.kaylene.supervision.repository.SiteRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * @author Mamadou Lamine NIANG
 **/
@Component
@Slf4j
public class DatabasePatcher {

    private final CompanyRepository companyRepository;
    private final SiteRepository siteRepository;

    public DatabasePatcher(CompanyRepository companyRepository, SiteRepository siteRepository) {
        this.siteRepository = siteRepository;
        this.companyRepository = companyRepository;
    }

    public void addSitesWithoutProxy() {
        boolean patched = false;
        log.info("Sites without proxy database patch");
        for(Company company: companyRepository.findAll()) {
            Optional<Site> siteWithoutProxy = siteRepository.findByCompanyAndZabbixProxyId(company, "0");
            if(!siteWithoutProxy.isPresent()) {
                log.info("Patching company {}", company.getName());
                Site site = new Site();
                site.setCompany(company);
                site.setLabel("Sans Proxy");
                site.setZabbixProxyId("0");
                siteRepository.save(site);
                patched = true;
            }
        }
        if(patched) {
            log.info("Patched Sites without proxy");
        } else {
            log.info("All Good, no patch needed");
        }
    }

}
