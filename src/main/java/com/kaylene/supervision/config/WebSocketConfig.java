package com.kaylene.supervision.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.messaging.handler.invocation.HandlerMethodArgumentResolver;
import org.springframework.messaging.handler.invocation.HandlerMethodReturnValueHandler;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketTransportRegistration;

import java.util.List;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
	

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
    	/**
		 * to enable a simple memory-based message broker to carry the greeting messages
		 * back to the client on destinations prefixed with "/client/websocket".
		 */
    	config.enableSimpleBroker("/client/websocket");//vers le client
    	// when our client will send message through socket, the URL to send will look
    	// approximately like this: http://IP/client/websocket
    	/**
		 * designates the "/app" prefix for messages that are bound
		 * for @MessageMapping-annotated methods. This prefix will be used to define all
		 * the message mappings; for example, "/server/websocket/hello"
		 */
    	config.setApplicationDestinationPrefixes("/server/websocket");//venant du client
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
		// the endpoint, that our clients will use to connect to the server
    	registry.addEndpoint("/kaylene-websocket").setAllowedOrigins("*").withSockJS();

    }


	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addReturnValueHandlers(List<HandlerMethodReturnValueHandler> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void configureClientInboundChannel(ChannelRegistration arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void configureClientOutboundChannel(ChannelRegistration arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean configureMessageConverters(List<MessageConverter> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void configureWebSocketTransport(WebSocketTransportRegistration arg0) {
		// TODO Auto-generated method stub
		
	}

}
