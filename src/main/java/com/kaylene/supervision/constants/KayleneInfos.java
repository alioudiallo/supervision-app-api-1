package com.kaylene.supervision.constants;

public final class KayleneInfos {

	public final static String NEW_DEMAND_EMAIL_SENDER = "nouvelles_demandes@kaylene-group.com";
	public final static String KAYLENE_TICKET_MESSAGE_EMAIL_SENDER = "Fils_Tickets@kaylene-group.com";
	public final static String KAYLENE_SUPPORT_EMAIL = "support@kaylene-group.com";
	public final static Long KAYLENE_COMPANY_ID = (long) 2;

	/* Une lettre determinant la priorité de l'item permet de faciliter le tri */
	public final static String DASHBOARD_ITEM_COLOR_GREY = "e-grey";
	public final static String DASHBOARD_ITEM_COLOR_GREEN = "d-green";
	public final static String DASHBOARD_ITEM_COLOR_ORANGE = "c-orange";
	public final static String DASHBOARD_ITEM_COLOR_RED = "b-red";
	public final static String DASHBOARD_ITEM_COLOR_BLACK = "a-black";

	public final static String SMS_SENDER_NAME = "Supervision";
	public final static String GET_TOKEN_URL = "https://api.orange.com/oauth/v2/token";
	public final static String GET_TOKEN_AUTHORIZATION_HEADER = "Basic cDdXcGpreDQ2QmRUa2R0WkFaRkFXdGgxMDFiak9MRmk6SDd3VkdNSEVDdnNTVUJmUA==";
	public final static String GET_TOKEN_REQUEST_BODY = "grant_type=client_credentials";
	public final static String SEND_SMS_URL = "https://api.orange.com/smsmessaging/v1/outbound/tel%3A%2B22100000000/requests";
	public final static String SMS_SENDER_ADDRESS = "tel:+22100000000";
}
