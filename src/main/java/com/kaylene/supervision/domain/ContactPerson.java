package com.kaylene.supervision.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A ContactPerson.
 */
@Entity
@Table(name = "contact_person")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ContactPerson implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "firtsname", nullable = false)
	private String firtsname;

	@NotNull
	@Column(name = "lastname", nullable = false)
	private String lastname;

	@NotNull
	@Column(name = "email", nullable = false)
	private String email;

	@NotNull
	@Column(name = "mobile_phone", nullable = false)
	private String mobilePhone;

	@Column(name = "fixed_phone")
	private String fixedPhone;

	@ManyToOne
	private Company company;

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirtsname() {
		return firtsname;
	}

	public ContactPerson firtsname(String firtsname) {
		this.firtsname = firtsname;
		return this;
	}

	public void setFirtsname(String firtsname) {
		this.firtsname = firtsname;
	}

	public String getLastname() {
		return lastname;
	}

	public ContactPerson lastname(String lastname) {
		this.lastname = lastname;
		return this;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public ContactPerson email(String email) {
		this.email = email;
		return this;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public ContactPerson mobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
		return this;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getFixedPhone() {
		return fixedPhone;
	}

	public ContactPerson fixedPhone(String fixedPhone) {
		this.fixedPhone = fixedPhone;
		return this;
	}

	public void setFixedPhone(String fixedPhone) {
		this.fixedPhone = fixedPhone;
	}

	public Company getCompany() {
		return company;
	}

	public ContactPerson company(Company company) {
		this.company = company;
		return this;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ContactPerson contactPerson = (ContactPerson) o;
		if (contactPerson.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), contactPerson.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "ContactPerson{" + "id=" + getId() + ", firtsname='" + getFirtsname() + "'" + ", lastname='"
				+ getLastname() + "'" + ", email='" + getEmail() + "'" + ", mobilePhone='" + getMobilePhone() + "'"
				+ ", fixedPhone='" + getFixedPhone() + "'" + "}";
	}
}
