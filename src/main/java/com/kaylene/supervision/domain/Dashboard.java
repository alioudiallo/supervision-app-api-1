package com.kaylene.supervision.domain;

import java.util.List;

/**
 * Dashboard DTO
 */
public class Dashboard {

	private List<DashboardItem> dashBoardItems;

	public List<DashboardItem> getDashBoardItems() {
		return dashBoardItems;
	}

	public void setDashBoardItems(List<DashboardItem> dashBoardItems) {
		this.dashBoardItems = dashBoardItems;
	}

}
