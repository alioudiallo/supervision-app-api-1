package com.kaylene.supervision.domain;

/**
 * A DashboardItem DTO
 */
public class DashboardItem {

	private String hostType;

	private String hostTypeLabel;

	private int numberOfEquipments;

	private int numberOfTickets;

	private String color;

	public String getHostType() {
		return hostType;
	}

	public DashboardItem hostType(String hostType) {
		this.hostType = hostType;
		return this;
	}

	public void setHostType(String hostType) {
		this.hostType = hostType;
	}

	public int getNumberOfEquipments() {
		return numberOfEquipments;
	}

	public DashboardItem numberOfEquipments(int numberOfEquipments) {
		this.numberOfEquipments = numberOfEquipments;
		return this;
	}

	public void setNumberOfEquipments(int numberOfEquipments) {
		this.numberOfEquipments = numberOfEquipments;
	}

	public int getNumberOfTickets() {
		return numberOfTickets;
	}

	public DashboardItem numberOfTickets(int numberOfTickets) {
		this.numberOfTickets = numberOfTickets;
		return this;
	}

	public void setNumberOfTickets(int numberOfTickets) {
		this.numberOfTickets = numberOfTickets;
	}

	public String getColor() {
		return color;
	}

	public DashboardItem color(String color) {
		this.color = color;
		return this;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getHostTypeLabel() {
		return hostTypeLabel;
	}

	public void setHostTypeLabel(String hostTypeLabel) {
		this.hostTypeLabel = hostTypeLabel;
	}

	@Override
	public String toString() {
		return "DashboardItem{" + ", hostType='" + getHostType() + "'" + ", hostTypeLabel='" + getHostTypeLabel() + "'"
				+ ", numberOfEquipments='" + getNumberOfEquipments() + "'" + ", numberOfTickets='"
				+ getNumberOfTickets() + "'" + ", color='" + getColor() + "'" + "}";
	}
}
