package com.kaylene.supervision.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kaylene.supervision.domain.enumeration.DemandStatus;
import com.kaylene.supervision.domain.enumeration.DemandType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * A Demand.
 */
@Entity
@Table(name = "demand")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Demand implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "custom_type")
	private DemandType type;

	@Enumerated(EnumType.STRING)
	@Column(name = "current_status")
	private DemandStatus currentStatus;

	@Column(name = "description")
	private String description;

	@Column(name = "created_date")
	private Instant createdDate;

	@Column(name = "last_modified_date")
	private Instant lastModifiedDate;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "username")
	private String userName;

	@Column(name = "admin_id")
	private Long adminId;

	@Column(name = "admin_name")
	private String adminName;

	@OneToMany(mappedBy = "demand", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<HostDemand> hostsToAdds = new HashSet<>();

	@ManyToOne
	private Company company;

	@ManyToOne
	private Contract contract;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DemandType getType() {
		return type;
	}

	public Demand type(DemandType type) {
		this.type = type;
		return this;
	}

	public void setType(DemandType type) {
		this.type = type;
	}

	public DemandStatus getCurrentStatus() {
		return currentStatus;
	}

	public Demand currentStatus(DemandStatus currentStatus) {
		this.currentStatus = currentStatus;
		return this;
	}

	public void setCurrentStatus(DemandStatus currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getDescription() {
		return description;
	}

	public Demand description(String description) {
		this.description = description;
		return this;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Instant getCreatedDate() {
		return createdDate;
	}

	public Demand createdDate(Instant createdDate) {
		this.createdDate = createdDate;
		return this;
	}

	public void setCreatedDate(Instant createdDate) {
		this.createdDate = createdDate;
	}

	public Instant getLastModifiedDate() {
		return lastModifiedDate;
	}

	public Demand lastModifiedDate(Instant lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
		return this;
	}

	public void setLastModifiedDate(Instant lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public Set<HostDemand> getHostsToAdds() {
		return hostsToAdds;
	}

	public Demand hostsToAdds(Set<HostDemand> hostDemands) {
		this.hostsToAdds = hostDemands;
		return this;
	}

	public Demand addHostsToAdd(HostDemand hostDemand) {
		this.hostsToAdds.add(hostDemand);
		hostDemand.setDemand(this);
		return this;
	}

	public Demand removeHostsToAdd(HostDemand hostDemand) {
		this.hostsToAdds.remove(hostDemand);
		hostDemand.setDemand(null);
		return this;
	}

	public void setHostsToAdds(Set<HostDemand> hostDemands) {
		Function<HostDemand, HostDemand> mapper = x -> x.demand(this);
		this.hostsToAdds = hostDemands.stream().map(mapper).collect(Collectors.toSet());
	}

	public Company getCompany() {
		return company;
	}

	public Demand company(Company company) {
		this.company = company;
		return this;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Contract getContract() {
		return contract;
	}

	public Demand contract(Contract contract) {
		this.contract = contract;
		return this;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getAdminId() {
		return adminId;
	}

	public void setAdminId(Long adminId) {
		this.adminId = adminId;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Demand demand = (Demand) o;
		if (demand.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), demand.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "Demand{" + "id=" + getId() + ", type='" + getType() + "'" + ", currentStatus='" + getCurrentStatus()
				+ "'" + ", description='" + getDescription() + "'" + ", createdDate='" + getCreatedDate() + "'"
				+ ", lastModifiedDate='" + getLastModifiedDate() + "'" +
				// ", company='" + getCompany().toString() + "'" +
				", hostsToAdds='" + getHostsToAdds().toString() + "'" + "}";
	}
}
