package com.kaylene.supervision.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A HostContract.
 */
@Entity
@Table(name = "host_contract")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class HostContract implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "zabbix_host_id")
	private String zabbixHostId;

	@ManyToOne
	private Contract contract;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getZabbixHostId() {
		return zabbixHostId;
	}

	public HostContract zabbixHostId(String zabbixHostId) {
		this.zabbixHostId = zabbixHostId;
		return this;
	}

	public void setZabbixHostId(String zabbixHostId) {
		this.zabbixHostId = zabbixHostId;
	}

	public Contract getContract() {
		return contract;
	}

	public HostContract contract(Contract contract) {
		this.contract = contract;
		return this;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		HostContract hostContract = (HostContract) o;
		if (hostContract.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), hostContract.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "HostContract{" + "id=" + getId() + ", zabbixHostId='" + getZabbixHostId() + "'" + "}";
	}
}
