package com.kaylene.supervision.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A IncidentType.
 */
@Entity
@Table(name = "incident_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class IncidentType implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "custom_key", unique = true)
	private String key;

	@Column(name = "custom_label", unique = true)
	private String label;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public IncidentType key(String key) {
		this.key = key;
		return this;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getLabel() {
		return label;
	}

	public IncidentType label(String label) {
		this.label = label;
		return this;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		IncidentType incidentType = (IncidentType) o;
		if (incidentType.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), incidentType.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "IncidentType{" + "id=" + getId() + ", key='" + getKey() + "'" + ", label='" + getLabel() + "'" + "}";
	}
}
