package com.kaylene.supervision.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Offer.
 */
@Entity
@Table(name = "offer")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Offer implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "custom_label")
	private String label;

	@Column(name = "max_host")
	private Integer maxHost;

	@Column(name = "beginning_time")
	private Instant beginningTime;

	@Column(name = "ending_time")
	private Instant endingTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public Offer label(String label) {
		this.label = label;
		return this;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Integer getMaxHost() {
		return maxHost;
	}

	public Offer maxHost(Integer maxHost) {
		this.maxHost = maxHost;
		return this;
	}

	public void setMaxHost(Integer maxHost) {
		this.maxHost = maxHost;
	}

	public Instant getBeginningTime() {
		return beginningTime;
	}

	public void setBeginningTime(Instant beginningTime) {
		this.beginningTime = beginningTime;
	}

	public Instant getEndingTime() {
		return endingTime;
	}

	public void setEndingTime(Instant endingTime) {
		this.endingTime = endingTime;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Offer offer = (Offer) o;
		if (offer.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), offer.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "Offer{" + "id=" + getId() + ", label='" + getLabel() + "'" + ", maxHost='" + getMaxHost() + "'" + "}";
	}
}
