package com.kaylene.supervision.domain;

import com.kaylene.supervision.domain.enumeration.Severity;
import com.kaylene.supervision.domain.enumeration.TicketStatus;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Ticket.
 */
@Entity
@Table(name = "ticket")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Ticket implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "incident_type")
	private String incidentType;

	@Column(name = "incident_type_label")
	private String incidentTypeLabel;

	@Column(name = "incident_date")
	private Instant incidentDate;

	@Column(name = "track_id_zabbix")
	private String trackIdZabbix;

	@Column(name = "host_idzabbix")
	private String hostIdzabbix;

	@Column(name = "host_label")
	private String hostLabel;

	@Column(name = "host_type")
	private String hostType;

	@Column(name = "host_type_label")
	private String hostTypeLabel;

	@Column(name = "site_label")
	private String siteLabel;

	@Lob
	@Column(name = "description")
	private String description;

	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private TicketStatus status;

	@Column(name = "close_date")
	private Instant closeDate;

	@Enumerated(EnumType.STRING)
	@Column(name = "severity")
	private Severity severity;

	@Column(name = "created_date")
	private Instant createdDate;

	@Column(name = "modified_date")
	private Instant modifiedDate;

	@Column(name = "admin_id")
	private Long adminId;

	@Column(name = "admin_name")
	private String adminName;

	@Column(name = "modifier_id")
	private Long modifierId;

	@Column(name = "modifier_name")
	private String modifierName;

	@ManyToOne
	private Company company;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIncidentType() {
		return incidentType;
	}

	public Ticket incidentType(String incidentType) {
		this.incidentType = incidentType;
		return this;
	}

	public void setIncidentType(String incidentType) {
		this.incidentType = incidentType;
	}

	public Instant getIncidentDate() {
		return incidentDate;
	}

	public Ticket incidentDate(Instant incidentDate) {
		this.incidentDate = incidentDate;
		return this;
	}

	public void setIncidentDate(Instant incidentDate) {
		this.incidentDate = incidentDate;
	}

	public String getTrackIdZabbix() {
		return trackIdZabbix;
	}

	public Ticket trackIdZabbix(String trackIdZabbix) {
		this.trackIdZabbix = trackIdZabbix;
		return this;
	}

	public void setTrackIdZabbix(String trackIdZabbix) {
		this.trackIdZabbix = trackIdZabbix;
	}

	public String getHostIdzabbix() {
		return hostIdzabbix;
	}

	public Ticket hostIdzabbix(String hostIdzabbix) {
		this.hostIdzabbix = hostIdzabbix;
		return this;
	}

	public void setHostIdzabbix(String hostIdzabbix) {
		this.hostIdzabbix = hostIdzabbix;
	}

	public String getHostLabel() {
		return hostLabel;
	}

	public void setHostLabel(String hostLabel) {
		this.hostLabel = hostLabel;
	}

	public String getSiteLabel() {
		return siteLabel;
	}

	public void setSiteLabel(String siteLabel) {
		this.siteLabel = siteLabel;
	}

	public String getDescription() {
		return description;
	}

	public Ticket description(String description) {
		this.description = description;
		return this;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public TicketStatus getStatus() {
		return status;
	}

	public Ticket status(TicketStatus status) {
		this.status = status;
		return this;
	}

	public void setStatus(TicketStatus status) {
		this.status = status;
	}

	public Instant getCloseDate() {
		return closeDate;
	}

	public Ticket closeDate(Instant closeDate) {
		this.closeDate = closeDate;
		return this;
	}

	public void setCloseDate(Instant closeDate) {
		this.closeDate = closeDate;
	}

	public Severity getSeverity() {
		return severity;
	}

	public Ticket severity(Severity severity) {
		this.severity = severity;
		return this;
	}

	public void setSeverity(Severity severity) {
		this.severity = severity;
	}

	public Instant getCreatedDate() {
		return createdDate;
	}

	public Ticket createdDate(Instant createdDate) {
		this.createdDate = createdDate;
		return this;
	}

	public void setCreatedDate(Instant createdDate) {
		this.createdDate = createdDate;
	}

	public Instant getModifiedDate() {
		return modifiedDate;
	}

	public Ticket modifiedDate(Instant modifiedDate) {
		this.modifiedDate = modifiedDate;
		return this;
	}

	public void setModifiedDate(Instant modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getHostType() {
		return hostType;
	}

	public void setHostType(String hostType) {
		this.hostType = hostType;
	}

	public Long getModifierId() {
		return modifierId;
	}

	public void setModifierId(Long modifierId) {
		this.modifierId = modifierId;
	}

	public String getModifierName() {
		return modifierName;
	}

	public void setModifierName(String modifierName) {
		this.modifierName = modifierName;
	}

	public Long getAdminId() {
		return adminId;
	}

	public void setAdminId(Long adminId) {
		this.adminId = adminId;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	public Company getCompany() {
		return company;
	}

	public Ticket company(Company company) {
		this.company = company;
		return this;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getHostTypeLabel() {
		return hostTypeLabel;
	}

	public void setHostTypeLabel(String hostTypeLabel) {
		this.hostTypeLabel = hostTypeLabel;
	}

	public String getIncidentTypeLabel() {
		return incidentTypeLabel;
	}

	public void setIncidentTypeLabel(String incidentTypeLabel) {
		this.incidentTypeLabel = incidentTypeLabel;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Ticket ticket = (Ticket) o;
		if (ticket.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), ticket.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "Ticket{" + "id=" + getId() + ", incidentType='" + getIncidentType() + "'" + ", incidentDate='"
				+ getIncidentDate() + "'" + ", trackIdZabbix='" + getTrackIdZabbix() + "'" + ", adminId='"
				+ getAdminId() + "'" + ", adminName='" + getAdminName() + "'" + ", hostIdzabbix='" + getHostIdzabbix()
				+ "'" + ", description='" + getDescription() + "'" + ", status='" + getStatus() + "'" + ", closeDate='"
				+ getCloseDate() + "'" + ", severity='" + getSeverity() + "'" + ", createdDate='" + getCreatedDate()
				+ "'" + ", modifiedDate='" + getModifiedDate() + "'" + "}";
	}
}
