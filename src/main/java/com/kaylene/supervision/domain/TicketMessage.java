package com.kaylene.supervision.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A TicketMessage.
 */
@Entity
@Table(name = "ticket_message")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TicketMessage implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "ticket_id")
	private Long ticketId;

	@Column(name = "text")
	private String text;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "user_name")
	private String userName;

	@Column(name = "created_date")
	private Instant createdDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTicketId() {
		return ticketId;
	}

	public TicketMessage ticketId(Long ticketId) {
		this.ticketId = ticketId;
		return this;
	}

	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public String getText() {
		return text;
	}

	public TicketMessage text(String text) {
		this.text = text;
		return this;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Long getUserId() {
		return userId;
	}

	public TicketMessage userId(Long userId) {
		this.userId = userId;
		return this;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public TicketMessage userName(String userName) {
		this.userName = userName;
		return this;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Instant getCreatedDate() {
		return createdDate;
	}

	public TicketMessage createdDate(Instant createdDate) {
		this.createdDate = createdDate;
		return this;
	}

	public void setCreatedDate(Instant createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		TicketMessage ticketMessage = (TicketMessage) o;
		if (ticketMessage.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), ticketMessage.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "TicketMessage{" + "id=" + getId() + ", ticketId='" + getTicketId() + "'" + ", text='" + getText() + "'"
				+ ", userId='" + getUserId() + "'" + ", userName='" + getUserName() + "'" + ", createdDate='"
				+ getCreatedDate() + "'" + "}";
	}
}
