package com.kaylene.supervision.domain;

import com.kaylene.supervision.domain.enumeration.TicketStatus;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A TicketStory.
 */
@Entity
@Table(name = "ticket_story")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TicketStory implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "custom_date")
	private Instant date;

	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private TicketStatus status;

	@Column(name = "admin_id")
	private Long adminId;

	@Column(name = "admin_name")
	private String adminName;

	@ManyToOne
	private Ticket ticket;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Instant getDate() {
		return date;
	}

	public TicketStory date(Instant date) {
		this.date = date;
		return this;
	}

	public void setDate(Instant date) {
		this.date = date;
	}

	public TicketStatus getStatus() {
		return status;
	}

	public TicketStory status(TicketStatus status) {
		this.status = status;
		return this;
	}

	public void setStatus(TicketStatus status) {
		this.status = status;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public TicketStory ticket(Ticket ticket) {
		this.ticket = ticket;
		return this;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public Long getAdminId() {
		return adminId;
	}

	public void setAdminId(Long adminId) {
		this.adminId = adminId;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		TicketStory ticketStory = (TicketStory) o;
		if (ticketStory.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), ticketStory.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "TicketStory{" + "id=" + getId() + ", date='" + getDate() + "'" + ", status='" + getStatus() + "'"
				+ ", ticket='" + getTicket().toString() + "'" + ", adminId='" + getAdminId() + "'" + ", adminName='"
				+ getAdminName() + "'" + "}";
	}
}
