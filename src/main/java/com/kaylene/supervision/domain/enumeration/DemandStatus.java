package com.kaylene.supervision.domain.enumeration;

/**
 * The DemandStatus enumeration.
 */
public enum DemandStatus {
	REQUESTED,

	ACCEPTED,

	REJECTED,

	DONE,

	INCOMPLETE,

	DELETED
}
