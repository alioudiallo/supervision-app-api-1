package com.kaylene.supervision.domain.enumeration;

/**
 * The DemandType enumeration.
 */
public enum DemandType {

	ADD,

	DELETE,

	UPDATE
}
