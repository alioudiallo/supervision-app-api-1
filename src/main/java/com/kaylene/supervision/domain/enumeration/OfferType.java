package com.kaylene.supervision.domain.enumeration;

/**
 * The OfferType enumeration.
 */
public enum OfferType {
    
	BRONZE, 
	
	SILVER, 
	
	GOLD
}
