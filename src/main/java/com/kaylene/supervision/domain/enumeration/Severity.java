package com.kaylene.supervision.domain.enumeration;

/**
 * The Severity enumeration.
 */
public enum Severity {
    MAJOR, MINOR, CRITICAL
}
