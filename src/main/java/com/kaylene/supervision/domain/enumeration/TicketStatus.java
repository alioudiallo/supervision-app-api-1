package com.kaylene.supervision.domain.enumeration;

/**
 * The TicketStatus enumeration.
 */
public enum TicketStatus {
    OPEN, INPROGRESS, CLOSE
}
