package com.kaylene.supervision.logicaldelete;

import com.kaylene.supervision.domain.AbstractAuditingEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.Instant;

/**
 * @author Mouhamad Ndiankho THIAM (thiamouhamadpro@gmail.com)
 * @since 2019/05/04
 */
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@MappedSuperclass
@Data
public class AbstractLogicalDelete extends AbstractAuditingEntity {

    @Column(name = "deleted")
    boolean deleted;

    @Column(name = "date_deleted")
    Instant dateDeleted;

    @Column(name = "user_deleted")
    String userDeleted;


}
