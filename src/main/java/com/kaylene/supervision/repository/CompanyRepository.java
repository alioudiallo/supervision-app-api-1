package com.kaylene.supervision.repository;

import com.kaylene.supervision.domain.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data JPA repository for the Company entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {

	Company findByZabbixLogin(String login);

	List<Company> findByNameIgnoreCase(String name);

	List<Company> findByPhone(String phone);

	List<Company> findByMainEmail(String email);

    List<Company> findAll();

    List<Company> findCompanyBySitesId(Long id);
}
