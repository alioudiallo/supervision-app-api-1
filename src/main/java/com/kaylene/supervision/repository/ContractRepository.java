package com.kaylene.supervision.repository;

import com.kaylene.supervision.domain.Contract;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository for the Contract entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContractRepository extends JpaRepository<Contract, Long> {

	List<Contract> findByCompanyId(Long companyId);

	@Query("SELECT c FROM Contract c WHERE c.currentStatus = 'ONGOING'")
	List<Contract> findOnGoingContracts();

	List<Contract> findAll();

	List<Contract> findContractByCompanyId(Long id);

}
