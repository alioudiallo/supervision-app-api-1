package com.kaylene.supervision.repository;

import com.kaylene.supervision.domain.Demand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository for the Demand entity.
 */
@Repository
public interface DemandRepository extends JpaRepository<Demand, Long> {

	public List<Demand> findByCompanyId(Long id);

	@Query("select dm from Demand dm where dm.company.id = ?1 and dm.currentStatus = 'REQUESTED'")
	public List<Demand> findReqestedDemandByCompanyId(Long companyId);

	public List<Demand> findByHostsToAddsZabbixId(String hostIdzabbix);

}
