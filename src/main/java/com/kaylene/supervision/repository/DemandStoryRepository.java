package com.kaylene.supervision.repository;

import com.kaylene.supervision.domain.DemandStory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data JPA repository for the DemandStory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DemandStoryRepository extends JpaRepository<DemandStory, Long> {

}
