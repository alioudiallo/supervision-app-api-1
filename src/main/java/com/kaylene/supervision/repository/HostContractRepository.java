package com.kaylene.supervision.repository;

import com.kaylene.supervision.domain.HostContract;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data JPA repository for the HostContract entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HostContractRepository extends JpaRepository<HostContract, Long> {

}
