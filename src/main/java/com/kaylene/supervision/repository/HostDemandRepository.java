package com.kaylene.supervision.repository;

import com.kaylene.supervision.domain.HostDemand;
import com.kaylene.supervision.domain.enumeration.DemandStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository for the HostDemand entity.
 */
@Repository
public interface HostDemandRepository extends JpaRepository<HostDemand, Long> {

	List<HostDemand> findByZabbixId(String zabbixId);

	List<HostDemand> findByLabelAndDemandCurrentStatus(String label, DemandStatus status);

	List<HostDemand> findByZabbixIdAndDemandCurrentStatus(String zabbixId, DemandStatus currentStatus);

	List<HostDemand> findByDemandCompanyId(Long id);

	@Query("select hd from HostDemand hd where hd.demand.company.id = ?1 and hd.demand.currentStatus != 'DONE'")
	List<HostDemand> findByCompanyDemandNotDone(Long companyId);

	List<HostDemand> findByDemandCompanyIdAndDemandCurrentStatus(Long companyId, DemandStatus currentStatus);

	List<HostDemand> findBySiteIdAndLabel(Long siteId, String label);

	List<HostDemand> findBySiteIdAndIpAddress(Long siteId, String ipAddress);

	List<HostDemand> findBySiteIdAndIpAddressAndDemandCurrentStatus(Long siteId, String ipAddress,
			DemandStatus currentStatus);

	List<HostDemand> findBySiteIdAndLabelAndDemandCurrentStatus(Long siteId, String ipAddress,
			DemandStatus currentStatus);
}
