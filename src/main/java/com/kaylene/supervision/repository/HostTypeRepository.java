package com.kaylene.supervision.repository;

import com.kaylene.supervision.domain.HostType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data JPA repository for the HostType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HostTypeRepository extends JpaRepository<HostType, Long> {
	
	List<HostType> findAll();
	
	Optional<HostType> findByName(String name);
	
	Optional<HostType> findByLabel(String label);
}
