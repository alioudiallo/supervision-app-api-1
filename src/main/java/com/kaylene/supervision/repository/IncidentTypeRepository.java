package com.kaylene.supervision.repository;

import com.kaylene.supervision.domain.IncidentType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data JPA repository for the IncidentType entity.
 */
@Repository
public interface IncidentTypeRepository extends JpaRepository<IncidentType, Long> {
	
	Optional<IncidentType> findByKey(String key);
	
	Optional<IncidentType> findByLabel(String label);

}
