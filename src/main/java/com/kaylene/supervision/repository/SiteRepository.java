package com.kaylene.supervision.repository;

import com.kaylene.supervision.domain.Company;
import com.kaylene.supervision.domain.Site;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the Site entity.
 */
@Repository
public interface SiteRepository extends JpaRepository<Site, Long> {

	public List<Site> findByCompanyId(Long id);

	public List<Site> findByZabbixProxyId(String id);

	List<Site> findByCompany(Company id);

	public List<Site> findAll();

	Optional<Site> findByCompanyAndZabbixProxyId(Company company, String proxyId);

}
