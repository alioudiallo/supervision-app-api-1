package com.kaylene.supervision.repository;

import com.kaylene.supervision.domain.TicketMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data JPA repository for the TicketMessage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TicketMessageRepository extends JpaRepository<TicketMessage, Long> {

	List<TicketMessage> findByTicketId(Long ticketId);
}
