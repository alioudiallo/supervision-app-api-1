package com.kaylene.supervision.repository;

import com.kaylene.supervision.domain.Ticket;
import com.kaylene.supervision.domain.enumeration.TicketStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the Ticket entity.
 */
@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {

	List<Ticket> findByCompanyId(Long id);

	List<Ticket> findByHostIdzabbix(String hostIdzabbix);

	List<Ticket> findByCompanyIdAndHostTypeAndStatus(Long companyId, String hostTypeName, TicketStatus status);

	List<Ticket> findByIncidentType(String incidentType);

	List<Ticket> findByTrackIdZabbix(String alerteId);

	List<Ticket> findByHostIdzabbixAndStatus(String hostIdzabbix, TicketStatus status);
}
