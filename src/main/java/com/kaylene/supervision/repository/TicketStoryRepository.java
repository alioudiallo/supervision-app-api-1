package com.kaylene.supervision.repository;

import com.kaylene.supervision.domain.TicketStory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the TicketStory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TicketStoryRepository extends JpaRepository<TicketStory, Long> {

//    @Query("select ticket_story from TicketStory ticket_story where ticket_story.admin.login = ?#{principal.username}")
//    List<TicketStory> findByAdminIsCurrentUser();

}
