package com.kaylene.supervision.repository;

import com.kaylene.supervision.domain.User;
import com.kaylene.supervision.logicaldelete.LogicalDeleteRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the User entity.
 */
@Repository
public interface UserRepository extends LogicalDeleteRepository<User> {

	Optional<User> findOneByActivationKey(String activationKey);

	default List<User> findByCompanyId(Long id) {
	    return findAllByCompanyIdAndDeleted(id, false);
    }

    List<User> findAllByCompanyIdAndDeleted(Long id, boolean deleted);

	User findByPhoneNumber(String phone);

	List<User> findAllByActivatedIsFalseAndCreatedDateBefore(Instant dateTime);

	Optional<User> findOneByResetKey(String resetKey);

	Optional<User> findOneByEmailIgnoreCase(String email);

	Optional<User> findOneByLogin(String login);

	User findByLogin(String login);

	@EntityGraph(attributePaths = "authorities")
	User findOneWithAuthoritiesById(Long id);

	@EntityGraph(attributePaths = "authorities")
	@Cacheable(cacheNames = "users")
	Optional<User> findOneWithAuthoritiesByLogin(String login);

	default Page<User> findAllByLoginNot(Pageable pageable, String login) {
	    return findAllByLoginNotAndDeleted(pageable, login, false);
    }

	Page<User> findAllByLoginNotAndDeleted(Pageable pageable, String login, boolean deleted);

	@Override
	void delete(User t);
}
