package com.kaylene.supervision.service;

import com.kaylene.supervision.domain.Company;
import com.kaylene.supervision.domain.Contract;
import com.kaylene.supervision.domain.Offer;
import com.kaylene.supervision.domain.enumeration.ContractStatus;
import com.kaylene.supervision.repository.CompanyRepository;
import com.kaylene.supervision.repository.ContractRepository;
import com.kaylene.supervision.repository.OfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Pattern;

import static com.kaylene.supervision.config.Constants.PHONE_NUMBER_LENGTH;

@Service
public class CompanyService {

	@Autowired
	ContractRepository contractRepository;

	@Autowired
	OfferRepository offerRepository;

	@Autowired
	CompanyRepository companyRepository;

	private static final Pattern PHONE_NUMBER_PATTERN = Pattern.compile("(^[0-9]*$)");

	public Integer getMaximalNumberOfHostsForCompany(Long companyId) {
		List<Contract> companyContracts = contractRepository.findByCompanyId(companyId);
		if (companyContracts.isEmpty()) {
			return 0;
		} else {
			Contract lastContract = companyContracts.get(companyContracts.size() - 1);
			Long offerId = lastContract.getOffer().getId();
			Offer companyOffer = offerRepository.findOne(offerId);
			Integer maximalNumberOfHostsForCompany = companyOffer.getMaxHost();
			return maximalNumberOfHostsForCompany;
		}
	}

	// fonction ajouter pour supprimmer plusieurs company
	public void deleteMultipleCompany(List<Company> ids) {
		companyRepository.delete(ids);
	}

	public boolean isZabbixLoginAlreadyInUse(String zabbixLogin) {
		Company company = companyRepository.findByZabbixLogin(zabbixLogin);
		if (company == null) {
			return false;
		} else {
			return true;
		}
	}

	public Contract getLastOnGoingContract(Long CompanyId) {
		List<Contract> contracts = contractRepository.findByCompanyId(CompanyId);
		if (contracts.isEmpty()) {
			return null;
		}
		Contract lastContract = contracts.get(contracts.size() - 1);
		if (lastContract.getCurrentStatus() == ContractStatus.ONGOING) {
			return lastContract;
		} else {
			return null;
		}
	}

	public boolean isCompanyNameAlreadyInUse(String name) {
		List<Company> companies = companyRepository.findByNameIgnoreCase(name);
		if (companies.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	public boolean isCompanyMainEmailAlreadyInUse(String email) {
		List<Company> companies = companyRepository.findByMainEmail(email);
		if (companies.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	public boolean isPhoneNumberInvalid(String phone) {
		if (!PHONE_NUMBER_PATTERN.matcher(phone).matches()) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isPhoneNumberLengthInvalid(String phone) {
		if (phone.length() != PHONE_NUMBER_LENGTH) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isPhoneNumberAlreadyInUse(String phone) {
		List<Company> companies = companyRepository.findByPhone(phone);
		if (companies.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

}
