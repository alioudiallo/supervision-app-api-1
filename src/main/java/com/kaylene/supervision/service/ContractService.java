package com.kaylene.supervision.service;

import com.kaylene.supervision.domain.Company;
import com.kaylene.supervision.domain.Contract;
import com.kaylene.supervision.domain.enumeration.ContractStatus;
import com.kaylene.supervision.repository.CompanyRepository;
import com.kaylene.supervision.repository.ContractRepository;
import com.kaylene.supervision.repository.UserRepository;
import com.kaylene.supervision.service.dto.ContractDTO;
import com.kaylene.supervision.service.mapper.ContractMapper;
import com.kaylene.supervision.web.rest.ContractResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Service
@Transactional
public class ContractService {

	private final Logger log = LoggerFactory.getLogger(ContractResource.class);

	@Autowired
	UserService userService;

	@Autowired
	MailService mailService;

	@Autowired
	ContractRepository contractRepository;

	@Autowired
	CompanyRepository companyRepository;

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	ContractMapper contractMapper;

	@Scheduled(cron = "0 0 8 * * ?")
	public void resiliateContracts() {
		List<Contract> onGoingContracts = contractRepository.findOnGoingContracts();
		for (Contract contract : onGoingContracts) {
			Instant currentEndDate = contract.getEndDate();
			if (currentEndDate.isBefore(Instant.now())) {
				// status
				contract.setCurrentStatus(ContractStatus.FINISH);
				contractRepository.save(contract);
				// Mail Notif
				Company company = companyRepository.findOne(contract.getCompany().getId());
				mailService.sendFinishingContractEmail(company.getName());
				// Disable users
				userService.disableUserWhenContractExpired(company.getId());
			}
		}
	}

	public boolean thereIsOnGoingContractForCompany(Long companyId) {
		List<Contract> contracts = contractRepository.findByCompanyId(companyId);
		if (contracts.isEmpty())
			return false;
		Contract lastContract = contracts.get(contracts.size() - 1);
		return (lastContract.getCurrentStatus() == ContractStatus.ONGOING) ? true : false;
	}

	public ContractDTO update(ContractDTO contractDTO) {
		log.debug("Servie Method to update Contract : {}", contractDTO);
		Instant currentEndDate = contractDTO.getEndDate();
		// Prolongement de contrat
		if (currentEndDate.isAfter(Instant.now()) && contractDTO.getCurrentStatus() == ContractStatus.FINISH ) {
			contractDTO.setCurrentStatus(ContractStatus.ONGOING);
		}
		Contract contract = contractMapper.toEntity(contractDTO);
		contract = contractRepository.save(contract);
		// reactive users
		userService.reactiveUsersAfterContractExtension(contract.getCompany().getId());
		
		return contractMapper.toDto(contract);
	}	

}
