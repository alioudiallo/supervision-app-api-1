package com.kaylene.supervision.service;

import com.kaylene.supervision.domain.Dashboard;
import com.kaylene.supervision.domain.DashboardItem;
import com.kaylene.supervision.domain.HostType;
import com.kaylene.supervision.domain.Ticket;
import com.kaylene.supervision.domain.enumeration.Severity;
import com.kaylene.supervision.domain.enumeration.TicketStatus;
import com.kaylene.supervision.repository.HostTypeRepository;
import com.kaylene.supervision.repository.UserRepository;
import com.kaylene.supervision.service.others.HostsToBeRendered;
import com.kaylene.supervision.service.others.HostsToBeRenderedService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.kaylene.supervision.constants.KayleneInfos.*;

@Service
@Transactional
public class DashboardService {

	private final Logger log = LoggerFactory.getLogger(DashboardService.class);

	@Autowired
	UserRepository userRepository;

	@Autowired
	SimpMessagingTemplate template;

	@Autowired
	HostTypeRepository hostTypeRepository;

	@Autowired
	HostsToBeRenderedService hostsToBeRenderedService;

	@Autowired
	HostTypeService hostTypeService;

	@Autowired
	TicketService ticketService;

	private List<HostsToBeRendered> hostsToBeRendered = new ArrayList<>();


	private List<DashboardItem> clientDashBoardItems;

	public Dashboard getDashboard(Long companyId) {
		log.debug("Get Dashbord");
		Dashboard dashboard = new Dashboard();
		dashboard.setDashBoardItems(getClientDashboardItems(companyId));
		return dashboard;
	}

	public void updateDashboard(Long companyId) {
		Dashboard dashboard = getDashboard(companyId);
		this.template.convertAndSend("/client/websocket/dashboard/" + companyId, dashboard);
	}
	
	public List<DashboardItem> getClientDashboardItems(Long companyId) {
		log.debug("Get Client Dashbord Items");
		
		this.clientDashBoardItems = new ArrayList<>();
		this.hostsToBeRendered = hostsToBeRenderedService.getHostsToBeRendered(companyId, false);
		List<HostType> allHostTypes = hostTypeRepository.findAll();
		DashboardItem currentDashBoardItem = new DashboardItem();
		for (HostType hostType : allHostTypes) {
			currentDashBoardItem.setHostType(hostType.getName()); // ok
			currentDashBoardItem.setHostTypeLabel(hostType.getLabel()); // ok
			currentDashBoardItem.setNumberOfEquipments(getNumberOfHostsOfGivenType(hostType.getName()));
			currentDashBoardItem.setNumberOfTickets(ticketService.getNumberOfCompanyOpenedTicketsForGivenType(companyId,
					hostType.getName(), TicketStatus.OPEN));
			currentDashBoardItem.setColor(getDashboardItemColor(currentDashBoardItem, companyId));
			this.clientDashBoardItems.add(currentDashBoardItem);
			currentDashBoardItem = new DashboardItem();
		}
		sortClientDashBoardByNumberOfEquipments();
		sortClientDashBoardByColor();
		return this.clientDashBoardItems;
	}

	private int getNumberOfHostsOfGivenType(String hostType) {
		if (this.hostsToBeRendered.isEmpty()) {
			return 0;
		}
		int numberOfHost = 0;
		for (HostsToBeRendered htbr : this.hostsToBeRendered) {
			if (htbr.getType() != null) {
				if (htbr.getType().equals(hostType)) {
					numberOfHost++;
				}
			}
		}
		return numberOfHost;
	}

	private void sortClientDashBoardByNumberOfEquipments() {
		Collections.sort(this.clientDashBoardItems, new Comparator<DashboardItem>() {
			@Override
			public int compare(DashboardItem dashBoardItem1, DashboardItem dashBoardItem2) {
				return new Integer(dashBoardItem2.getNumberOfEquipments())
						.compareTo(dashBoardItem1.getNumberOfEquipments());
			}
		});
	}

	private void sortClientDashBoardByColor() {
		Collections.sort(this.clientDashBoardItems, new Comparator<DashboardItem>() {
			@Override
			public int compare(DashboardItem dashBoardItem1, DashboardItem dashBoardItem2) {
				return dashBoardItem1.getColor().compareTo(dashBoardItem2.getColor());
			}
		});
	}

	private String getDashboardItemColor(DashboardItem dashBoarditem, Long companyId) {
		if (dashBoarditem.getNumberOfEquipments() == 0) {
			return DASHBOARD_ITEM_COLOR_GREY;
		}
		List<Ticket> openedTickets = ticketService.getCompanyOpenedTicketsForGivenType(companyId,
				dashBoarditem.getHostType(), TicketStatus.OPEN);
		if (openedTickets.isEmpty()) {
			return DASHBOARD_ITEM_COLOR_GREEN;
		}
		// if there is at least one ticket, the default color is orange
		String orangeOrRed = DASHBOARD_ITEM_COLOR_ORANGE;
		for (Ticket ticket : openedTickets) {
			if (ticket.getSeverity() == Severity.MAJOR) {
				orangeOrRed = DASHBOARD_ITEM_COLOR_RED;
			}
			if (ticket.getSeverity() == Severity.CRITICAL) {
				return DASHBOARD_ITEM_COLOR_BLACK;
			}
		}
		return orangeOrRed;
	}
}
