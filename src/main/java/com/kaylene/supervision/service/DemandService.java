package com.kaylene.supervision.service;

import com.kaylene.supervision.config.ApplicationProperties;
import com.kaylene.supervision.domain.*;
import com.kaylene.supervision.domain.enumeration.DemandStatus;
import com.kaylene.supervision.domain.enumeration.DemandType;
import com.kaylene.supervision.repository.*;
import com.kaylene.supervision.security.SecurityUtils;
import com.kaylene.supervision.service.dto.DemandDTO;
import com.kaylene.supervision.service.dto.HostDemandDTO;
import com.kaylene.supervision.service.mapper.DemandMapper;
import com.kaylene.supervision.service.mapper.HostDemandMapper;
import com.kaylene.supervision.service.others.Host;
import com.kaylene.supervision.service.others.ZabbixRequests;
import com.kaylene.supervision.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing Demand.
 */
@Service
@Transactional
public class DemandService {

	private final Logger log = LoggerFactory.getLogger(DemandService.class);

	private static final String EntityNAME = "DEMAND";

	@Autowired
	private MailService mailService;

	@Autowired
	UserRepository userRepository;

	@Autowired
	HostDemandRepository hostDemandRepository;

	@Autowired
	HostDemandMapper hostDemandMapper;

	ZabbixRequests zabbixRequests = new ZabbixRequests();

	@Autowired
	HostTypeService hostTypeService;

	@Autowired
	UserService userService;

	@Autowired
	DemandStoryRepository demandStoryRepository;

	@Autowired
	CompanyRepository companyRepository;

	private List<Host> hostsFromZabbix;

	private final DemandRepository demandRepository;

	private final DemandMapper demandMapper;

	private final ApplicationProperties applicationProperties;

	public DemandService(DemandRepository demandRepository, DemandMapper demandMapper,
			ApplicationProperties applicationProperties) {
		this.demandRepository = demandRepository;
		this.demandMapper = demandMapper;
		this.applicationProperties = applicationProperties;
	}

	/**
	 * Save a demand.
	 *
	 * @param demandDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public DemandDTO save(DemandDTO demandDTO) {
		log.debug("Request to save Demand : {}", demandDTO);
		Demand demand = demandMapper.toEntity(demandDTO);
		demand.setCreatedDate(Instant.now());
		User currentUser = userRepository.findByLogin(SecurityUtils.getCurrentUserLogin());
		demand.setUserId(currentUser.getId());
		demand.setUserName(currentUser.getFirstName() + " " + currentUser.getLastName());
		demand = demandRepository.save(demand);
		log.debug(demand.toString());
		if (demand.getType() == DemandType.ADD) {
			mailService.sendAddDemandCreationEmail(demand);
		}
		if (demand.getType() == DemandType.UPDATE) {
			mailService.sendUpdateDemandCreationEmail(demand);
		}
		if (demand.getType() == DemandType.DELETE) {
			mailService.sendDeleteDemandCreationEmail(demand);
		}
		return demandMapper.toDto(demand);
	}

	/**
	 * Update a demand.
	 *
	 * @param demandDTO
	 *            the entity to update
	 * @return the updated entity
	 */
	public DemandDTO update(DemandDTO demandDTO) {
		log.debug("Request to update Demand : {}", demandDTO);
		Demand demand = demandMapper.toEntity(demandDTO);
		demand.setLastModifiedDate(Instant.now());
		User currentUser = userRepository.findByLogin(SecurityUtils.getCurrentUserLogin());
		demand.setAdminId(currentUser.getId());
		demand.setAdminName(currentUser.getFirstName() + " " + currentUser.getLastName());
		demand = demandRepository.save(demand);
		mailService.sendDoneDemandEmail(demand);
		return demandMapper.toDto(demand);
	}

	/**
	 * Get all the demands.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<DemandDTO> findAll(Pageable pageable) {
		log.debug("Request to get all Demands");
		return demandRepository.findAll(pageable).map(demandMapper::toDto);
	}

	public List<Demand> findAllDemandBycompanyId(Long companyId) {
		log.debug("Request to get all Demands");
		return demandRepository.findByCompanyId(companyId);
	}

	/**
	 * Get one demand by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public DemandDTO findOne(Long id) {
		log.debug("Request to get Demand : {}", id);
		Demand demand = demandRepository.findOne(id);
		return demandMapper.toDto(demand);
	}

	/**
	 * Delete the demand by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Demand : {}", id);
		demandRepository.delete(id);
	}

	/**
	 * Delete the multiple demand by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void deleteMultipleDemand(List<Demand> ids) {
		log.debug("Request to delete Demand : {}", ids);
		demandRepository.delete(ids);
	}

	public int getNumberOfDemandsOnHost(String zabbixId) {
		List<Demand> demandsOnHost = demandRepository.findByHostsToAddsZabbixId(zabbixId);
		return demandsOnHost.size();
	}

	public DemandDTO getCheckedDemand(DemandDTO demand, DemandType demandType, boolean exception) {
		if (demand.getCurrentStatus() == DemandStatus.DONE) {
			return demand;
		}
		Company company = companyRepository.findOne(demand.getCompanyId());
		String url = this.applicationProperties.getZabbix().getUrl();
		this.hostsFromZabbix = zabbixRequests.getHosts(url, company.getZabbixLogin(), company.getZabbixPassword());
		if (demand.getHostsToAdds().isEmpty()) {
			return demand;
		}
		for (HostDemandDTO hd : demand.getHostsToAdds()) {
			if (isHostDoneInZabbix(hd, demandType, exception)) {
				hd.setIsDoneInZabbix(true);
			} else {
				hd.setIsDoneInZabbix(false);
			}
		}
		return demand;
	}

	public boolean isHostDoneInZabbix(HostDemandDTO hostDemandDTO, DemandType demandType, boolean exception) {
		String errorMessage;
		String existingNameIpAddress;
		String requestedNameIpAddress;
		HostDemand hostDemand = new HostDemand();
		if (demandType != DemandType.DELETE) {
			String requestedLabel = hostDemandDTO.getLabel();
			String requestedIpAddress = hostDemandDTO.getIpAddress();
			Optional<Host> oneHostFromZabbixOpt = findHostByName(requestedLabel);
			if (requestedLabel != null && !oneHostFromZabbixOpt.isPresent()) {
				if (exception) {
					errorMessage = "Aucun équipement trouvé dans le groupe ZABBIX du client avec le nom \""
							+ requestedLabel + "\", cette demande ne peut être réalisée.";
					throw new BadRequestAlertException(errorMessage, EntityNAME, "nozabbixhostfound");
				}
				return false;
			} else {
				Host oneHostFromZabbix = oneHostFromZabbixOpt.get();
				if (!oneHostFromZabbix.getInterfaces().isEmpty()) {
					existingNameIpAddress = oneHostFromZabbix.getName()
							+ oneHostFromZabbix.getInterfaces().get(0).getIp();
				} else {
					existingNameIpAddress = oneHostFromZabbix.getName();
				}
				requestedNameIpAddress = requestedLabel + requestedIpAddress;
				if (existingNameIpAddress.equals(requestedNameIpAddress)) {
					if (demandType == DemandType.ADD) {
						hostDemandDTO.setZabbixId(oneHostFromZabbix.getHostid());
						hostDemand = hostDemandMapper.toEntity(hostDemandDTO);
						hostDemandRepository.save(hostDemand);
					}
					return true;
				} else {
					if (exception) {
						errorMessage = "L\'adresse IP de l'équipement \"" + requestedLabel
								+ "\" ne correspond pas à celle trouvée dans ZABBIX, cette demande ne peut être réalisée.";
						throw new BadRequestAlertException(errorMessage, EntityNAME,
								"nozabbixhostfoundwithgivenIpaddress");
					}
					return false;
				}
			}
		} else {
			String zabbixIdToDelete = hostDemandDTO.getZabbixId();
			Host oneHostFromZabbix = findHostByZabbixId(zabbixIdToDelete);
			if (oneHostFromZabbix == null) {
				return true;
			} else {
				if (exception) {
					errorMessage = "Un équipement a été trouvé dans le groupe ZABBIX du client avec l\'id \""
							+ zabbixIdToDelete + "\", cette demande ne peut être réalisée.";
					throw new BadRequestAlertException(errorMessage, EntityNAME, "zabbixhostfound");
				}
				return false;
			}
		}
	}

	public void setHostTypeLabelsForHostsInDemand(DemandDTO demandDTO) {
		for (HostDemandDTO hd : demandDTO.getHostsToAdds()) {
			if (hd.getType() != null) {
				hd.setTypeLabel(hostTypeService.getHostTypeLabel(hd.getType()));
			} else {
				hd.setTypeLabel(new String());
			}
		}
	}

	private Optional<Host> findHostByName(String name) {
		return this.hostsFromZabbix.stream().filter(htbr -> htbr.getName().equals(name)).findFirst();
	}

	private Host findHostByZabbixId(String zabbixId) {
		Host host = null;
		for (Host htbr : this.hostsFromZabbix) {
			if (htbr.getHostid().equals(zabbixId)) {
				host = htbr;
				break;
			}
		}
		return host;
	}

	public void saveInDemandStory(DemandDTO demand) {
		DemandStory demandStory = new DemandStory();
		demandStory.setDemandId(demand.getId());
		demandStory.setStatus(demand.getCurrentStatus());
		demandStory.setUserId(userService.findByLogin(SecurityUtils.getCurrentUserLogin()).getId());
		demandStoryRepository.save(demandStory);
	}

	public void saveDeletionInDemandStory(DemandDTO demand) {
		demand.setCurrentStatus(DemandStatus.DELETED);
		saveInDemandStory(demand);
	}
}
