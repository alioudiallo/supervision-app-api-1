package com.kaylene.supervision.service;

import com.kaylene.supervision.domain.Demand;
import com.kaylene.supervision.domain.HostDemand;
import com.kaylene.supervision.domain.enumeration.DemandStatus;
import com.kaylene.supervision.domain.enumeration.DemandType;
import com.kaylene.supervision.repository.DemandRepository;
import com.kaylene.supervision.repository.HostDemandRepository;
import com.kaylene.supervision.service.dto.DemandDTO;
import com.kaylene.supervision.service.dto.HostDemandDTO;
import com.kaylene.supervision.service.mapper.DemandMapper;
import com.kaylene.supervision.service.mapper.HostDemandMapper;
import com.kaylene.supervision.service.others.HostsToBeRendered;
import com.kaylene.supervision.service.others.HostsToBeRenderedService;
import org.apache.commons.validator.routines.InetAddressValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.kaylene.supervision.constants.KayleneInfos.KAYLENE_COMPANY_ID;

/**
 * Service Implementation for managing HostDemand.
 */
@Service
@Transactional
public class HostDemandService {

	private final Logger log = LoggerFactory.getLogger(HostDemandService.class);

	private final HostDemandRepository hostDemandRepository;

	private final HostDemandMapper hostDemandMapper;

	private DemandRepository demandRepository;

	private CompanyService companyService;

	HostsToBeRenderedService hostsToBeRenderedService;

	private List<HostsToBeRendered> companyHosts = new ArrayList<HostsToBeRendered>();

	public HostDemandService(DemandRepository demandRepository, CompanyService companyService,
			HostsToBeRenderedService hostsToBeRenderedService, HostDemandRepository hostDemandRepository,
			HostDemandMapper hostDemandMapper, DemandMapper demandMapper) {
		this.demandRepository = demandRepository;
		this.companyService = companyService;
		this.hostsToBeRenderedService = hostsToBeRenderedService;
		this.hostDemandRepository = hostDemandRepository;
		this.hostDemandMapper = hostDemandMapper;
	}

	/**
	 * Save a hostDemand.
	 *
	 * @param hostDemandDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public HostDemandDTO save(HostDemandDTO hostDemandDTO) {
		log.debug("Request to save HostDemand : {}", hostDemandDTO);
		HostDemand hostDemand = hostDemandMapper.toEntity(hostDemandDTO);
		hostDemand = hostDemandRepository.save(hostDemand);
		return hostDemandMapper.toDto(hostDemand);
	}

	/**
	 * Get all the hostDemands.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<HostDemandDTO> findAll(Pageable pageable) {
		log.debug("Request to get all HostDemands");
		return hostDemandRepository.findAll(pageable).map(hostDemandMapper::toDto);
	}

	/**
	 * Get one hostDemand by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public HostDemandDTO findOne(Long id) {
		log.debug("Request to get HostDemand : {}", id);
		HostDemand hostDemand = hostDemandRepository.findOne(id);
		return hostDemandMapper.toDto(hostDemand);
	}

	/**
	 * Delete the hostDemand by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete HostDemand : {}", id);
		hostDemandRepository.delete(id);
	}

	/**
	 * Get all hosts related to the company which id is given as parameter
	 *
	 * @param id
	 *            the id of the company
	 */
	public List<HostDemandDTO> findByDemandCompanyId(Long id) {
		List<HostDemand> hostsDemand = hostDemandRepository.findByDemandCompanyId(id);
		return hostDemandMapper.toDto(hostsDemand);
	}

	/**
	 * Get all hosts in a not done demand related to the company which id is given
	 * as parameter
	 *
	 * @param id
	 *            the id of the company
	 */
	public List<HostDemandDTO> findByCompanyDemandNotDone(Long id) {
		List<HostDemand> hostsDemand = hostDemandRepository.findByCompanyDemandNotDone(id);
		return hostDemandMapper.toDto(hostsDemand);
	}

	/**
	 * Get a list of hosts in demands where current statuses are done and zabbix
	 * host id is given as a parameter
	 *
	 * @param zabbixId
	 *            the zabbixId of the host
	 */
	public List<HostDemandDTO> findByZabbixIdWhereDemandDone(String zabbixId) {
		List<HostDemand> hosts = hostDemandRepository.findByZabbixIdAndDemandCurrentStatus(zabbixId, DemandStatus.DONE);
		return hostDemandMapper.toDto(hosts);
	}

	public boolean thereAreTwoIdenticalIpAddresses(DemandDTO demandDTO) {
		if (demandDTO.getType() == DemandType.DELETE) {
			return false;
		}
		this.companyHosts = hostsToBeRenderedService.getHostsToBeRendered(demandDTO.getCompanyId(), false);
		if (demandDTO.getType() == DemandType.UPDATE) {
			return isNewIpAddressAlreadyInSite(demandDTO);
		}
		Set<HostDemandDTO> hosts = demandDTO.getHostsToAdds();
		if (hosts.isEmpty()) {
			return false;
		}
		// check duplicated Ip addresses in the demand itself
		List<String> adressesIpEtSites = hosts.stream().map(x -> x.getIpAddress() + x.getSiteId())
				.collect(Collectors.toList());
		int initialSize = adressesIpEtSites.size();
		Set<String> setadressesIpEtSites = new HashSet<>(adressesIpEtSites);
		int finalSize = setadressesIpEtSites.size();
		if (finalSize < initialSize) {
			return true;
		}
		// check among requested demands
		if (isIpAddressInRequestedDemands(demandDTO)) {
			return true;
		}
		// check among existing hosts
		Long currentSiteId;
		String currentIpAddress = new String();
		List<HostsToBeRendered> existingHosts = new ArrayList<>();
		for (HostDemandDTO host : hosts) {
			currentSiteId = host.getSiteId();
			currentIpAddress = host.getIpAddress();
			existingHosts = customFindByIpAddressAndSiteId(demandDTO.getCompanyId(), currentIpAddress, currentSiteId);
			if (!existingHosts.isEmpty()) {
				return true;
			}
		}
		return false;
	}

	public boolean thereAreTwoIdenticalHostNames(DemandDTO demandDTO) {
		if (demandDTO.getType() == DemandType.DELETE) {
			return false;
		}
		if (this.companyHosts.isEmpty()) {
			this.companyHosts = hostsToBeRenderedService.getHostsToBeRendered(demandDTO.getCompanyId(), false);
		}
		if (demandDTO.getType() == DemandType.UPDATE) {
			return isNewLabelAlreadyInSite(demandDTO);
		}
		Set<HostDemandDTO> hosts = demandDTO.getHostsToAdds();
		if (hosts.isEmpty()) {
			return false;
		}
		// check duplicated hostLabel in the demand itself
		List<String> nomsEtSites = hosts.stream().map(x -> x.getLabel() + x.getSiteId()).collect(Collectors.toList());
		int initialSize = nomsEtSites.size();
		Set<String> setNomsEtSites = new HashSet<>(nomsEtSites);
		int finalSize = setNomsEtSites.size();
		if (finalSize < initialSize) {
			return true;
		}
		// check among requested demands
		if (isHostLabelInRequestedDemands(demandDTO)) {
			return true;
		}
		// check among existing hosts
		Long currentSiteId;
		String currentLabel = new String();
		List<HostsToBeRendered> existingHosts = new ArrayList<>();
		for (HostDemandDTO host : hosts) {
			currentSiteId = host.getSiteId();
			currentLabel = host.getLabel();
			existingHosts = customFindByLabelAndSiteId(demandDTO.getCompanyId(), currentLabel, currentSiteId);
			if (!existingHosts.isEmpty()) {
				return true;
			}
		}
		return false;
	}

	public boolean isIntegrationDateBeforeToday(DemandDTO demandDTO) {
		Set<HostDemandDTO> hostsInDemand = demandDTO.getHostsToAdds();
		Instant currentDate = Instant.now();
		LocalDateTime currentDateLdt = LocalDateTime.ofInstant(currentDate, ZoneOffset.UTC);
		LocalDate currentDateLd = currentDateLdt.toLocalDate();
		Instant dateIntegrationSouhaitee;
		for (HostDemandDTO hd : hostsInDemand) {
			dateIntegrationSouhaitee = hd.getDateIntegration();
			LocalDateTime dateIntegrationSouhaiteeLdt = LocalDateTime.ofInstant(dateIntegrationSouhaitee,
					ZoneOffset.UTC);
			LocalDate dateIntegrationSouhaiteeLd = dateIntegrationSouhaiteeLdt.toLocalDate();
			if (dateIntegrationSouhaiteeLd.isBefore(currentDateLd)) {
				return true;
			}
		}
		return false;
	}

	public boolean isOneIpAddressInvalid(DemandDTO demandDTO) {
		Set<HostDemandDTO> hostsInDemand = demandDTO.getHostsToAdds();
		if (hostsInDemand.isEmpty()) {
			return false;
		}
		String currentIpAddress = new String();
		for (HostDemandDTO hd : hostsInDemand) {
			currentIpAddress = hd.getIpAddress();
			if (currentIpAddress != null && currentIpAddress.length() != 0) {
				if (!InetAddressValidator.getInstance().isValid(currentIpAddress)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean thereIsANotValidatedDemandOnHost(DemandDTO demandDTO) {
		Set<HostDemandDTO> hostsInDemand = demandDTO.getHostsToAdds();
		if (hostsInDemand.isEmpty()) {
			return false;
		}
		String currentZabbixId = new String();
		List<Demand> demands = new ArrayList<>();
		Demand lastDemand = new Demand();

		for (HostDemandDTO hd : hostsInDemand) {
			currentZabbixId = hd.getZabbixId();
			demands = demandRepository.findByHostsToAddsZabbixId(currentZabbixId);
			if (demands.isEmpty()) {
				return false;
			}
			lastDemand = demands.get(demands.size() - 1);
			if (lastDemand.getCurrentStatus() != DemandStatus.DONE) {
				return true;
			}
		}
		return false;
	}

	public boolean isNumberOfHostsOk(DemandDTO demandDTO) {
		if (demandDTO.getType() != DemandType.ADD)
			return true;

		Long companyId = demandDTO.getCompanyId();
		Integer maxNumberOfHostsForCompany = companyService.getMaximalNumberOfHostsForCompany(companyId);
		int numberOfHostInDemand = demandDTO.getHostsToAdds().size();
		Integer numberOfCompanyExistingHosts = countHosts(companyId);

		return (numberOfCompanyExistingHosts + numberOfHostInDemand) > maxNumberOfHostsForCompany ? false : true;
	}

	public Integer countHosts(Long companyId) {
		if (this.companyHosts.isEmpty()) {
			this.companyHosts = hostsToBeRenderedService.getHostsToBeRendered(companyId, false);
		}
		return this.companyHosts.size();
	}

	public boolean isNewLabelAlreadyInSite(DemandDTO demandDTO) {
		if (this.companyHosts.isEmpty()) {
			return false;
		}

		if (isHostLabelInRequestedDemands(demandDTO)) {
			return true;
		}

		List<HostsToBeRendered> existingHostWithGivenLabel = new ArrayList<>();
		Set<HostDemandDTO> hostsInDemand = demandDTO.getHostsToAdds();
		for (HostDemandDTO hd : hostsInDemand) {
			existingHostWithGivenLabel = customFindByLabelAndSiteId(demandDTO.getCompanyId(), hd.getLabel(),
					hd.getSiteId());
			if (!existingHostWithGivenLabel.isEmpty()
					&& !existingHostWithGivenLabel.get(0).getHostid().equals(hd.getZabbixId())) {
				return true;
			}
		}
		return false;
	}

	public boolean isNewIpAddressAlreadyInSite(DemandDTO demandDTO) {
		if (this.companyHosts.isEmpty()) {
			return false;
		}

		if (isIpAddressInRequestedDemands(demandDTO)) {
			return true;
		}

		List<HostsToBeRendered> existingHostWithGivenIpAddress = new ArrayList<>();
		Set<HostDemandDTO> hostsInDemand = demandDTO.getHostsToAdds();
		for (HostDemandDTO hd : hostsInDemand) {
			existingHostWithGivenIpAddress = customFindByIpAddressAndSiteId(demandDTO.getCompanyId(), hd.getIpAddress(),
					hd.getSiteId());
			if (!existingHostWithGivenIpAddress.isEmpty()
					&& !existingHostWithGivenIpAddress.get(0).getHostid().equals(hd.getZabbixId())) {
				return true;
			}
		}
		return false;
	}

	public String getHostType(String zabbixId) {
		String hostType = new String();
		List<HostDemand> hosts = hostDemandRepository.findByZabbixIdAndDemandCurrentStatus(zabbixId, DemandStatus.DONE);
		if (hosts.isEmpty()) {
			return hostType;
		}
		HostDemand lastHost = hosts.get(hosts.size() - 1);
		hostType = lastHost.getType();
		return hostType;
	}

	public List<HostsToBeRendered> customFindByLabelAndSiteId(Long companyId, String hostLabel, Long siteId) {
		List<HostsToBeRendered> foundHosts = new ArrayList<HostsToBeRendered>();
		for (HostsToBeRendered htbr : this.companyHosts) {
			if (htbr.getSiteId() != null) {
				if (htbr.getName().equals(hostLabel) && htbr.getSiteId().equals(siteId)) {
					foundHosts.add(htbr);
				}
			}
		}
		return foundHosts;
	}

	public List<HostsToBeRendered> customFindByIpAddressAndSiteId(Long companyId, String ipAddress, Long siteId) {
		List<HostsToBeRendered> foundHosts = new ArrayList<>();
		for (HostsToBeRendered htbr : this.companyHosts) {
			if (!htbr.getInterfaces().isEmpty()) {
				if (htbr.getInterfaces().get(0).getIp().equals(ipAddress) && htbr.getSiteId().equals(siteId)) {
					foundHosts.add(htbr);
				}
			}
		}
		return foundHosts;
	}

	public boolean isIpAddressInRequestedDemands(DemandDTO demandDTO) {
		Set<HostDemandDTO> hosts = demandDTO.getHostsToAdds();
		if (hosts.isEmpty()) {
			return false;
		}
		for (HostDemandDTO hd : hosts) {
			List<HostDemand> hostsInRequestedDemands = hostDemandRepository
					.findBySiteIdAndIpAddressAndDemandCurrentStatus(hd.getSiteId(), hd.getIpAddress(),
							DemandStatus.REQUESTED);
			if (!hostsInRequestedDemands.isEmpty()) {
				return true;
			}
		}
		return false;
	}

	public boolean isHostLabelInRequestedDemands(DemandDTO demandDTO) {
		Set<HostDemandDTO> hosts = demandDTO.getHostsToAdds();
		if (hosts.isEmpty()) {
			return false;
		}
		for (HostDemandDTO hd : hosts) {
			List<HostDemand> hostsInRequestedDemands = hostDemandRepository
					.findBySiteIdAndLabelAndDemandCurrentStatus(hd.getSiteId(), hd.getLabel(), DemandStatus.REQUESTED);
			if (!hostsInRequestedDemands.isEmpty()) {
				return true;
			}
		}
		return false;
	}

	public List<HostsToBeRendered> customFindByType(String hostType) {
		List<HostsToBeRendered> allHosts = hostsToBeRenderedService.getHostsToBeRendered(KAYLENE_COMPANY_ID, true);
		if (allHosts.isEmpty()) {
			return new ArrayList<>();
		}
		List<HostsToBeRendered> foundHosts = new ArrayList<>();
		for (HostsToBeRendered htbr : allHosts) {
			if (htbr.getType() != null) {
				if (htbr.getType().equals(hostType)) {
					foundHosts.add(htbr);
				}
			}
		}
		return foundHosts;
	}

	public boolean isHostTypeReferenced(String hostType) {
		List<HostsToBeRendered> hosts = customFindByType(hostType);
		return hosts.isEmpty() ? false : true;
	}
}
