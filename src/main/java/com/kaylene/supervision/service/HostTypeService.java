package com.kaylene.supervision.service;

import com.kaylene.supervision.domain.HostType;
import com.kaylene.supervision.repository.HostTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class HostTypeService {

	@Autowired
	HostTypeRepository hostTypeRepository;

	/**
	 * TODO delete
	 * 
	 * @deprecated
	 */
	public String getHostTypeLabel(String name) {

		Optional<HostType> hostType = hostTypeRepository.findByName(name);
		if (hostType.isPresent()) {
			return hostType.get().getLabel();
		} else {
			return new String();
		}
	}

	@Cacheable("dashboardimages")
	public byte[] getHostTypeImageByName(String name) {
		Optional<HostType> hostType = hostTypeRepository.findByName(name);
		if (hostType.isPresent()) {
			return hostType.get().getImg();
		} else {
			return new byte[0];
		}
	}

}
