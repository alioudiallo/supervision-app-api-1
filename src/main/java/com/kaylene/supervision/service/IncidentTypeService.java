package com.kaylene.supervision.service;

import com.kaylene.supervision.domain.IncidentType;
import com.kaylene.supervision.repository.IncidentTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IncidentTypeService {

	@Autowired
	IncidentTypeRepository incidentTypeRepository;

	public String getIncidentTypeLabelByKey(String key) {
		Optional<IncidentType> incidentType = incidentTypeRepository.findByKey(key);
		if (incidentType.isPresent()) {
			return incidentType.get().getLabel();
		} else {
			return key;
		}
	}

}
