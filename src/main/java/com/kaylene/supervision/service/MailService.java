package com.kaylene.supervision.service;

import com.kaylene.supervision.config.ApplicationProperties;
import com.kaylene.supervision.domain.*;
import com.kaylene.supervision.domain.enumeration.DemandType;
import com.kaylene.supervision.domain.enumeration.Severity;
import com.kaylene.supervision.repository.CompanyRepository;
import com.kaylene.supervision.repository.SiteRepository;
import com.kaylene.supervision.repository.UserRepository;
import com.kaylene.supervision.web.rest.vm.ManagedUserVM;
import io.github.jhipster.config.JHipsterProperties;
import org.apache.commons.lang3.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import javax.mail.internet.MimeMessage;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

import static com.kaylene.supervision.constants.KayleneInfos.*;

/**
 * Service for sending emails.
 * <p>
 * We use the @Async annotation to send emails asynchronously.
 */
@Service
public class MailService {

	private final Logger log = LoggerFactory.getLogger(MailService.class);

	private static final String USER = "user";

	private static final String BASE_URL = "baseUrl";

	private final JHipsterProperties jHipsterProperties;

	private final JavaMailSender javaMailSender;

	private final MessageSource messageSource;

	private final SpringTemplateEngine templateEngine;

	@Autowired
	private SiteRepository siteRepository;

	@Autowired
	private CompanyRepository companyRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	IncidentTypeService incidentTypeService;

	@Autowired
	private TicketMessageService ticketMessageService;

	private final ApplicationProperties applicationProperties;

	public MailService(JHipsterProperties jHipsterProperties, JavaMailSender javaMailSender,
			MessageSource messageSource, SpringTemplateEngine templateEngine,
			ApplicationProperties applicationProperties) {

		this.jHipsterProperties = jHipsterProperties;
		this.javaMailSender = javaMailSender;
		this.messageSource = messageSource;
		this.templateEngine = templateEngine;
		this.applicationProperties = applicationProperties;
	}

	@Async
	public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
		log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}", isMultipart,
				isHtml, to, subject, content);

		// Prepare message using a Spring helper
		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		try {
			MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
			message.setTo(to);
			message.setFrom(jHipsterProperties.getMail().getFrom());
			message.setSubject(subject);
			message.setText(content, isHtml);
			javaMailSender.send(mimeMessage);
			log.debug("Sent email to User '{}'", to);
		} catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.warn("Email could not be sent to user '{}'", to, e);
			} else {
				log.warn("Email could not be sent to user '{}': {}", to, e.getMessage());
			}
		}
	}

	@Async
	public void sendEmailFromTemplate(User user, String templateName, String titleKey) {
		Locale locale = Locale.forLanguageTag(user.getLangKey());
		Context context = new Context(locale);
		context.setVariable(USER, user);
		context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
		String content = templateEngine.process(templateName, context);
		String subject = messageSource.getMessage(titleKey, null, locale);
		sendEmail(user.getEmail(), subject, content, false, true);

	}

	@Async
	public void sendActivationEmail(User user) {
		log.debug("Sending activation email to '{}'", user.getEmail());
		sendEmailFromTemplate(user, "activationEmail", "email.activation.title");
	}

	@Async
	public void sendCreationEmail(User user) {
		log.debug("Sending creation email to '{}'", user.getEmail());
		sendEmailFromTemplate(user, "creationEmail", "email.activation.title");
	}

	@Async
	public void sendPasswordResetMail(User user) {
		log.debug("Sending password reset email to '{}'", user.getEmail());
		sendEmailFromTemplate(user, "passwordResetEmail", "email.reset.title");
	}

	@Async
	public void customUserCreationEmail(ManagedUserVM userVM)// implémentation personnalisée
	{
		String emailAddress = userVM.getLogin();
		String subject = "Création compte Kaylene Supervision247";
		String newLine = "<br/>";
		String lienAppliSupervision = "<a href=\"" + this.applicationProperties.getFrontend().getUrl() + "\">"
				+ "Kaylene Supervision247" + "</a>";
		String message = "Bonjour cher client," + newLine + "votre compte Supervision247 a été créé avec succès."
				+ newLine + "Vous pouvez vous connecter en suivant le lien ci-après "
				+ "avec les informations de connexion suivantes:" + newLine + "Login: " + userVM.getLogin() + newLine
				+ "Mot de passe: " + userVM.getPassword() + newLine + "Lien: " + lienAppliSupervision;

		sendEmail(emailAddress, subject, message, false, true);
	}

	@Async
	public void sendTicketCreationEmail(Ticket ticket, User user) {
		String emailAddress = user.getEmail();
		if (emailAddress == null) {
			log.error("Aucune adresse e-mail trouvée pour l'utilisateur: ", user);
		} else {
			String subject = createTicketMailSubject(ticket);
			String message = createTicketMailMessage(ticket);
			sendEmail(emailAddress, subject, message, false, true);
		}
	}

	@Async
	public void sendTicketModificationEmail(Ticket ticket) {
		List<String> companyEmailAddresses = getCompanyEmailAddresses(ticket.getCompany().getId());
		if (companyEmailAddresses == null) {
			log.error("Aucune adresse e-mail trouvée pour la compagnie: " + ticket.getCompany().getId());
		} else {
			String subject = createTicketModificationMailSubject(ticket);
			String message = createTicketModificationMailMessage(ticket);
			for (int i = 0; i < companyEmailAddresses.size(); i++) {
				String currentEmailAddress = companyEmailAddresses.get(i);
				sendEmail(currentEmailAddress, subject, message, false, true);
			}
		}
	}

	@Async
	public void sendTicketClosingEmail(Ticket ticket) {
		List<String> companyEmailAddresses = getCompanyEmailAddresses(ticket.getCompany().getId());
		if (companyEmailAddresses == null) {
			log.error("Aucune adresse e-mail trouvée pour la compagnie: " + ticket.getCompany().getId());
		} else {
			String subject = createTicketClosingMailSubject(ticket);
			String message = createTicketClosingMailMessage(ticket);
			for (int i = 0; i < companyEmailAddresses.size(); i++) {
				String currentEmailAddress = companyEmailAddresses.get(i);
				sendEmail(currentEmailAddress, subject, message, false, true);
			}
		}
	}

	@Async
	public void sendDoneDemandEmail(Demand demand) {
		User user = userRepository.findOne(demand.getUserId());
		String userEmailAddress = user.getEmail();
		String subject = "Avis de réalisation de demande";
		String message = createDoneDemandMailMessage(demand);
		sendEmail(userEmailAddress, subject, message, false, true);
	}

	public List<String> getCompanyEmailAddresses(Long companyId) {
		List<User> companyUsers = userRepository.findAllByCompanyIdAndDeleted(companyId, false);
		List<String> emailAddresses = companyUsers.stream()
            .filter(User::getActivated)
            .map(c -> c.getEmail().toLowerCase())
            .collect(Collectors.toList());
		if (emailAddresses.isEmpty()) {
			log.error("Aucune adresse email trouvée pour les admins Kaylene");
			return null;
		} else {
			return emailAddresses;
		}
	}

	// Prend en paramètre un ticket a partir duquel l'objet du mail est créé
	private String createTicketMailSubject(Ticket ticket) {
		String mailSubject = new String();
		Long ticketId = ticket.getId();
		String hostName = ticket.getHostLabel();
		mailSubject = "Alerte incident - Ticket N°" + " " + ticketId + " - Sévérité: "
				+ getTranslatedSeverity(ticket.getSeverity()) + " " + "- Equipement" + " " + hostName;
		return mailSubject;
	}

	// Prend en paramètre un ticket a partir duquel le message du mail est créé
	private String createTicketMailMessage(Ticket ticket) {
		String mailMessage = new String();
		mailMessage = getTicketInformations(ticket);
		return mailMessage;
	}

	// Prend en paramètre un ticket a partir duquel l'objet du mail est créé
	private String createTicketModificationMailSubject(Ticket ticket) {
		String mailSubject = new String();
		Long ticketId = ticket.getId();
		String hostName = ticket.getHostLabel();
		mailSubject = "Avis de modification - Ticket N°" + " " + ticketId + " " + "sur équipement" + " " + hostName;
		return mailSubject;
	}

	// Prend en paramètre un ticket a partir duquel le message du mail est créé
	private String createTicketModificationMailMessage(Ticket ticket) {
		String mailMessage = new String();
		Long ticketId = ticket.getId();
		String hostName = ticket.getHostLabel();
		String newLine = "<br/>";
		mailMessage = "Cher client, le ticket N° " + ticketId + " ouvert sur l'équipement " + hostName
				+ " vient de faire l'objet d'une modification." + newLine + getTicketInformations(ticket);
		return mailMessage;
	}

	// Prend en paramètre un ticket a partir duquel l'objet du mail est créé
	private String createTicketClosingMailSubject(Ticket ticket) {
		String mailSubject = new String();
		Long ticketId = ticket.getId();
		String hostName = ticket.getHostLabel();
		mailSubject = "Avis de clôture - Ticket N°" + " " + ticketId + " " + "sur équipement" + " " + hostName;
		return mailSubject;
	}

	// Prend en paramètre un ticket a partir duquel le message du mail est créé
	private String createTicketClosingMailMessage(Ticket ticket) {
		String mailMessage = new String();
		Long ticketId = ticket.getId();
		String hostName = ticket.getHostLabel();
		String newLine = "<br/>";
		mailMessage = "Cher client, le ticket N° " + ticketId + " ouvert sur l'équipement " + hostName
				+ " vient d'être clôturé" + newLine + getTicketInformations(ticket);
		return mailMessage;
	}

	private String getTicketInformations(Ticket ticket) {
		Long ticketId = ticket.getId();
		String hostName = ticket.getHostLabel();
		String incidentType = incidentTypeService.getIncidentTypeLabelByKey(ticket.getIncidentType());
		String description = ticket.getDescription();
		String newLine = "<br/>";
		String openBold = "<span style=\"font-weight:bold\"/>";
		String closeBold = "</span>";
		String lienTicket = "<a href=\"" + this.applicationProperties.getFrontend().getUrlticketd() + ticket.getId()
				+ "\">" + "Cliquez pour voir le ticket" + "</a>";
		String ticketInfos = openBold + "- N° Ticket: " + closeBold + ticketId + newLine + openBold + "- Sévérité: "
				+ closeBold + getTranslatedSeverity(ticket.getSeverity()) + newLine + openBold + "- Equipement: "
				+ closeBold + hostName + newLine + openBold + "- Type incident: " + closeBold + incidentType + newLine
				+ openBold + "- Description détaillée: " + closeBold + "<pre>" + description + "</pre>" + newLine
				+ lienTicket;
		return ticketInfos;
	}

	public String getTranslatedSeverity(Severity severity) {
		String translatedSeverity = "";
		switch (severity) {
		case MINOR:
			translatedSeverity = "Mineure";
			break;
		case MAJOR:
			translatedSeverity = "Majeure";
			break;
		case CRITICAL:
			translatedSeverity = "Critique";
			break;
		default:
			translatedSeverity = severity.name();
			break;
		}
		return translatedSeverity;
	}

	@Async
	public void sendAddDemandCreationEmail(Demand demand) {

		Company company = companyRepository.findOne(demand.getCompany().getId());
		String clientName = company.getName();
		List<String> kayleneAdminsEmailAddresses = getCompanyEmailAddresses(KAYLENE_COMPANY_ID);
		String currentEmailAddress = new String();
		String newLine = "<br/>";
		boolean isMultipart = false;
		boolean isHtml = true;
		String subject = "Nouvelle demande d’ajout d’équipements à superviser - Demande N° " + demand.getId();
		String bonjourEtDebutTexte = "Bonjour,\n" + newLine
				+ "Une nouvelle demande d’ajout d’équipements à superviser a été saisie par le client " + clientName
				+ ", pour la (les) date(s) d’intégration souhaitée(s) suivante(s): \n" + newLine;
		String finDuTexte = "Veuillez prendre en charge cette demande.";
		String lesDatesDIntegration = getHostsIntegrationDates(demand);

		String content = bonjourEtDebutTexte + lesDatesDIntegration + finDuTexte;

		// Prepare message using a Spring helper
		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		try {
			MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
			message.setFrom(NEW_DEMAND_EMAIL_SENDER);
			message.setSubject(subject);
			message.setText(content, isHtml);

			for (int i = 0; i < kayleneAdminsEmailAddresses.size(); i++) {
				currentEmailAddress = kayleneAdminsEmailAddresses.get(i);
				message.setTo(currentEmailAddress);
				log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
						isMultipart, isHtml, currentEmailAddress, subject, content);
				javaMailSender.send(mimeMessage);
				log.debug("Sent email to '{}'", currentEmailAddress);
			}
		} catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.warn("Email could not be sent to '{}'", currentEmailAddress, e);
			} else {
				log.warn("Email could not be sent to '{}': {}", currentEmailAddress, e.getMessage());
			}
		}
	}

	@Async
	public void sendUpdateDemandCreationEmail(Demand demand) {

		Company company = companyRepository.findOne(demand.getCompany().getId());
		String clientName = company.getName();
		List<String> kayleneAdminsEmailAddresses = getCompanyEmailAddresses(KAYLENE_COMPANY_ID);
		String currentEmailAddress = new String();
		String newLine = "<br/>";
		boolean isMultipart = false;
		boolean isHtml = true;
		String subject = "Nouvelle demande de modification d’équipements - Demande N° " + demand.getId();
		String bonjourEtDebutTexte = "Bonjour,\n" + newLine
				+ "Une nouvelle demande de modification d’équipement a été saisie par le client " + clientName
				+ ", pour la date souhaitée suivante: \n" + newLine;
		String finDuTexte = "Veuillez prendre en charge cette demande.";
		String lesDatesDIntegration = getHostsIntegrationDates(demand);

		String content = bonjourEtDebutTexte + lesDatesDIntegration + finDuTexte;

		// Prepare message using a Spring helper
		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		try {
			MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
			message.setFrom(NEW_DEMAND_EMAIL_SENDER);
			message.setSubject(subject);
			message.setText(content, isHtml);

			for (int i = 0; i < kayleneAdminsEmailAddresses.size(); i++) {
				currentEmailAddress = kayleneAdminsEmailAddresses.get(i);
				message.setTo(currentEmailAddress);
				log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
						isMultipart, isHtml, currentEmailAddress, subject, content);
				javaMailSender.send(mimeMessage);
				log.debug("Sent email to '{}'", currentEmailAddress);
			}
		} catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.warn("Email could not be sent to '{}'", currentEmailAddress, e);
			} else {
				log.warn("Email could not be sent to '{}': {}", currentEmailAddress, e.getMessage());
			}
		}
	}

	@Async
	public void sendDeleteDemandCreationEmail(Demand demand) {

		Company company = companyRepository.findOne(demand.getCompany().getId());
		String clientName = company.getName();
		List<String> kayleneAdminsEmailAddresses = getCompanyEmailAddresses(KAYLENE_COMPANY_ID);
		String currentEmailAddress = new String();
		String newLine = "<br/>";
		boolean isMultipart = false;
		boolean isHtml = true;
		String subject = "Nouvelle demande de suppression - Demande N° " + demand.getId();
		String bonjourEtDebutTexte = "Bonjour,\n" + newLine
				+ "Une nouvelle demande de suppression d'équipements a été saisie par le client " + clientName
				+ " pour l'équipement suivant: " + newLine;
		String finDuTexte = "Veuillez prendre en charge cette demande.";
		// String lesDatesDIntegration = getHostsIntegrationDates(demand);
		String descriptionDuHost = new String();
		for (HostDemand host : demand.getHostsToAdds()) {
			descriptionDuHost = host.getZabbixId() + " " + host.getLabel() + newLine;
		}

		String content = bonjourEtDebutTexte + descriptionDuHost + finDuTexte;

		// Prepare message using a Spring helper
		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		try {
			MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
			message.setFrom(NEW_DEMAND_EMAIL_SENDER);
			message.setSubject(subject);
			message.setText(content, isHtml);

			for (int i = 0; i < kayleneAdminsEmailAddresses.size(); i++) {
				currentEmailAddress = kayleneAdminsEmailAddresses.get(i);
				message.setTo(currentEmailAddress);
				log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
						isMultipart, isHtml, currentEmailAddress, subject, content);
				javaMailSender.send(mimeMessage);
				log.debug("Sent email to '{}'", currentEmailAddress);
			}
		} catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.warn("Email could not be sent to '{}'", currentEmailAddress, e);
			} else {
				log.warn("Email could not be sent to '{}': {}", currentEmailAddress, e.getMessage());
			}
		}
	}

	private String createDoneDemandMailMessage(Demand demand) {
		String mailMessage = new String();
		String newLine = "<br/>";
		mailMessage = "Cher client, la demande que vous avez effectuée concernant les équipements suivants:" + newLine
				+ getHostsNamesInDemand(demand) + " vient d'être réalisée" + newLine;
		return mailMessage;
	}

	private String getHostsIntegrationDates(Demand demand) {
		String integrationDates = new String();
		Set<HostDemand> hostsInDemand = demand.getHostsToAdds();
		String newLine = "<br/>";
		for (HostDemand hd : hostsInDemand) {
			integrationDates = integrationDates + hd.getLabel() + " : " + getFormattedDate(hd.getDateIntegration())
					+ newLine;
		}
		return integrationDates;
	}

	private String getHostsNamesInDemand(Demand demand) {
		String names = new String();
		Set<HostDemand> hostsInDemand = demand.getHostsToAdds();
		String newLine = "<br/>";
		Site currentSite = new Site();
		if (hostsInDemand.isEmpty()) {
			return "";
		}
		for (HostDemand hd : hostsInDemand) {
			if (demand.getType() == DemandType.DELETE) {
				names = hd.getZabbixId() + newLine;
				break;
			}
			if (hd != null) {
				if (hd.getSiteId() == null) {
					names = names + " &bull; " + hd.getLabel() + ", " + hd.getIpAddress() + newLine;
				} else {
					Long currentSiteId = hd.getSiteId();
					currentSite = siteRepository.findOne(currentSiteId);
					names = names + " &bull; " + hd.getLabel() + ", " + hd.getIpAddress() + ", "
							+ currentSite.getLabel() + " " + newLine;
				}
			}
		}
		return names;
	}

	private String getFormattedDate(Instant dateToFormat) {
		if (dateToFormat == null) {
			return new String();
		}
		String formattedDate = new String();
		LocalDateTime dateToFormatLdt = LocalDateTime.ofInstant(dateToFormat, ZoneId.systemDefault());
		int day = dateToFormatLdt.getDayOfMonth();
		int month = dateToFormatLdt.getMonthValue();
		int year = dateToFormatLdt.getYear();
		formattedDate = day + "/" + month + "/" + year;
		return formattedDate;
	}

	private String getFormattedDateTime(Instant dateToFormat) {
		String formattedDate = new String();
		formattedDate = getFormattedDate(dateToFormat);

		LocalDateTime dateToFormatLdt = LocalDateTime.ofInstant(dateToFormat, ZoneId.systemDefault());
		int hour = dateToFormatLdt.getHour();
		int minute = dateToFormatLdt.getMinute();
		formattedDate += " " + hour + ":" + minute;
		return formattedDate;
	}

	// ******************************************************************//
	// TicketMessage emails //
	// ******************************************************************//

	@Async
	public void sendTicketMessageCreationEmail(TicketMessage ticketMessage, Ticket ticket, boolean isAdmin) {
		String emailSender;
		List<String> emailReceivers = new ArrayList<String>();
		String mailObject = "Nouveau message sur le fil de discussion du Ticket N° " + ticket.getId();
		String content = new String();

		if (isAdmin) {
			// User admin = userRepository.findOne(ticketMessage.getUserId());
			// emailSender = admin.getEmail();
			emailSender = KAYLENE_SUPPORT_EMAIL;
			content = getAdminTicketMessageCreationEmailContent(ticketMessage);
			if (ticket.getCompany() != null) {
				emailReceivers = getCompanyEmailAddresses(ticket.getCompany().getId());
			} else {
				log.error("Aucune compagnie trouvée comme destinataire du ticket {} ", ticket.getId());
			}
		} else {
			emailSender = KAYLENE_TICKET_MESSAGE_EMAIL_SENDER;
			content = getClientTicketMessageCreationEmailContent(ticketMessage);
			emailReceivers = getCompanyEmailAddresses(KAYLENE_COMPANY_ID);
		}

		boolean isMultipart = false;
		boolean isHtml = true;

		// Prepare message using a Spring helper
		String email = new String();
		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		try {
			for (String currentEmail : emailReceivers) {
				email = currentEmail;
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
				message.setFrom(emailSender);
				message.setSubject(mailObject);
				message.setText(content, isHtml);

				message.setTo(currentEmail);
				log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
						isMultipart, isHtml, currentEmail, mailObject, content);
				javaMailSender.send(mimeMessage);
				log.debug("Sent email to '{}'", currentEmail);
			}
		} catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.warn("Email could not be sent to '{}'", email, e);
			} else {
				log.warn("Email could not be sent to '{}': {}", email, e.getMessage());
			}
		}
	}

	public String getAdminTicketMessageCreationEmailContent(TicketMessage ticketMessage) {
		String newLine = "<br/>";
		String openPre = "<pre>";
		String closePre = "</pre>";
		User ticketMessageSender = userRepository.findOne(ticketMessage.getUserId());
		String ticketMessageSenderName = ticketMessageSender.getFirstName() + " " + ticketMessageSender.getLastName();
		String content = "Cher client," + newLine + " un nouveau message a été envoyé par " + ticketMessageSenderName
				+ " dans la discussion à propos du ticket numéro " + ticketMessage.getTicketId() + newLine + openPre
				+ "\"" + ticketMessage.getText() + "\"" + closePre + "L'équipe Supervision247." + newLine
				+ getTicketMessageStory(ticketMessage.getTicketId());
		return content;
	}

	public String getClientTicketMessageCreationEmailContent(TicketMessage ticketMessage) {
		String newLine = "<br/>";
		String openPre = "<pre>";
		String closePre = "</pre>";
		User ticketMessageSender = userRepository.findOne(ticketMessage.getUserId());
		Company ticketMessageSenderCompany;
		String companyName = new String();
		if (ticketMessageSender.getCompany() != null) {
			ticketMessageSenderCompany = companyRepository.findOne(ticketMessageSender.getCompany().getId());
			companyName = ticketMessageSenderCompany.getName();
		}
		String ticketMessageSenderName = ticketMessageSender.getFirstName() + " " + ticketMessageSender.getLastName();
		String content = "Un nouveau message a été envoyé par " + companyName + "/" + ticketMessageSenderName + "/"
				+ " " + ticketMessageSender.getEmail() + " à propos du ticket numéro " + ticketMessage.getTicketId()
				+ newLine + openPre + "\"" + ticketMessage.getText() + "\"" + closePre
				+ "Merci de prendre en compte ce message." + getTicketMessageStory(ticketMessage.getTicketId());
		return content;
	}

	private String getTicketMessageStory(Long ticketId) {
		String ticketMessageStory = new String();
		String currentDate = new String();
		String currentName = new String();
		String currentText = new String();
		String currentTicketMessage = new String();
		String newLine = "<br/>";
		String separator = "<hr>";
		String openBold = "<span style=\"font-weight:bold\"/>";
		String closeBold = "</span>";
		String openPre = "<pre style=\"\">";
		String closePre = "</pre>";
		List<TicketMessage> messages = ticketMessageService.getTicketMessagesByTicketId(ticketId);
		sortTicketMessagesByDates(messages);
		// ne pas recuperer le message le plus récent vu qu'il est déja dans le mail.
		Long mostRecentMessageId = messages.get(0).getId();
		messages = messages.stream().filter(message -> !message.getId().equals(mostRecentMessageId))
				.collect(Collectors.toList());

		for (TicketMessage message : messages) {
			currentDate = getFormattedDateTime(message.getCreatedDate());
			currentName = message.getUserName();
			currentText = openPre + message.getText() + closePre;
			currentTicketMessage = separator + currentDate + " " + openBold + currentName + closeBold + " " + newLine
					+ currentText;
			ticketMessageStory += currentTicketMessage;
		}
		return ticketMessageStory;
	}

	private void sortTicketMessagesByDates(List<TicketMessage> messages) {
		Collections.sort(messages, new Comparator<TicketMessage>() {
			@Override
			public int compare(TicketMessage message1, TicketMessage message2) {
				return message2.getCreatedDate().compareTo(message1.getCreatedDate());
			}
		});
	}

	// ******************************************************************//
	// Contract emails //
	// ******************************************************************//
	@Async
	public void sendFinishingContractEmail(String companyName) {
		List<String> companyEmailAddresses = getCompanyEmailAddresses(KAYLENE_COMPANY_ID);
		if (companyEmailAddresses == null) {
			log.error("Aucune adresse e-mail trouvée pour la compagnie: " + KAYLENE_COMPANY_ID);
		} else {
			String subject = "Fin de contrat client Supervision247";
			String message = "Le contrat de " + companyName + " est arrivé à terme.";
			for (int i = 0; i < companyEmailAddresses.size(); i++) {
				String currentEmailAddress = companyEmailAddresses.get(i);
				sendEmail(currentEmailAddress, subject, message, false, true);
			}
		}
	}

	/**
	 * /* Envoie d'mail de modifaication à user à son nouvel addresse mail
	 */
	@Async
	public void customUserResetEmail(ManagedUserVM userVM)// implémentation personnalisée
	{
		String emailAddress = userVM.getNewLogin();
		String subject = "Resiliation de login  compte Kaylene Supervision247";
		String newLine = "<br/>";
		String lienAppliSupervision = "<a href=\"" + this.applicationProperties.getFrontend().getUrl() + "\">"
				+ "Kaylene Supervision247" + "</a>";
		String message = "Bonjour cher client," + newLine + "pour modifier votre login Supervision247 ." + newLine
				+ "Vous pouvez cliquer sur le lien ci-après " + "avec les informations de connexion suivantes:"
				+ newLine + "Login: " + userVM.getNewLogin() + newLine + "Mot de passe: " + userVM.getPassword()
				+ newLine + "Lien: " + lienAppliSupervision;

		sendEmail(emailAddress, subject, message, false, true);
	}

	@Async
	public void sendLoginResetMail(User user, String newlogin) {
		log.debug("Sending login reset email to '{}'", newlogin);
		Locale locale = Locale.forLanguageTag(user.getLangKey());
		Context context = new Context(locale);
		context.setVariable(USER, user);
		context.setVariable("key", user.getResetKey());
		context.setVariable("newlogin", newlogin);
		context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
		String content = templateEngine.process("loginResetEmail", context);
		String subject = messageSource.getMessage("email.resetlogin.title", null, locale);
		sendEmail(newlogin, subject, content, false, true);
	}

}
