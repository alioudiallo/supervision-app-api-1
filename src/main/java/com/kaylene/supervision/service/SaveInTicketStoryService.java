package com.kaylene.supervision.service;

import com.kaylene.supervision.domain.Ticket;
import com.kaylene.supervision.domain.TicketStory;
import com.kaylene.supervision.domain.enumeration.TicketStatus;
import com.kaylene.supervision.repository.TicketStoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class SaveInTicketStoryService {

	private final Logger log = LoggerFactory.getLogger(SaveInTicketStoryService.class);

	@Autowired
	TicketStoryRepository ticketStoryRepository;

	public boolean save(Ticket ticket) {
		TicketStatus ticketStatus = ticket.getStatus();
		Long adminId = ticket.getAdminId();
		String adminName = ticket.getAdminName();
		TicketStory ticketStory = new TicketStory();
		ticketStory.setDate(Instant.now());
		ticketStory.setStatus(ticketStatus);
		ticketStory.setAdminId(adminId);
		ticketStory.setAdminName(adminName);
		ticketStory.setTicket(ticket);
		try {
			ticketStoryRepository.save(ticketStory);
			return true;
		} catch (Exception e) {
			log.error("{}", e.getMessage());
			return false;
		}
	}
}
