package com.kaylene.supervision.service;

import com.kaylene.supervision.domain.TicketMessage;
import com.kaylene.supervision.domain.User;
import com.kaylene.supervision.repository.TicketMessageRepository;
import com.kaylene.supervision.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.kaylene.supervision.constants.KayleneInfos.KAYLENE_COMPANY_ID;

@Service
public class TicketMessageService {

	@Autowired
	TicketMessageRepository ticketMessageRepository;

	@Autowired
	UserRepository userRepository;

	public List<TicketMessage> getTicketMessagesByTicketId(Long ticketId) {
		List<TicketMessage> messages = ticketMessageRepository.findByTicketId(ticketId);
		if (messages.isEmpty()) {
			return new ArrayList<>();
		}
		for (TicketMessage message : messages) {
			User messageSender = userRepository.findOne(message.getUserId());
			if (messageSender != null) {
				message.setUserName(messageSender.getFirstName() + " " + messageSender.getLastName());
			}
		}
		return messages;
	}

	public List<String> getNonAdminUsersEmailOnTicketMessages(Long ticketId) {
		List<TicketMessage> messages = getTicketMessagesByTicketId(ticketId);
		List<Long> usersIds = new ArrayList<>();
		for (TicketMessage message : messages) {
			usersIds.add(message.getUserId());
		}
		Set<Long> usersIdsUnDuplicated = new HashSet<>(usersIds);
		User currentUser = new User();
		String currentEmail = new String();
		List<String> usersEmails = new ArrayList<>();
		for (Long userId : usersIdsUnDuplicated) {
			currentUser = userRepository.findOne(userId);
			currentEmail = currentUser.getEmail();
			if (currentUser.getCompany() != null && (!currentUser.getCompany().getId().equals(KAYLENE_COMPANY_ID))) {
				usersEmails.add(currentEmail);
			}
		}
		return usersEmails;
	}

}
