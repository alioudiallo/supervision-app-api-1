package com.kaylene.supervision.service.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the Company entity.
 */
public class CompanyDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	@NotNull
	private String zabbixLogin;

	private String zabbixPassword;

	@NotNull
	private String name;

	private String website;

	private String address;

	private String mainContact;

	private String mainEmail;

	private Set<SiteDTO> sites = new HashSet<>();

	private String phone;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getZabbixLogin() {
		return zabbixLogin;
	}

	public void setZabbixLogin(String zabbixLogin) {
		this.zabbixLogin = zabbixLogin;
	}

	public String getZabbixPassword() {
		return zabbixPassword;
	}

	public void setZabbixPassword(String zabbixPassword) {
		this.zabbixPassword = zabbixPassword;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		CompanyDTO companyDTO = (CompanyDTO) o;
		if (companyDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), companyDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	public Set<SiteDTO> getSites() {
		return sites;
	}

	public void setSites(Set<SiteDTO> sites) {
		this.sites = sites;
	}

	public String getMainEmail() {
		return mainEmail;
	}

	public void setMainEmail(String mainEmail) {
		this.mainEmail = mainEmail;
	}

	public String getMainContact() {
		return mainContact;
	}

	public void setMainContact(String mainContact) {
		this.mainContact = mainContact;
	}

	@Override
	public String toString() {
		return "CompanyDTO {" + "id=" + getId() + ", zabbixLogin='" + getZabbixLogin() + "'" + ", name='" + getName()
				+ "'" + ", website='" + getWebsite() + "'" + ", address='" + getAddress() + "'" + ", phone='"
				+ getPhone() + "'" + "}";
	}

}
