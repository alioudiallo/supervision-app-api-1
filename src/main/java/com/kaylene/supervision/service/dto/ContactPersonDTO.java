package com.kaylene.supervision.service.dto;

import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ContactPerson entity.
 */
public class ContactPersonDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	@NotNull
	private String firtsname;

	@NotNull
	private String lastname;

	@Email
	@NotNull
	private String email;

	@NotNull
	@Pattern(regexp = "(^[0-9]*$)")
	@Size(min = 7, max = 14)
	private String mobilePhone;

	@Pattern(regexp = "(^[0-9]*$)")
	@Size(min = 7, max = 14)
	private String fixedPhone;

	private Long companyId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirtsname() {
		return firtsname;
	}

	public void setFirtsname(String firtsname) {
		this.firtsname = firtsname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getFixedPhone() {
		return fixedPhone;
	}

	public void setFixedPhone(String fixedPhone) {
		this.fixedPhone = fixedPhone;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		ContactPersonDTO contactPersonDTO = (ContactPersonDTO) o;
		if (contactPersonDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), contactPersonDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "ContactPersonDTO{" + "id=" + getId() + ", firtsname='" + getFirtsname() + "'" + ", lastname='"
				+ getLastname() + "'" + ", email='" + getEmail() + "'" + ", mobilePhone='" + getMobilePhone() + "'"
				+ ", fixedPhone='" + getFixedPhone() + "'" + "}";
	}
}
