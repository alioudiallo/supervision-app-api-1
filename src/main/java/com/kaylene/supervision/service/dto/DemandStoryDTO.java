package com.kaylene.supervision.service.dto;

import com.kaylene.supervision.domain.enumeration.DemandStatus;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the DemandStory entity.
 */
public class DemandStoryDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private Instant createdDate;

	private DemandStatus status;

	private Long userId;

	private Long demandId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Instant getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Instant createdDate) {
		this.createdDate = createdDate;
	}

	public DemandStatus getStatus() {
		return status;
	}

	public void setStatus(DemandStatus status) {
		this.status = status;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getDemandId() {
		return demandId;
	}

	public void setDemandId(Long demandId) {
		this.demandId = demandId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		DemandStoryDTO demandStoryDTO = (DemandStoryDTO) o;
		if (demandStoryDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), demandStoryDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "DemandStoryDTO{" + "id=" + getId() + ", createdDate='" + getCreatedDate() + "'" + ", status='"
				+ getStatus() + "'" + "}";
	}
}
