package com.kaylene.supervision.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the HostDemand entity.
 */
public class HostDemandDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String zabbixId;

	private String initialLabel;

	private String label;

	private String initialIpAddress;

	private String ipAddress;

	private String initialSiteId;

	private Long siteId;

	private String type;

	private String typeLabel;

	private String codeSnmp;

	private Instant dateIntegration;

	private Long demandId;

	private boolean isDoneInZabbix = false;

	private String marque;

	private String modele;

	private String role;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getZabbixId() {
		return zabbixId;
	}

	public void setZabbixId(String zabbixId) {
		this.zabbixId = zabbixId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Long getSiteId() {
		return siteId;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTypeLabel() {
		return typeLabel;
	}

	public void setTypeLabel(String typeLabel) {
		this.typeLabel = typeLabel;
	}

	public String getCodeSnmp() {
		return codeSnmp;
	}

	public void setCodeSnmp(String codeSnmp) {
		this.codeSnmp = codeSnmp;
	}

	public Instant getDateIntegration() {
		return dateIntegration;
	}

	public void setDateIntegration(Instant dateIntegration) {
		this.dateIntegration = dateIntegration;
	}

	public Long getDemandId() {
		return demandId;
	}

	public void setDemandId(Long demandId) {
		this.demandId = demandId;
	}

	public String getInitialLabel() {
		return initialLabel;
	}

	public void setInitialLabel(String initialLabel) {
		this.initialLabel = initialLabel;
	}

	public String getInitialIpAddress() {
		return initialIpAddress;
	}

	public void setInitialIpAddress(String initialIpAddress) {
		this.initialIpAddress = initialIpAddress;
	}

	public String getInitialSiteId() {
		return initialSiteId;
	}

	public void setInitialSiteId(String initialSiteId) {
		this.initialSiteId = initialSiteId;
	}

	public boolean getIsDoneInZabbix() {
		return isDoneInZabbix;
	}

	public void setIsDoneInZabbix(boolean isDoneInZabbix) {
		this.isDoneInZabbix = isDoneInZabbix;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public String getModele() {
		return modele;
	}

	public void setModele(String modele) {
		this.modele = modele;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		HostDemandDTO hostDemandDTO = (HostDemandDTO) o;
		if (hostDemandDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), hostDemandDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "HostDemandDTO{" + "id=" + getId() + ", zabbixId='" + getZabbixId() + "'" + ", label='" + getLabel()
				+ "'" + ", ipAddress='" + getIpAddress() + "'" + ", siteId='" + getSiteId() + "'" + ", type='"
				+ getType() + "'" + ", codeSnmp='" + getCodeSnmp() + "'" + ", dateIntegration='" + getDateIntegration()
				+ "'" + ", modele='" + getModele() + "'" + ", marque='" + getMarque() + "'" + ", role='" + getRole()
				+ "'" + "}";
	}
}
