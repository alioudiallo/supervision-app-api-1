package com.kaylene.supervision.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the Offer entity.
 */
public class OfferDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String label;

	private Integer maxHost;

	private Instant beginningTime;

	private Instant endingTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Integer getMaxHost() {
		return maxHost;
	}

	public void setMaxHost(Integer maxHost) {
		this.maxHost = maxHost;
	}

	public Instant getBeginningTime() {
		return beginningTime;
	}

	public void setBeginningTime(Instant beginningTime) {
		this.beginningTime = beginningTime;
	}

	public Instant getEndingTime() {
		return endingTime;
	}

	public void setEndingTime(Instant endingTime) {
		this.endingTime = endingTime;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		OfferDTO offerDTO = (OfferDTO) o;
		if (offerDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), offerDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "OfferDTO{" + "id=" + getId() + ", label='" + getLabel() + "'" + ", maxHost='" + getMaxHost() + "'"
				+ "}";
	}
}
