package com.kaylene.supervision.service.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Site entity.
 */
public class SiteDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String zabbixProxyId;

	@NotNull
	private String label;

	@NotNull
	private String address;

	private String latitude;

	private String longitude;

	private Long companyId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getZabbixProxyId() {
		return zabbixProxyId;
	}

	public void setZabbixProxyId(String zabbixProxyId) {
		this.zabbixProxyId = zabbixProxyId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		SiteDTO siteDTO = (SiteDTO) o;
		if (siteDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), siteDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "SiteDTO{" + "id=" + getId() + ", zabbixProxyId='" + getZabbixProxyId() + "'" + ", label='" + getLabel()
				+ "'" + ", address='" + getAddress() + "'" + ", latitude='" + getLatitude() + "'" + ", longitude='"
				+ getLongitude() + "'" + "}";
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
}
