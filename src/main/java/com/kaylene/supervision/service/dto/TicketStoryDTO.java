package com.kaylene.supervision.service.dto;

import com.kaylene.supervision.domain.enumeration.TicketStatus;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the TicketStory entity.
 */
public class TicketStoryDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private Instant date;

	private TicketStatus status;

	private Long ticketId;

	private Long adminId;

	private String adminName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Instant getDate() {
		return date;
	}

	public void setDate(Instant date) {
		this.date = date;
	}

	public TicketStatus getStatus() {
		return status;
	}

	public void setStatus(TicketStatus status) {
		this.status = status;
	}

	public Long getTicketId() {
		return ticketId;
	}

	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public Long getAdminId() {
		return adminId;
	}

	public void setAdminId(Long adminId) {
		this.adminId = adminId;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		TicketStoryDTO ticketStoryDTO = (TicketStoryDTO) o;
		if (ticketStoryDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), ticketStoryDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "TicketStoryDTO{" + "id=" + getId() + ", date='" + getDate() + "'" + ", status='" + getStatus() + "'"
				+ ", ticket='" + getTicketId() + "'" + ", adminId='" + getAdminId() + "'" + ", adminName='"
				+ getAdminName() + "'" + "}";
	}
}
