package com.kaylene.supervision.service.dto;

import com.kaylene.supervision.config.Constants;
import com.kaylene.supervision.domain.Authority;
import com.kaylene.supervision.domain.User;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A DTO representing a user, with his authorities.
 */
public class UserDTO {

	private Long id;

	@NotBlank
	@Pattern(regexp = Constants.LOGIN_REGEX)
	@Size(min = 5, max = 100)
	private String login;

	private String password;

	@Size(max = 50)
	private String firstName;

	@Size(max = 50)
	private String lastName;

	@Size(min = 5, max = 100)
	private String email;

	@Size(max = 256)
	private String imageUrl;

	private boolean activated = false;

	@Size(min = 2, max = 6)
	private String langKey;

	private String createdBy;

	private Instant createdDate;

	private String lastModifiedBy;

	private Instant lastModifiedDate;

	private Boolean accesFacture;

	private Set<String> authorities;

	private String phoneNumber;

	private String poste;

	private Boolean onTicketEMail = false;

	private Boolean onTicketSMS = false;

	private Boolean onTicketPhoneCall = false;

	@NotNull
	private Long companyId;

	public UserDTO() {
		// Empty constructor needed for Jackson.
	}

	public UserDTO(User user) {
		this(user.getId(), user.getLogin(), user.getPassword(), user.getFirstName(), user.getLastName(),
				user.getEmail(), user.getActivated(), user.getImageUrl(), user.getLangKey(), null, user.getCreatedBy(),
				user.getCreatedDate(), user.getLastModifiedBy(), user.getLastModifiedDate(),
				user.getAuthorities().stream().map(Authority::getName).collect(Collectors.toSet()));
	}

	public UserDTO(Long id, String login, String password, String firstName, String lastName, String email,
			boolean activated, String imageUrl, String langKey, Long companyId, String createdBy, Instant createdDate,
			String lastModifiedBy, Instant lastModifiedDate, Set<String> authorities) {

		this.id = id;
		this.login = login;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.activated = activated;
		this.imageUrl = imageUrl;
		this.langKey = langKey;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.lastModifiedBy = lastModifiedBy;
		this.lastModifiedDate = lastModifiedDate;
		this.authorities = authorities;
		this.password = password;
		this.companyId = companyId;
	}

	public Boolean getAccesFacture() {
		return accesFacture;
	}

	public void setAccesFacture(Boolean acces_facture) {
		this.accesFacture = acces_facture;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getEmail() {
		return email;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public boolean isActivated() {
		return activated;
	}

	public String getLangKey() {
		return langKey;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public Instant getCreatedDate() {
		return createdDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public Instant getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Instant lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public Set<String> getAuthorities() {
		return authorities;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	@Override
	public String toString() {
		return "UserDTO{" + "login='" + login + '\'' + ", firstName='" + firstName + '\'' + ", lastName='" + lastName
				+ '\'' + ", email='" + email + '\'' + ", imageUrl='" + imageUrl + '\'' + ", activated=" + activated
				+ ", langKey='" + langKey + '\'' + ", createdBy=" + createdBy + ", createdDate=" + createdDate
				+ ", lastModifiedBy='" + lastModifiedBy + '\'' + ", lastModifiedDate=" + lastModifiedDate
				+ ", authorities=" + authorities + "}";
	}

	public Boolean getOnTicketEMail() {
		return onTicketEMail;
	}

	public void setOnTicketEMail(Boolean onTicketEMail) {
		this.onTicketEMail = onTicketEMail;
	}

	public Boolean getOnTicketSMS() {
		return onTicketSMS;
	}

	public void setOnTicketSMS(Boolean onTicketSMS) {
		this.onTicketSMS = onTicketSMS;
	}

	public Boolean getOnTicketPhoneCall() {
		return onTicketPhoneCall;
	}

	public void setOnTicketPhoneCall(Boolean onTicketPhoneCall) {
		this.onTicketPhoneCall = onTicketPhoneCall;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPoste() {
		return poste;
	}

	public void setPoste(String poste) {
		this.poste = poste;
	}

}
