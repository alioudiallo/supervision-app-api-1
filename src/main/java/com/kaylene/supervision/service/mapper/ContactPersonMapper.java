package com.kaylene.supervision.service.mapper;

import com.kaylene.supervision.domain.ContactPerson;
import com.kaylene.supervision.service.dto.ContactPersonDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity ContactPerson and its DTO ContactPersonDTO.
 */
@Mapper(componentModel = "spring", uses = {CompanyMapper.class})
public interface ContactPersonMapper extends EntityMapper<ContactPersonDTO, ContactPerson> {

    @Mapping(source = "company.id", target = "companyId")
    ContactPersonDTO toDto(ContactPerson contactPerson); 

    @Mapping(source = "companyId", target = "company")
    ContactPerson toEntity(ContactPersonDTO contactPersonDTO);

    default ContactPerson fromId(Long id) {
        if (id == null) {
            return null;
        }
        ContactPerson contactPerson = new ContactPerson();
        contactPerson.setId(id);
        return contactPerson;
    }
}
