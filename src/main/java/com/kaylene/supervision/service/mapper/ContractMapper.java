package com.kaylene.supervision.service.mapper;

import com.kaylene.supervision.domain.Contract;
import com.kaylene.supervision.service.dto.ContractDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Contract and its DTO ContractDTO.
 */
@Mapper(componentModel = "spring", uses = { CompanyMapper.class, OfferMapper.class })
public interface ContractMapper extends EntityMapper<ContractDTO, Contract> {

	@Mapping(source = "company.id", target = "companyId")
	@Mapping(source = "offer.id", target = "offerId")
	ContractDTO toDto(Contract contractDTO);

	@Mapping(source = "companyId", target = "company")
	@Mapping(source = "offerId", target = "offer")
	Contract toEntity(ContractDTO contractDTO);

	default Contract fromId(Long id) {
		if (id == null) {
			return null;
		}
		Contract contract = new Contract();
		contract.setId(id);
		return contract;
	}

}
