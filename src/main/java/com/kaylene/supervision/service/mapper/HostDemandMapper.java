package com.kaylene.supervision.service.mapper;

import com.kaylene.supervision.domain.HostDemand;
import com.kaylene.supervision.service.dto.HostDemandDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity HostDemand and its DTO HostDemandDTO.
 */
@Mapper(componentModel = "spring", uses = {DemandMapper.class})
public interface HostDemandMapper extends EntityMapper<HostDemandDTO, HostDemand> {

    @Mapping(source = "demand.id", target = "demandId")
    HostDemandDTO toDto(HostDemand hostDemand); 

    @Mapping(source = "demandId", target = "demand")
    HostDemand toEntity(HostDemandDTO hostDemandDTO);

    default HostDemand fromId(Long id) {
        if (id == null) {
            return null;
        }
        HostDemand hostDemand = new HostDemand();
        hostDemand.setId(id);
        return hostDemand;
    }
}
