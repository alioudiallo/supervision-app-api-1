package com.kaylene.supervision.service.mapper;

import com.kaylene.supervision.domain.Offer;
import com.kaylene.supervision.service.dto.OfferDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity Offer and its DTO OfferDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface OfferMapper extends EntityMapper<OfferDTO, Offer> {

    

    

    default Offer fromId(Long id) {
        if (id == null) {
            return null;
        }
        Offer offer = new Offer();
        offer.setId(id);
        return offer;
    }
}
