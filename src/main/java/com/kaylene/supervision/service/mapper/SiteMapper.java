package com.kaylene.supervision.service.mapper;

import com.kaylene.supervision.domain.Site;
import com.kaylene.supervision.service.dto.SiteDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Site and its DTO SiteDTO.
 */
@Mapper(componentModel = "spring", uses = {CompanyMapper.class})
public interface SiteMapper extends EntityMapper<SiteDTO, Site> {

    @Mapping(source = "company.id", target = "companyId")
    SiteDTO toDto(Site site); 

    @Mapping(source = "companyId", target = "company")
    Site toEntity(SiteDTO siteDTO);

    default Site fromId(Long id) {
        if (id == null) {
            return null;
        }
        Site site = new Site();
        site.setId(id);
        return site;
    }
}
