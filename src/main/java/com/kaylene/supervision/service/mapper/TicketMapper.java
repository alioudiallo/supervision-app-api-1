package com.kaylene.supervision.service.mapper;

import com.kaylene.supervision.domain.Ticket;
import com.kaylene.supervision.service.dto.TicketDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Ticket and its DTO TicketDTO.
 */
@Mapper(componentModel = "spring", uses = { UserMapper.class, CompanyMapper.class })
public interface TicketMapper extends EntityMapper<TicketDTO, Ticket> {

	@Mapping(source = "company.id", target = "companyId")
	TicketDTO toDto(Ticket ticket);

	@Mapping(source = "companyId", target = "company")
	Ticket toEntity(TicketDTO ticketDTO);

	default Ticket fromId(Long id) {
		if (id == null) {
			return null;
		}
		Ticket ticket = new Ticket();
		ticket.setId(id);
		return ticket;
	}
}
