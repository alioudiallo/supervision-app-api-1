package com.kaylene.supervision.service.mapper;

import com.kaylene.supervision.domain.TicketMessage;
import com.kaylene.supervision.service.dto.TicketMessageDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity TicketMessage and its DTO TicketMessageDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TicketMessageMapper extends EntityMapper<TicketMessageDTO, TicketMessage> {

	default TicketMessage fromId(Long id) {
		if (id == null) {
			return null;
		}
		TicketMessage ticketMessage = new TicketMessage();
		ticketMessage.setId(id);
		return ticketMessage;
	}
}
