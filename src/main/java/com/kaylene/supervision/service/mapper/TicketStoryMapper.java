package com.kaylene.supervision.service.mapper;

import com.kaylene.supervision.domain.TicketStory;
import com.kaylene.supervision.service.dto.TicketStoryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity TicketStory and its DTO TicketStoryDTO.
 */
@Mapper(componentModel = "spring", uses = { TicketMapper.class, UserMapper.class })
public interface TicketStoryMapper extends EntityMapper<TicketStoryDTO, TicketStory> {

	@Mapping(source = "ticket.id", target = "ticketId")
	TicketStoryDTO toDto(TicketStory ticketStory);

	@Mapping(source = "ticketId", target = "ticket")
	TicketStory toEntity(TicketStoryDTO ticketStoryDTO);

	default TicketStory fromId(Long id) {
		if (id == null) {
			return null;
		}
		TicketStory ticketStory = new TicketStory();
		ticketStory.setId(id);
		return ticketStory;
	}
}
