package com.kaylene.supervision.service.others;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public class AuthenticationToZabbix {

	private final static Logger log = LoggerFactory.getLogger(AuthenticationToZabbix.class);

	public String authenticate(String url, String login, String password) throws JSONException {

		JSONObject params = new JSONObject();
		params.put("user", login);
		params.put("password", password);

		JSONObject request = new JSONObject();
		request.put("jsonrpc", "2.0");
		request.put("method", "user.login");
		request.put("params", params);
		request.put("id", 1);

		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		log.info("Sending authentication request to zabbix API");

		HttpEntity<String> entity = new HttpEntity<>(request.toString(), headers);
		try {
			ZabbixResponse response = restTemplate.postForObject(url, entity, ZabbixResponse.class);
			return response.getResult();
		} catch (Exception e) {
			log.error("Failed to authenticate to zabbix API via" + url + "LOGIN:" + login + "Password:" + password);
			return null;
		}

	}

	public Boolean notAuthenticate(String url, String login, String password) throws JSONException {
		AuthenticationToZabbix authenticationToZabbix = new AuthenticationToZabbix();

		String authResult = null;
		try {
			authResult = authenticationToZabbix.authenticate(url, login, password);
		} catch (JSONException e) {
			log.error("{}", e.getMessage());
		}
		
		JSONObject params = new JSONObject();
		params.put("auth", authResult);
		params.put("", params);
		JSONObject request = new JSONObject();
		request.put("jsonrpc", "2.0");
		request.put("method", "user.logout");
		request.put("params", params);
		request.put("id", 1);
		request.put("auth", authResult);

		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		log.info("Sending authentication deconnect request to zabbix API");

		HttpEntity<String> entity = new HttpEntity<>(request.toString(), headers);
		try {
			ZabbixResponse response = restTemplate.postForObject(url, entity, ZabbixResponse.class);
			// return result;
			// TODO Malick check
			return true;
		} catch (Exception e) {
			log.error("Failed to authenticate to zabbix API via" + url);
			return false;
		}

	}
}
