package com.kaylene.supervision.service.others;

import java.util.List;

public class Hosts {
	
	private String jsonrpc;
	
	private List<Host> result;
	
	private Long id;
    
	public String getJsonrpc() {
		return jsonrpc;
	}

	public void setJsonrpc(String jsonrpc) {
		this.jsonrpc = jsonrpc;
	}

	public List<Host> getResult() {
		return result;
	}
    
	public void setResult(List<Host> result) {
		this.result =  result;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
