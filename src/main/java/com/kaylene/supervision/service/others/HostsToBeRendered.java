package com.kaylene.supervision.service.others;

import java.util.List;
/**
 *  Un HostsToBeRendered est un équipement:
 *  
 *  	1. existant dans Zabbix
 *  
 *  	2. ayant fait l'objet d'une demande réalisée
 *  
 *  	3. faisant le mapping entre les informations présentes dans Zabbix et les informations
 *  
 *  	   présentes dans la base de données de l'API.
 *  
 *  Voir le service "HostsToBeRenderedService"
 * 
 */
public class HostsToBeRendered {
	
	//infos from ZabbixServer API
	private String hostid;
	
	private String name;
	
	private List<Interfaces> interfaces;
	
	private String status;
	
	private String maintenance_status;
	
	private String proxy_hostid;
	
	//infos from our API
	private String integrationDate;
	
	private String ticketSeverity;
	
	private String type;
	
	private String typeLabel;
	
	private String site;
	
	private Long siteId;
	
	private Integer numberOfTickets;
	
	private Integer numberOfDemands;
	

	public String getHostid() {
		return hostid;
	}

	public void setHostid(String hostid) {
		this.hostid = hostid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Interfaces> getInterfaces() {
		return interfaces;
	}

	public void setInterfaces(List<Interfaces> interfaces) {
		this.interfaces = interfaces;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIntegrationDate() {
		return integrationDate;
	}

	public void setIntegrationDate(String integrationDate) {
		this.integrationDate = integrationDate;
	}

	public String getTicketSeverity() {
		return ticketSeverity;
	}

	public void setTicketSeverity(String ticketSeveriry) {
		this.ticketSeverity = ticketSeveriry;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTypeLabel() {
		return typeLabel;
	}

	public void setTypeLabel(String typeLabel) {
		this.typeLabel = typeLabel;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public Integer getNumberOfTickets() {
		return numberOfTickets;
	}

	public void setNumberOfTickets(Integer numberOfTickets) {
		this.numberOfTickets = numberOfTickets;
	}

	public Integer getNumberOfDemands() {
		return numberOfDemands;
	}

	public void setNumberOfDemands(Integer numberOfDemands) {
		this.numberOfDemands = numberOfDemands;
	}

	public String getMaintenance_status() {
		return maintenance_status;
	}

	public void setMaintenance_status(String maintenance_status) {
		this.maintenance_status = maintenance_status;
	}

	public String getProxy_hostid() {
		return proxy_hostid;
	}

	public void setProxy_hostid(String proxy_hostid) {
		this.proxy_hostid = proxy_hostid;
	}

	public Long getSiteId() {
		return siteId;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}
}
