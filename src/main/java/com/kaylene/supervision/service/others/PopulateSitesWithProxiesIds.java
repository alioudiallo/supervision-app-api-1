package com.kaylene.supervision.service.others;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

// TODO Malick check  

@Service
public class PopulateSitesWithProxiesIds {

	private final static Logger log = LoggerFactory.getLogger(PopulateSitesWithProxiesIds.class);

	// @Autowired
	// private CompanyRepository companyRepository;

	// private static Hosts hosts;

	private Hosts getHostsInfo(String url, String login, String password) throws JSONException {

		AuthenticationToZabbix authenticationToZabbix = new AuthenticationToZabbix();

		String authResult = authenticationToZabbix.authenticate(url, login, password);

		RestTemplate restTemplate = new RestTemplate();

		JSONArray outputProperties = new JSONArray();
		outputProperties.put("hostid");
		outputProperties.put("proxy_hostid");
		outputProperties.put("host");

		JSONObject params = new JSONObject();
		params.put("output", outputProperties);

		JSONObject request = new JSONObject();
		request.put("jsonrpc", "2.0");
		request.put("method", "host.get");
		request.put("params", params);
		request.put("id", 1);
		request.put("auth", authResult);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);
		try {
			Hosts hosts = restTemplate.postForObject(url, entity, Hosts.class);
			if (hosts.getResult() != null) {
				// PopulateSitesWithProxiesIds.hosts = hosts;
				return hosts;
			} else {
				return null;
			}
		} catch (HttpClientErrorException hcee) {
			log.error("{}", hcee.getMessage());
			return null;
		}

	}

	public void populate(String url, String login, String password) {
		try {
			Hosts hosts = getHostsInfo(url, login, password);
			if (hosts == null) {
				log.error("No 'hostid', 'proxy_host_id', 'host' returned by Zabbix API for user {}", login);
			} else {
				// Set<String> theProxiesIds = getProxiesIds();
				// Company company = companyRepository.findByLogin(login);
				// TODO check
			}
		} catch (JSONException je) {
			log.error("{}", je.getMessage());
		}

	}

	public static Set<String> getProxiesIds(List<Host> hosts) {
		List<Host> hostsInfos = hosts;
		return hostsInfos.stream().map(x -> x.getProxy_hostid()).collect(Collectors.toSet());

	}
}
