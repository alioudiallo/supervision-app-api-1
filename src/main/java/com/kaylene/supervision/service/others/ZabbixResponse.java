package com.kaylene.supervision.service.others;

public class ZabbixResponse {

	private String jsonrpc;

	private String result;

	private Long id;

	public String getJsonrpc() {
		return jsonrpc;
	}

	public String setJsonrpc(String jsonrpc) {
		return jsonrpc;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}