package com.kaylene.supervision.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kaylene.supervision.domain.Authority;
import com.kaylene.supervision.repository.AuthorityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/kaylene")
public class AuthorityResource {
	
	@Autowired
	AuthorityRepository authorityRepository;
	 @GetMapping("/authorithy")
	    @Timed
	    public List<Authority> getAllAuthority() {
	        List<Authority> listAuthaurity=authorityRepository.findAll();
	        return listAuthaurity;
	    }

}
