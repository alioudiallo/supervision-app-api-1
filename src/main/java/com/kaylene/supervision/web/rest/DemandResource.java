package com.kaylene.supervision.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kaylene.supervision.domain.Demand;
import com.kaylene.supervision.repository.DemandRepository;
import com.kaylene.supervision.service.DemandService;
import com.kaylene.supervision.service.dto.DemandDTO;
import com.kaylene.supervision.web.rest.errors.BadRequestAlertException;
import com.kaylene.supervision.web.rest.util.HeaderUtil;
import com.kaylene.supervision.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Demand.
 */
@RestController
@RequestMapping("/api")
public class DemandResource {

	private final Logger log = LoggerFactory.getLogger(DemandResource.class);

	private static final String ENTITY_NAME = "demand";

	private final DemandService demandService;

	private final DemandRepository demandRepository;

	public DemandResource(DemandService demandService, DemandRepository demandRepository) {
		this.demandService = demandService;
		this.demandRepository = demandRepository;
	}

	/**
	 * POST /demands : Create a new demand.
	 *
	 * @param demandDTO
	 *            the demandDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         demandDTO, or with status 400 (Bad Request) if the demand has already
	 *         an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/demands")
	@Timed
	public ResponseEntity<DemandDTO> createDemand(@RequestBody DemandDTO demandDTO) throws URISyntaxException {
		log.debug("REST request to save Demand : {}", demandDTO);
		if (demandDTO.getId() != null) {
			throw new BadRequestAlertException("A new demand cannot already have an ID", ENTITY_NAME, "idexists");
		}
		DemandDTO result = demandService.save(demandDTO);
		return ResponseEntity.created(new URI("/api/demands/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /demands : Updates an existing demand.
	 *
	 * @param demandDTO
	 *            the demandDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         demandDTO, or with status 400 (Bad Request) if the demandDTO is not
	 *         valid, or with status 500 (Internal Server Error) if the demandDTO
	 *         couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/demands")
	@Timed
	public ResponseEntity<DemandDTO> updateDemand(@RequestBody DemandDTO demandDTO) throws URISyntaxException {
		log.debug("REST request to update Demand : {}", demandDTO);
		if (demandDTO.getId() == null) {
			return createDemand(demandDTO);
		}
		DemandDTO result = demandService.update(demandDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, demandDTO.getId().toString())).body(result);
	}

	/**
	 * GET /demands : get all the demands.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of demands in
	 *         body
	 */
	@GetMapping("/demands")
	@Timed
	public ResponseEntity<List<DemandDTO>> getAllDemands(@PageableDefault(size = 150) @ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Demands");
		Page<DemandDTO> page = demandService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/demands");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	@GetMapping("/demands/requestdemand")
	@Timed
	public List<Demand> getAllRequestedDemands(Long companyId) {
		log.debug("List des demandes ouvert pour une company");
		List<Demand> listDemand = demandRepository.findReqestedDemandByCompanyId(companyId);
		return listDemand;

	}

	/**
	 * GET /demands/:id : get the "id" demand.
	 *
	 * @param id
	 *            the id of the demandDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the demandDTO,
	 *         or with status 404 (Not Found)
	 */
	@GetMapping("/demands/{id}")
	@Timed
	public ResponseEntity<DemandDTO> getDemand(@PathVariable Long id) {
		log.debug("REST request to get Demand : {}", id);
		DemandDTO demandDTO = demandService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(demandDTO));
	}

	/**
	 * DELETE /demands/:id : delete the "id" demand.
	 *
	 * @param id
	 *            the id of the demandDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/demands/{id}")
	@Timed
	public ResponseEntity<Void> deleteDemand(@PathVariable Long id) {
		log.debug("REST request to delete Demand : {}", id);
		demandService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * GET /demands/number-of-demands-on-host/:zabbixId
	 *
	 * @param zabbixId
	 *            the zabbixId of the host
	 * @return the ResponseEntity with status 200 (OK) and with body the number of
	 *         demands on host, or with status 404 (Not Found)
	 */
	@GetMapping("/tickets/number-of-demands-on-host/{zabbixId}")
	@Timed
	public ResponseEntity<Integer> getNumberOfTicketsOnHost(@PathVariable String zabbixId) {
		log.debug("REST request to get number of demands on host : {}", zabbixId);
		int numberOfDemands = demandService.getNumberOfDemandsOnHost(zabbixId);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(numberOfDemands));
	}
}
