package com.kaylene.supervision.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kaylene.supervision.domain.DemandStory;
import com.kaylene.supervision.repository.DemandStoryRepository;
import com.kaylene.supervision.service.dto.DemandStoryDTO;
import com.kaylene.supervision.service.mapper.DemandStoryMapper;
import com.kaylene.supervision.web.rest.errors.BadRequestAlertException;
import com.kaylene.supervision.web.rest.util.HeaderUtil;
import com.kaylene.supervision.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DemandStory.
 */
@RestController
@RequestMapping("/api")
public class DemandStoryResource {

	private final Logger log = LoggerFactory.getLogger(DemandStoryResource.class);

	private static final String ENTITY_NAME = "demandStory";

	private final DemandStoryRepository demandStoryRepository;

	private final DemandStoryMapper demandStoryMapper;

	public DemandStoryResource(DemandStoryRepository demandStoryRepository, DemandStoryMapper demandStoryMapper) {
		this.demandStoryRepository = demandStoryRepository;
		this.demandStoryMapper = demandStoryMapper;
	}

	/**
	 * POST /demand-stories : Create a new demandStory.
	 *
	 * @param demandStoryDTO
	 *            the demandStoryDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         demandStoryDTO, or with status 400 (Bad Request) if the demandStory
	 *         has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/demand-stories")
	@Timed
	public ResponseEntity<DemandStoryDTO> createDemandStory(@RequestBody DemandStoryDTO demandStoryDTO)
			throws URISyntaxException {
		log.debug("REST request to save DemandStory : {}", demandStoryDTO);
		if (demandStoryDTO.getId() != null) {
			throw new BadRequestAlertException("A new demandStory cannot already have an ID", ENTITY_NAME, "idexists");
		}
		DemandStory demandStory = demandStoryMapper.toEntity(demandStoryDTO);
		demandStory = demandStoryRepository.save(demandStory);
		DemandStoryDTO result = demandStoryMapper.toDto(demandStory);
		return ResponseEntity.created(new URI("/api/demand-stories/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /demand-stories : Updates an existing demandStory.
	 *
	 * @param demandStoryDTO
	 *            the demandStoryDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         demandStoryDTO, or with status 400 (Bad Request) if the
	 *         demandStoryDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the demandStoryDTO couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/demand-stories")
	@Timed
	public ResponseEntity<DemandStoryDTO> updateDemandStory(@RequestBody DemandStoryDTO demandStoryDTO)
			throws URISyntaxException {
		log.debug("REST request to update DemandStory : {}", demandStoryDTO);
		if (demandStoryDTO.getId() == null) {
			return createDemandStory(demandStoryDTO);
		}
		DemandStory demandStory = demandStoryMapper.toEntity(demandStoryDTO);
		demandStory = demandStoryRepository.save(demandStory);
		DemandStoryDTO result = demandStoryMapper.toDto(demandStory);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, demandStoryDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /demand-stories : get all the demandStories.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of demandStories
	 *         in body
	 */
	@GetMapping("/demand-stories")
	@Timed
	public ResponseEntity<List<DemandStoryDTO>> getAllDemandStories(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of DemandStories");
		Page<DemandStory> page = demandStoryRepository.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/demand-stories");
		return new ResponseEntity<>(demandStoryMapper.toDto(page.getContent()), headers, HttpStatus.OK);
	}

	/**
	 * GET /demand-stories/:id : get the "id" demandStory.
	 *
	 * @param id
	 *            the id of the demandStoryDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         demandStoryDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/demand-stories/{id}")
	@Timed
	public ResponseEntity<DemandStoryDTO> getDemandStory(@PathVariable Long id) {
		log.debug("REST request to get DemandStory : {}", id);
		DemandStory demandStory = demandStoryRepository.findOne(id);
		DemandStoryDTO demandStoryDTO = demandStoryMapper.toDto(demandStory);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(demandStoryDTO));
	}

	/**
	 * DELETE /demand-stories/:id : delete the "id" demandStory.
	 *
	 * @param id
	 *            the id of the demandStoryDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/demand-stories/{id}")
	@Timed
	public ResponseEntity<Void> deleteDemandStory(@PathVariable Long id) {
		log.debug("REST request to delete DemandStory : {}", id);
		demandStoryRepository.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
