package com.kaylene.supervision.web.rest;

import com.kaylene.supervision.domain.Company;
import com.kaylene.supervision.domain.User;
import com.kaylene.supervision.repository.CompanyRepository;
import com.kaylene.supervision.repository.UserRepository;
import com.kaylene.supervision.security.SecurityUtils;
import com.kaylene.supervision.zabbix.service.GraphService;
import com.kaylene.supervision.zabbix.dto.ZabbixImageDTO;
import com.kaylene.supervision.zabbix.dto.ZabbixScreenDTO;
import com.kaylene.supervision.zabbix.exception.ZabbixApiException;
import com.kaylene.supervision.zabbix.model.ZabbixScreenItem;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author Mamadou Lamine NIANG
 **/
@RestController
@RequestMapping("/api/graphs")
@RequiredArgsConstructor
public class GraphResource {

    private final CompanyRepository companyRepository;
    private final GraphService graphService;
    private final UserRepository userRepository;

    private ConcurrentMap<Long, String> cookieCache = new ConcurrentHashMap<>();
    private ConcurrentMap<String, ZabbixScreenItem> itemsCache = new ConcurrentHashMap<>();

    @Scheduled(fixedRate = 3600000)
    public void clearCache() {
        cookieCache.clear();
        itemsCache.clear();
    }

    private String getCookie() throws ZabbixApiException {
        User user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).orElse(null);
        if(user == null) {
            throw new NoSuchElementException("User not found");
        }
        Long companyId = user.getCompany().getId();
        if(cookieCache.containsKey(companyId)) {
            return cookieCache.get(companyId);
        }
        Company company = companyRepository.findOne(companyId);
        if(company == null) {
            throw new NoSuchElementException("Company not found");
        }
        String cookie = graphService.getCookie(company.getZabbixLogin(), company.getZabbixPassword());
        cookieCache.put(companyId, cookie);
        return cookie;
    }

    @GetMapping("/clear-cache")
    public ResponseEntity<Void> clearCaches() {
        clearCache();
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    private Optional<ZabbixScreenDTO> findScreen(String id) throws ZabbixApiException {
        Optional<User> optionalUser = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());
        if(!optionalUser.isPresent()) {
            throw new NoSuchElementException("User not found");
        }
        Company company = optionalUser.get().getCompany();
        return graphService.getScreen(company.getZabbixLogin(), company.getZabbixPassword(), id);
    }

    private Optional<ZabbixScreenItem> findScreenItem(String screenId, String graphId) throws ZabbixApiException {
        Optional<ZabbixScreenDTO> screenDTO = findScreen(screenId);
        if(!screenDTO.isPresent()) {
            throw new NoSuchElementException("Screen not found");
        }
        Optional<ZabbixScreenItem> item = screenDTO.get().getItems().stream()
            .filter(i -> i.getId().equals(graphId))
            .findFirst();

        item.ifPresent(zabbixScreenItem -> itemsCache.put(graphId, zabbixScreenItem));
        return item;
    }

    @GetMapping("/screen/{id}")
    public ResponseEntity<ZabbixScreenDTO> getScreen(@PathVariable("id") String id) throws ZabbixApiException {
        return ResponseUtil.wrapOrNotFound(findScreen(id));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ZabbixScreenItem> getScreenItem(@PathVariable("id") String graphId, @RequestParam("screen") String screenId)
        throws ZabbixApiException {
        return ResponseUtil.wrapOrNotFound(findScreenItem(screenId, graphId));
    }

    @GetMapping("/image")
    public ResponseEntity<ZabbixImageDTO> getGraph(@RequestParam("screen") String screenId,
                                                   @RequestParam("graph") String graphId,
                                                   @RequestParam(value = "period", defaultValue = "1h") String period)
        throws ZabbixApiException {
        Optional<ZabbixScreenItem> optionalItem = Optional.ofNullable(itemsCache.getOrDefault(graphId, findScreenItem(screenId, graphId).orElse(null)));
        if(!optionalItem.isPresent()) {
            throw new NoSuchElementException("ScreenItem not found");
        }
        ZabbixScreenItem item = optionalItem.get();
        item.setPeriod(period);
        ZabbixImageDTO dto = new ZabbixImageDTO();
        dto.setBase64Img(graphService.getGraphAsBase64(getCookie(), item));
        return ResponseEntity.ok(dto);
    }
}
