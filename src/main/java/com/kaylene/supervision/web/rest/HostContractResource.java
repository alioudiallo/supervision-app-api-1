package com.kaylene.supervision.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kaylene.supervision.domain.HostContract;
import com.kaylene.supervision.repository.HostContractRepository;
import com.kaylene.supervision.service.dto.HostContractDTO;
import com.kaylene.supervision.service.mapper.HostContractMapper;
import com.kaylene.supervision.web.rest.errors.BadRequestAlertException;
import com.kaylene.supervision.web.rest.util.HeaderUtil;
import com.kaylene.supervision.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing HostContract.
 */
@RestController
@RequestMapping("/api")
public class HostContractResource {

	private final Logger log = LoggerFactory.getLogger(HostContractResource.class);

	private static final String ENTITY_NAME = "hostContract";

	private final HostContractRepository hostContractRepository;

	private final HostContractMapper hostContractMapper;

	public HostContractResource(HostContractRepository hostContractRepository, HostContractMapper hostContractMapper) {
		this.hostContractRepository = hostContractRepository;
		this.hostContractMapper = hostContractMapper;
	}

	/**
	 * POST /host-contracts : Create a new hostContract.
	 *
	 * @param hostContractDTO
	 *            the hostContractDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         hostContractDTO, or with status 400 (Bad Request) if the hostContract
	 *         has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/host-contracts")
	@Timed
	public ResponseEntity<HostContractDTO> createHostContract(@RequestBody HostContractDTO hostContractDTO)
			throws URISyntaxException {
		log.debug("REST request to save HostContract : {}", hostContractDTO);
		if (hostContractDTO.getId() != null) {
			throw new BadRequestAlertException("A new hostContract cannot already have an ID", ENTITY_NAME, "idexists");
		}
		HostContract hostContract = hostContractMapper.toEntity(hostContractDTO);
		hostContract = hostContractRepository.save(hostContract);
		HostContractDTO result = hostContractMapper.toDto(hostContract);
		return ResponseEntity.created(new URI("/api/host-contracts/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /host-contracts : Updates an existing hostContract.
	 *
	 * @param hostContractDTO
	 *            the hostContractDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         hostContractDTO, or with status 400 (Bad Request) if the
	 *         hostContractDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the hostContractDTO couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/host-contracts")
	@Timed
	public ResponseEntity<HostContractDTO> updateHostContract(@RequestBody HostContractDTO hostContractDTO)
			throws URISyntaxException {
		log.debug("REST request to update HostContract : {}", hostContractDTO);
		if (hostContractDTO.getId() == null) {
			return createHostContract(hostContractDTO);
		}
		HostContract hostContract = hostContractMapper.toEntity(hostContractDTO);
		hostContract = hostContractRepository.save(hostContract);
		HostContractDTO result = hostContractMapper.toDto(hostContract);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, hostContractDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /host-contracts : get all the hostContracts.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of hostContracts
	 *         in body
	 */
	@GetMapping("/host-contracts")
	@Timed
	public ResponseEntity<List<HostContractDTO>> getAllHostContracts(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of HostContracts");
		Page<HostContract> page = hostContractRepository.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/host-contracts");
		return new ResponseEntity<>(hostContractMapper.toDto(page.getContent()), headers, HttpStatus.OK);
	}

	/**
	 * GET /host-contracts/:id : get the "id" hostContract.
	 *
	 * @param id
	 *            the id of the hostContractDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         hostContractDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/host-contracts/{id}")
	@Timed
	public ResponseEntity<HostContractDTO> getHostContract(@PathVariable Long id) {
		log.debug("REST request to get HostContract : {}", id);
		HostContract hostContract = hostContractRepository.findOne(id);
		HostContractDTO hostContractDTO = hostContractMapper.toDto(hostContract);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(hostContractDTO));
	}

	/**
	 * DELETE /host-contracts/:id : delete the "id" hostContract.
	 *
	 * @param id
	 *            the id of the hostContractDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/host-contracts/{id}")
	@Timed
	public ResponseEntity<Void> deleteHostContract(@PathVariable Long id) {
		log.debug("REST request to delete HostContract : {}", id);
		hostContractRepository.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
