package com.kaylene.supervision.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.kaylene.supervision.service.HostDemandService;
import com.kaylene.supervision.service.HostTypeService;
import com.kaylene.supervision.service.dto.HostDemandDTO;
import com.kaylene.supervision.service.others.HostsToBeRendered;
import com.kaylene.supervision.service.others.HostsToBeRenderedService;
import com.kaylene.supervision.web.rest.errors.BadRequestAlertException;
import com.kaylene.supervision.web.rest.util.HeaderUtil;
import com.kaylene.supervision.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing HostDemand.
 */
@RestController
@RequestMapping("/api/kaylene")
public class HostDemandResource {

	private final Logger log = LoggerFactory.getLogger(HostDemandResource.class);

	private static final String ENTITY_NAME = "hostDemand";

	private final HostDemandService hostDemandService;

	@Autowired
	HostsToBeRenderedService hostsToBeRenderedService;

	@Autowired
	HostTypeService hostTypeService;

	public HostDemandResource(HostDemandService hostDemandService) {
		this.hostDemandService = hostDemandService;
	}

	/**
	 * POST /host-demands : Create a new hostDemand.
	 *
	 * @param hostDemandDTO
	 *            the hostDemandDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         hostDemandDTO, or with status 400 (Bad Request) if the hostDemand has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/host-demands")
	@Timed
	public ResponseEntity<HostDemandDTO> createHostDemand(@RequestBody HostDemandDTO hostDemandDTO)
			throws URISyntaxException {
		log.debug("REST request to save HostDemand : {}", hostDemandDTO);
		if (hostDemandDTO.getId() != null) {
			throw new BadRequestAlertException("A new hostDemand cannot already have an ID", ENTITY_NAME, "idexists");
		}
		HostDemandDTO result = hostDemandService.save(hostDemandDTO);
		return ResponseEntity.created(new URI("/api/host-demands/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /host-demands : Updates an existing hostDemand.
	 *
	 * @param hostDemandDTO
	 *            the hostDemandDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         hostDemandDTO, or with status 400 (Bad Request) if the hostDemandDTO
	 *         is not valid, or with status 500 (Internal Server Error) if the
	 *         hostDemandDTO couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/host-demands")
	@Timed
	public ResponseEntity<HostDemandDTO> updateHostDemand(@RequestBody HostDemandDTO hostDemandDTO)
			throws URISyntaxException {
		log.debug("REST request to update HostDemand : {}", hostDemandDTO);
		if (hostDemandDTO.getId() == null) {
			return createHostDemand(hostDemandDTO);
		}
		HostDemandDTO result = hostDemandService.save(hostDemandDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, hostDemandDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /host-demands : get all the hostDemands.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of hostDemands
	 *         in body
	 */
	@GetMapping("/host-demands")
	@Timed
	public ResponseEntity<List<HostDemandDTO>> getAllHostDemands(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of HostDemands");
		Page<HostDemandDTO> page = hostDemandService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/host-demands");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /host-demands/:id : get the "id" hostDemand.
	 *
	 * @param id
	 *            the id of the hostDemandDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         hostDemandDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/host-demands/{id}")
	@Timed
	public ResponseEntity<HostDemandDTO> getHostDemand(@PathVariable Long id) {
		log.debug("REST request to get HostDemand : {}", id);
		HostDemandDTO hostDemandDTO = hostDemandService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(hostDemandDTO));
	}

	/**
	 * DELETE /host-demands/:id : delete the "id" hostDemand.
	 *
	 * @param id
	 *            the id of the hostDemandDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/host-demands/{id}")
	@Timed
	public ResponseEntity<Void> deleteHostDemand(@PathVariable Long id) {
		log.debug("REST request to delete HostDemand : {}", id);
		hostDemandService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * GET /host-demands/by-company-id/id: get the "id" CompanyId.
	 *
	 * @param id
	 *            the id of the Company
	 * @return the ResponseEntity with status 200 (OK) and with body the list of
	 *         hostDemandDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/host-demands/by-company-id/{id}")
	@Timed
	public ResponseEntity<List<HostDemandDTO>> getHostDemandByCompanyId(@PathVariable Long id) {
		log.debug("REST request to get a list of HostDemand by company id : {}", id);
		List<HostDemandDTO> hostsDemandDTO = hostDemandService.findByDemandCompanyId(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(hostsDemandDTO));
	}

	/**
	 * GET /host-demands/by-company-demand-not-done/id: get the "id" CompanyId.
	 *
	 * @param id
	 *            the id of the Company
	 * @return the ResponseEntity with status 200 (OK) and with body the list of
	 *         hostDemandDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/host-demands/by-company-demand-not-done/{id}")
	@Timed
	public ResponseEntity<List<HostDemandDTO>> getHostByCompanyDemandNotDone(@PathVariable Long id) {
		log.debug("REST request to get a list of HostDemand by company id : {}", id);
		List<HostDemandDTO> hostsDemandDTO = hostDemandService.findByCompanyDemandNotDone(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(hostsDemandDTO));
	}

	/**
	 * GET /host-demands/hosts-to-be-rendered/{id}: get the "id" CompanyId.
	 *
	 * @param id
	 *            the id of the Company
	 * @return the ResponseEntity with status 200 (OK) and with body the list of
	 *         hosts, or with status 404 (Not Found)
	 * @throws JsonProcessingException
	 */
	@GetMapping("/host-demands/hosts-to-be-rendered/{companyId}")
	@Timed
	public ResponseEntity<String> getHostsToBeRendered(@PathVariable Long companyId) throws JsonProcessingException {
		log.debug("REST request to get a list of Host by company id : {}", companyId);
		List<HostsToBeRendered> hostsToBeRendered = hostsToBeRenderedService.getHostsToBeRendered(companyId, false);
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String hostToBeRenderedInJson = ow.writeValueAsString(hostsToBeRendered);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(hostToBeRenderedInJson));
	}

	/**
	 * GET /host-demands/on-last-done-demand/{zabbixId}: get the "zabbixId"
	 * CompanyId.
	 *
	 * @param id
	 *            the id of the Company
	 * @return the ResponseEntity with status 200 (OK) and with body the list of
	 *         hosts, or with status 404 (Not Found)
	 * @throws JsonProcessingException
	 */
	@GetMapping("/host-demands/on-last-done-demand/{zabbixId}")
	@Timed
	public ResponseEntity<HostDemandDTO> getByLastDoneDemand(@PathVariable String zabbixId) {
		log.debug("REST request to get a host by its zabbix id on the last done demand: {}", zabbixId);
		List<HostDemandDTO> hosts = hostDemandService.findByZabbixIdWhereDemandDone(zabbixId);
		if (hosts.isEmpty()) {
			return ResponseUtil.wrapOrNotFound(Optional.ofNullable(null));
		}
		HostDemandDTO lastHost = hosts.get(hosts.size() - 1);
		lastHost.setTypeLabel(hostTypeService.getHostTypeLabel(lastHost.getType()));
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(lastHost));
	}
}
