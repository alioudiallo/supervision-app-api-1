package com.kaylene.supervision.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kaylene.supervision.domain.HostType;
import com.kaylene.supervision.repository.HostTypeRepository;
import com.kaylene.supervision.service.HostDemandService;
import com.kaylene.supervision.web.rest.errors.BadRequestAlertException;
import com.kaylene.supervision.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static com.kaylene.supervision.constants.Messages.*;

/**
 * REST controller for managing HostType.
 */
@RestController
@RequestMapping("/api")
public class HostTypeResource {

	private final Logger log = LoggerFactory.getLogger(HostTypeResource.class);

	private static final String ENTITY_NAME = "hostType";

	private final HostTypeRepository hostTypeRepository;

	@Autowired
	HostDemandService hostDemandService;

	public HostTypeResource(HostTypeRepository hostTypeRepository) {
		this.hostTypeRepository = hostTypeRepository;
	}

	/**
	 * POST /host-types : Create a new hostType.
	 *
	 * @param hostType
	 *            the hostType to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         hostType, or with status 400 (Bad Request) if the hostType has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/host-types")
	@Timed
	public ResponseEntity<HostType> createHostType(@RequestBody HostType hostType) throws URISyntaxException {
		log.debug("REST request to save HostType : {}", hostType);
		if (hostType.getId() != null) {
			throw new BadRequestAlertException("A new hostType cannot already have an ID", ENTITY_NAME, "idexists");
		}
		hostType.setLabel(hostType.getLabel().toUpperCase());
		hostType.setName(hostType.getName().toUpperCase());
		if (hostTypeRepository.findByName(hostType.getName()).isPresent()) {
			throw new BadRequestAlertException(EXISTING_HOST_TYPE_NAME_ERROR_MSG, ENTITY_NAME, "nameexists");
		}
		if (hostTypeRepository.findByLabel(hostType.getLabel()).isPresent()) {
			throw new BadRequestAlertException(EXISTING_HOST_TYPE_LABEL_ERROR_MSG, ENTITY_NAME, "labelexists");
		}
		HostType result = hostTypeRepository.save(hostType);
		return ResponseEntity.created(new URI("/api/host-types/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /host-types : Updates an existing hostType.
	 *
	 * @param hostType
	 *            the hostType to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         hostType, or with status 400 (Bad Request) if the hostType is not
	 *         valid, or with status 500 (Internal Server Error) if the hostType
	 *         couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/host-types")
	@Timed
	public ResponseEntity<HostType> updateHostType(@RequestBody HostType hostType) throws URISyntaxException {
		log.debug("REST request to update HostType : {}", hostType);
		if (hostType.getId() == null) {
			return createHostType(hostType);
		}
		hostType.setLabel(hostType.getLabel().toUpperCase());
		hostType.setName(hostType.getName().toUpperCase());
		Optional<HostType> existingHostType = hostTypeRepository.findByName(hostType.getName());
		if (existingHostType.isPresent() && (!existingHostType.get().getId().equals(hostType.getId()))) {
			throw new BadRequestAlertException(EXISTING_HOST_TYPE_NAME_ERROR_MSG, ENTITY_NAME, "nameexists");
		}
		existingHostType = hostTypeRepository.findByLabel(hostType.getLabel());
		if (existingHostType.isPresent() && (!existingHostType.get().getId().equals(hostType.getId()))) {
			throw new BadRequestAlertException(EXISTING_HOST_TYPE_LABEL_ERROR_MSG, ENTITY_NAME, "labelexists");
		}
		HostType hostTypeToCheckModification = hostTypeRepository.findOne(hostType.getId());
		if (hostTypeToCheckModification != null
				&& (!hostTypeToCheckModification.getName().equals(hostType.getName()))) {
			throw new BadRequestAlertException("Seul le label d'un type d'équipement peut être modifié", ENTITY_NAME,
					"namecannnotbemodified");
		}
		HostType result = hostTypeRepository.save(hostType);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, hostType.getId().toString()))
				.body(result);
	}

	/**
	 * GET /host-types : get all the hostTypes.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of hostTypes in
	 *         body
	 */
	@GetMapping("/host-types")
	@Timed
	public List<HostType> getAllHostTypes() {
		log.debug("REST request to get all HostTypes");
		return hostTypeRepository.findAll();
	}

	/**
	 * GET /host-types/:id : get the "id" hostType.
	 *
	 * @param id
	 *            the id of the hostType to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the hostType,
	 *         or with status 404 (Not Found)
	 */
	@GetMapping("/host-types/{id}")
	@Timed
	public ResponseEntity<HostType> getHostType(@PathVariable Long id) {
		log.debug("REST request to get HostType : {}", id);
		HostType hostType = hostTypeRepository.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(hostType));
	}

	/**
	 * GET /host-types/by-name/:id : get the "name" hostType.
	 *
	 * @param name
	 *            the name of the hostType to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the hostType,
	 *         or with status 404 (Not Found)
	 */
	@GetMapping("/host-types/by-name/{name}")
	@Timed
	public ResponseEntity<HostType> getHostTypeByName(@PathVariable String name) {
		log.debug("REST request to get HostType by name : {}", name);
		return ResponseUtil.wrapOrNotFound(hostTypeRepository.findByName(name));
	}

	/**
	 * DELETE /host-types/:id : delete the "id" hostType.
	 *
	 * @param id
	 *            the id of the hostType to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/host-types/{id}")
	@Timed
	public ResponseEntity<Void> deleteHostType(@PathVariable Long id) {
		log.debug("REST request to delete HostType : {}", id);
		HostType hostTypeToDelete = hostTypeRepository.findOne(id);
		if (hostTypeToDelete == null) {
			throw new BadRequestAlertException(NO_HOST_TYPE_FOUND_ERROR_MSG, ENTITY_NAME, "hosttypenotfound");
		}
		if (hostDemandService.isHostTypeReferenced(hostTypeToDelete.getName())) {
			throw new BadRequestAlertException(
					"Ce type est référencé par au moins un équipement, il ne peut être supprimé.", ENTITY_NAME,
					"referencedtype");
		}
		hostTypeRepository.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
