package com.kaylene.supervision.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kaylene.supervision.domain.IncidentType;
import com.kaylene.supervision.repository.IncidentTypeRepository;
import com.kaylene.supervision.service.TicketService;
import com.kaylene.supervision.web.rest.errors.BadRequestAlertException;
import com.kaylene.supervision.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static com.kaylene.supervision.constants.Messages.EXISTING_INCIDENT_TYPE_KEY_ERROR_MSG;
import static com.kaylene.supervision.constants.Messages.EXISTING_INCIDENT_TYPE_LABEL_ERROR_MSG;

/**
 * REST controller for managing IncidentType.
 */
@RestController
@RequestMapping("/api")
public class IncidentTypeResource {

	private final Logger log = LoggerFactory.getLogger(IncidentTypeResource.class);

	private static final String ENTITY_NAME = "incidentType";

	private final IncidentTypeRepository incidentTypeRepository;

	@Autowired
	TicketService ticketService;

	public IncidentTypeResource(IncidentTypeRepository incidentTypeRepository) {
		this.incidentTypeRepository = incidentTypeRepository;
	}

	/**
	 * POST /incident-types : Create a new incidentType.
	 *
	 * @param incidentType
	 *            the incidentType to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         incidentType, or with status 400 (Bad Request) if the incidentType
	 *         has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/incident-types")
	@Timed
	public ResponseEntity<IncidentType> createIncidentType(@RequestBody IncidentType incidentType)
			throws URISyntaxException {
		log.debug("REST request to save IncidentType : {}", incidentType);
		if (incidentType.getId() != null) {
			throw new BadRequestAlertException("A new incidentType cannot already have an ID", ENTITY_NAME, "idexists");
		}
		if (incidentTypeRepository.findByKey(incidentType.getKey()).isPresent()) {
			throw new BadRequestAlertException(EXISTING_INCIDENT_TYPE_KEY_ERROR_MSG, ENTITY_NAME, "incidenttypeexists");
		}
		if (incidentTypeRepository.findByLabel(incidentType.getLabel()).isPresent()) {
			throw new BadRequestAlertException(EXISTING_INCIDENT_TYPE_LABEL_ERROR_MSG, ENTITY_NAME,
					"incidenttypelabelexists");
		}
		IncidentType result = incidentTypeRepository.save(incidentType);
		return ResponseEntity.created(new URI("/api/incident-types/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /incident-types : Updates an existing incidentType.
	 *
	 * @param incidentType
	 *            the incidentType to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         incidentType, or with status 400 (Bad Request) if the incidentType is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         incidentType couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/incident-types")
	@Timed
	public ResponseEntity<IncidentType> updateIncidentType(@RequestBody IncidentType incidentType)
			throws URISyntaxException {
		log.debug("REST request to update IncidentType : {}", incidentType);
		if (incidentType.getId() == null) {
			throw new BadRequestAlertException("L'id du type d'incident n'est pas renseigné", ENTITY_NAME,
					"incidenttypeidisnull");
		}
		Optional<IncidentType> existingIncidentType = incidentTypeRepository.findByKey(incidentType.getKey());
		if (existingIncidentType.isPresent() && !existingIncidentType.get().getId().equals(incidentType.getId())) {
			throw new BadRequestAlertException(EXISTING_INCIDENT_TYPE_KEY_ERROR_MSG, ENTITY_NAME, "incidenttypeexists");
		}
		existingIncidentType = incidentTypeRepository.findByLabel(incidentType.getLabel());
		if (existingIncidentType.isPresent() && !existingIncidentType.get().getId().equals(incidentType.getId())) {
			throw new BadRequestAlertException(EXISTING_INCIDENT_TYPE_LABEL_ERROR_MSG, ENTITY_NAME,
					"incidenttypelabelexists");
		}
		IncidentType incidentTypeToCheckModification = incidentTypeRepository.findOne(incidentType.getId());
		if (incidentTypeToCheckModification != null
				&& (!incidentTypeToCheckModification.getKey().equals(incidentType.getKey()))) {
			throw new BadRequestAlertException("Seul le label d'un type d'incident peut être modifié", ENTITY_NAME,
					"incidenttypecannotbemodified");
		}
		IncidentType result = incidentTypeRepository.save(incidentType);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, incidentType.getId().toString())).body(result);
	}

	/**
	 * GET /incident-types : get all the incidentTypes.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of incidentTypes
	 *         in body
	 */
	@GetMapping("/incident-types")
	@Timed
	public List<IncidentType> getAllIncidentTypes() {
		log.debug("REST request to get all IncidentTypes");
		return incidentTypeRepository.findAll();
	}

	/**
	 * GET /incident-types/:id : get the "id" incidentType.
	 *
	 * @param id
	 *            the id of the incidentType to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         incidentType, or with status 404 (Not Found)
	 */
	@GetMapping("/incident-types/{id}")
	@Timed
	public ResponseEntity<IncidentType> getIncidentType(@PathVariable Long id) {
		log.debug("REST request to get IncidentType : {}", id);
		IncidentType incidentType = incidentTypeRepository.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(incidentType));
	}

	/**
	 * DELETE /incident-types/:id : delete the "id" incidentType.
	 *
	 * @param id
	 *            the id of the incidentType to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/incident-types/{id}")
	@Timed
	public ResponseEntity<Void> deleteIncidentType(@PathVariable Long id) {
		log.debug("REST request to delete IncidentType : {}", id);
		IncidentType incidentTypeToDelete = incidentTypeRepository.findOne(id);
		if (incidentTypeToDelete == null) {
			throw new BadRequestAlertException("Aucun type d'incident trouvé", ENTITY_NAME, "incidenttypenotfound");
		}
		if (ticketService.isIncidentTypeReferencedByTicket(incidentTypeToDelete.getKey())) {
			throw new BadRequestAlertException(
					"Ce type d'incident est référencé par au moins un ticket, il ne peut être supprimé.", ENTITY_NAME,
					"referencedincidenttype");
		}
		incidentTypeRepository.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
