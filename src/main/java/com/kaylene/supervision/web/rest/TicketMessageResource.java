package com.kaylene.supervision.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kaylene.supervision.domain.Ticket;
import com.kaylene.supervision.domain.TicketMessage;
import com.kaylene.supervision.domain.enumeration.TicketStatus;
import com.kaylene.supervision.repository.TicketMessageRepository;
import com.kaylene.supervision.repository.TicketRepository;
import com.kaylene.supervision.service.MailService;
import com.kaylene.supervision.service.UserService;
import com.kaylene.supervision.service.dto.TicketMessageDTO;
import com.kaylene.supervision.service.mapper.TicketMessageMapper;
import com.kaylene.supervision.web.rest.errors.BadRequestAlertException;
import com.kaylene.supervision.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TicketMessage.
 */
@RestController
@RequestMapping("/api")
public class TicketMessageResource {

	private final Logger log = LoggerFactory.getLogger(TicketMessageResource.class);

	private static final String ENTITY_NAME = "ticketMessage";

	private final TicketMessageRepository ticketMessageRepository;

	private final TicketMessageMapper ticketMessageMapper;

	private final TicketRepository ticketRepository;

	private final UserService userService;

	private final MailService mailService;

	public TicketMessageResource(TicketMessageRepository ticketMessageRepository,
			TicketMessageMapper ticketMessageMapper, TicketRepository ticketRepository, UserService userService,
			MailService mailService) {
		super();
		this.ticketMessageRepository = ticketMessageRepository;
		this.ticketMessageMapper = ticketMessageMapper;
		this.ticketRepository = ticketRepository;
		this.userService = userService;
		this.mailService = mailService;
	}

	/**
	 * POST /ticket-messages : Create a new ticketMessage.
	 *
	 * @param ticketMessageDTO
	 *            the ticketMessageDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         ticketMessageDTO, or with status 400 (Bad Request) if the
	 *         ticketMessage has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/ticket-messages/{profile}")
	@Timed
	public ResponseEntity<TicketMessageDTO> createTicketMessage(@RequestBody TicketMessageDTO ticketMessageDTO,
			@PathVariable String profile) throws URISyntaxException {
		log.debug("REST request to save TicketMessage : {}", ticketMessageDTO);
		if (ticketMessageDTO.getId() != null) {
			throw new BadRequestAlertException("A new ticketMessage cannot already have an ID", ENTITY_NAME,
					"idexists");
		}
		if (profile == null) {
			throw new BadRequestAlertException("Veuillez renseigner un profil", ENTITY_NAME, "noprofileprovided");
		}
		boolean isAdmin = false;
		if (profile.toLowerCase().equals("admin")) {
			isAdmin = true;
		} else {
			isAdmin = false;
		}
		Ticket ticket = ticketRepository.findOne(ticketMessageDTO.getTicketId());
		if (ticket == null) {
			throw new BadRequestAlertException("Aucun ticket trouvé avec l'id renseigné", ENTITY_NAME,
					"ticketnotfound");
		}
		if (ticket.getStatus() == TicketStatus.CLOSE) {
			throw new BadRequestAlertException("Les fils de discussion sont fermés sur les tickets clôturés.",
					ENTITY_NAME, "closedtickets");
		}
		TicketMessage ticketMessage = ticketMessageMapper.toEntity(ticketMessageDTO);
		ticketMessage.setUserId(userService.getCurrentUserId());
		ticketMessage.createdDate(Instant.now());
		ticketMessage = ticketMessageRepository.save(ticketMessage);
		mailService.sendTicketMessageCreationEmail(ticketMessage, ticket, isAdmin);
		TicketMessageDTO result = ticketMessageMapper.toDto(ticketMessage);
		return ResponseEntity.created(new URI("/api/ticket-messages/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * GET /ticket-messages : get all the ticketMessages.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         ticketMessages in body
	 */
	@GetMapping("/ticket-messages")
	@Timed
	public List<TicketMessageDTO> getAllTicketMessages() {
		log.debug("REST request to get all TicketMessages");
		List<TicketMessage> ticketMessages = ticketMessageRepository.findAll();
		return ticketMessageMapper.toDto(ticketMessages);
	}

	/**
	 * GET /ticket-messages/:id : get the "id" ticketMessage.
	 *
	 * @param id
	 *            the id of the ticketMessageDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         ticketMessageDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/ticket-messages/{id}")
	@Timed
	public ResponseEntity<TicketMessageDTO> getTicketMessage(@PathVariable Long id) {
		log.debug("REST request to get TicketMessage : {}", id);
		TicketMessage ticketMessage = ticketMessageRepository.findOne(id);
		TicketMessageDTO ticketMessageDTO = ticketMessageMapper.toDto(ticketMessage);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(ticketMessageDTO));
	}

}
