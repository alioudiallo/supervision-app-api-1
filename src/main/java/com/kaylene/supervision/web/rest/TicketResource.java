package com.kaylene.supervision.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kaylene.supervision.domain.Ticket;
import com.kaylene.supervision.domain.enumeration.TicketStatus;
import com.kaylene.supervision.repository.CompanyRepository;
import com.kaylene.supervision.repository.TicketRepository;
import com.kaylene.supervision.service.*;
import com.kaylene.supervision.service.dto.TicketDTO;
import com.kaylene.supervision.service.mapper.TicketMapper;
import com.kaylene.supervision.web.rest.errors.BadRequestAlertException;
import com.kaylene.supervision.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import static com.kaylene.supervision.constants.Messages.UNCLOSED_TICKET_EXISTS_ERROR_MSG;

/**
 * REST controller for managing Ticket.
 */
@RestController
@RequestMapping("/api/kaylene")
@Transactional
public class TicketResource {

	private final Logger log = LoggerFactory.getLogger(TicketResource.class);

	private static final String ENTITY_NAME = "ticket";

	@Autowired
	private MailService mailService;

	@Autowired
	private UserService userService;

	@Autowired
	private IncidentTypeService incidentTypeService;

	@Autowired
	private TicketService ticketService;

	@Autowired
	private HostDemandService hostDemandService;

	@Autowired
	SaveInTicketStoryService saveInTicketStoryService;

	@Autowired
	HostTypeService hostTypeService;

	@Autowired
	CompanyRepository companyRepository;

	@Autowired
	TicketMessageService ticketMessageService;

	@Autowired
	DashboardService dashboardService;

	@Autowired
	TicketRepository ticketRepository;

	@Autowired
	TicketMapper ticketMapper;

	/**
	 * POST /tickets : Create a new ticket.
	 *
	 * @param ticketDTO
	 *            the ticketDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         ticketDTO, or with status 400 (Bad Request) if the ticket has already
	 *         an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/tickets")
	@Timed
	public ResponseEntity<TicketDTO> createTicket(@RequestBody TicketDTO ticketDTO) throws URISyntaxException {
		log.debug("REST request to save Ticket : {}", ticketDTO);
		if (ticketDTO.getId() != null) {
			throw new BadRequestAlertException("A new ticket cannot already have an ID", ENTITY_NAME, "idexists");
		}
		if (companyRepository.findOne(ticketDTO.getCompanyId()) == null) {
			throw new BadRequestAlertException("Aucune compagnie trouvée avec l'id renseigné", ENTITY_NAME,
					"companynotfound");
		}
		Ticket ticket = ticketMapper.toEntity(ticketDTO);
		if (ticketService.isTicketOpenedOnHost(ticket.getHostIdzabbix(), ticketDTO.getCompanyId())) {
			throw new BadRequestAlertException(UNCLOSED_TICKET_EXISTS_ERROR_MSG, ENTITY_NAME, "nonclosedticketexists");
		}
		List<Ticket> foundTickets = ticketRepository.findByTrackIdZabbix(ticket.getTrackIdZabbix());
		if (!foundTickets.isEmpty()) {
			throw new BadRequestAlertException("Un ticket a déja été associé à cet alert Id", ENTITY_NAME,
					"trackidexists");
		}
		ticket.setStatus(TicketStatus.OPEN);
		ticket.setCreatedDate(Instant.now());
		ticket.setHostType(hostDemandService.getHostType(ticket.getHostIdzabbix()));
		ticket.setHostTypeLabel(hostTypeService.getHostTypeLabel(ticket.getHostType()));
		ticket.setIncidentTypeLabel(incidentTypeService.getIncidentTypeLabelByKey(ticket.getIncidentType()));
		ticket = ticketRepository.save(ticket);
		if (!saveInTicketStoryService.save(ticket)) {
			log.error("Création de ticket non enregistrée dans l'historique");
		}
		// mise a jour du Dashboard du client
		dashboardService.updateDashboard(ticketDTO.getCompanyId());
		// envoi ticket en fonction des préférences de chaque user de la compagnie
		ticketService.sendTicket(ticket);
		TicketDTO result = ticketMapper.toDto(ticket);
		return ResponseEntity.created(new URI("/api/tickets/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /tickets : Updates an existing ticket.
	 *
	 * @param ticketDTO
	 *            the ticketDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         ticketDTO, or with status 400 (Bad Request) if the ticketDTO is not
	 *         valid, or with status 500 (Internal Server Error) if the ticketDTO
	 *         couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/tickets")
	@Timed
	public ResponseEntity<TicketDTO> updateTicket(@RequestBody TicketDTO ticketDTO) throws URISyntaxException {
		log.debug("REST request to update Ticket : {}", ticketDTO);
		if (ticketDTO.getId() == null) {
			return createTicket(ticketDTO);
		}
		if (companyRepository.findOne(ticketDTO.getCompanyId()) == null) {
			throw new BadRequestAlertException("Aucune compagnie trouvée avec l'id renseigné", ENTITY_NAME,
					"companynotfound");
		}
		Ticket ticket = ticketMapper.toEntity(ticketDTO);
		ticket.setModifiedDate(Instant.now());
		ticket.setModifierId(userService.getCurrentUserId());
		ticket.setModifierName(userService.getCurrentUserName());
		ticket.setIncidentTypeLabel(incidentTypeService.getIncidentTypeLabelByKey(ticket.getIncidentType()));
		ticket = ticketRepository.save(ticket);
		// Saving in TicketStory the current status of the ticket
		if (!saveInTicketStoryService.save(ticket)) {
			log.error("Modification de ticket non enregistrée dans l'historique");
		}
		TicketStatus status = ticket.getStatus();
		if (status == TicketStatus.OPEN) {
			mailService.sendTicketModificationEmail(ticket);
		}
		if (status == TicketStatus.CLOSE) {
			mailService.sendTicketClosingEmail(ticket);
		}
		dashboardService.updateDashboard(ticketDTO.getCompanyId());
		TicketDTO result = ticketMapper.toDto(ticket);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ticketDTO.getId().toString())).body(result);
	}

	/**
	 * GET /tickets : get all the tickets.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of tickets in
	 *         body
	 */
	// @GetMapping("/tickets")
	// @Timed
	// public ResponseEntity<List<TicketDTO>> getAllTickets(@ApiParam Pageable
	// pageable) {
	// log.debug("REST request to get a page of Tickets");
	// Page<Ticket> page = ticketRepository.findAll(pageable);
	// HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
	// "/api/tickets");
	// return new ResponseEntity<>(ticketMapper.toDto(page.getContent()), headers,
	// HttpStatus.OK);
	// }
	//
	@GetMapping("/tickets")
	@Timed
	public List<Ticket> getAllTickets() {
		log.debug("REST request to get a page of Tickets");
		List<Ticket> listTickets = ticketRepository.findAll();
		return listTickets;
	}

	/**
	 * GET /tickets/:id : get the "id" ticket.
	 *
	 * @param id
	 *            the id of the ticketDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the ticketDTO,
	 *         or with status 404 (Not Found)
	 */
	@GetMapping("/tickets/{id}")
	@Timed
	public ResponseEntity<TicketDTO> getTicket(@PathVariable Long id) {
		log.debug("REST request to get Ticket : {}", id);
		Ticket ticket = ticketRepository.findOne(id);
		TicketDTO ticketDTO = ticketMapper.toDto(ticket);
		ticketDTO.setIncidentTypeLabel(incidentTypeService.getIncidentTypeLabelByKey(ticket.getIncidentType()));
		ticketDTO.setMessages(ticketMessageService.getTicketMessagesByTicketId(id));
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(ticketDTO));
	}

	/**
	 * DELETE /tickets/:id : delete the "id" ticket.
	 *
	 * @param id
	 *            the id of the ticketDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/tickets/{id}")
	@Timed
	public ResponseEntity<Void> deleteTicket(@PathVariable Long id) {
		log.debug("REST request to delete Ticket : {}", id);
		saveInTicketStoryService.save(ticketRepository.findOne(id));
		ticketRepository.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * GET /tickets/by-company-id/:id
	 *
	 * @param id
	 *            the id of the company
	 * @return the ResponseEntity with status 200 (OK) and with body the ticketsDTO,
	 *         or with status 404 (Not Found)
	 */
	@GetMapping("/tickets/by-company-id/{id}")
	@Timed
	public ResponseEntity<List<TicketDTO>> getTicketsByCompanyId(@PathVariable Long id) {
		log.debug("REST request to get Tickets of company : {}", id);
		List<Ticket> tickets = ticketRepository.findByCompanyId(id);
		List<TicketDTO> ticketsDTO = ticketMapper.toDto(tickets);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(ticketsDTO));

	}

	/**
	 * GET /tickets/number-of-tickets-on-host/:hostIdzabbix
	 *
	 * @param hostIdzabbix
	 *            the zabbixId of the host
	 * @return the ResponseEntity with status 200 (OK) and with body the number of
	 *         tickets on host, or with status 404 (Not Found)
	 */
	@GetMapping("/tickets/number-of-tickets-on-host")
	@Timed
	public ResponseEntity<Integer> getNumberOfTicketsOnHost(@RequestParam("proxy") String hostIdzabbix,
                                                            @RequestParam("company") Long companyId) {
		log.debug("REST request to get number of tickets on host : {}", hostIdzabbix);
		int numberOfTickets = ticketService.getNumberOfTicketsOnHost(hostIdzabbix, companyId);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(numberOfTickets));
	}
}
