package com.kaylene.supervision.web.rest.errors;
import static com.kaylene.supervision.constants.Messages.EMAIL_ALREADY_USED_ERROR_MSG;

public class EmailAlreadyUsedException extends BadRequestAlertException {

    public EmailAlreadyUsedException() {
        super(ErrorConstants.EMAIL_ALREADY_USED_TYPE, EMAIL_ALREADY_USED_ERROR_MSG, "userManagement", "emailexists");
    }
}
