package com.kaylene.supervision.web.rest.errors;
import static com.kaylene.supervision.constants.Messages.ALREADY_USED_LOGIN_ERROR_MSG;

public class LoginAlreadyUsedException extends BadRequestAlertException {

    public LoginAlreadyUsedException() {
        super(ErrorConstants.LOGIN_ALREADY_USED_TYPE,ALREADY_USED_LOGIN_ERROR_MSG , "userManagement", "userexists");
    }
}
