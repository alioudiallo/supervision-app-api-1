package com.kaylene.supervision.web.rest.supervision;

import com.codahale.metrics.annotation.Timed;
import com.kaylene.supervision.domain.Contract;
import com.kaylene.supervision.domain.Demand;
import com.kaylene.supervision.domain.enumeration.DemandStatus;
import com.kaylene.supervision.domain.enumeration.DemandType;
import com.kaylene.supervision.repository.DemandRepository;
import com.kaylene.supervision.repository.HostDemandRepository;
import com.kaylene.supervision.service.CompanyService;
import com.kaylene.supervision.service.DemandService;
import com.kaylene.supervision.service.HostDemandService;
import com.kaylene.supervision.service.dto.DemandDTO;
import com.kaylene.supervision.service.dto.HostDemandDTO;
import com.kaylene.supervision.service.mapper.DemandMapper;
import com.kaylene.supervision.service.mapper.HostDemandMapper;
import com.kaylene.supervision.web.rest.errors.BadRequestAlertException;
import com.kaylene.supervision.web.rest.util.HeaderUtil;
import com.kaylene.supervision.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static com.kaylene.supervision.constants.Messages.*;

/**
 * REST controller for managing Demand.
 */
@RestController
@RequestMapping("/api/kaylene")
@Api(value = "/Demand", description = "Operations about Demands")
public class DemandCustomResource {

    private final Logger log = LoggerFactory.getLogger(DemandCustomResource.class);

    private static final String ENTITY_NAME = "demand";

    private final DemandService demandService;
    
    @Autowired
	HostDemandRepository hostDemandRepository; 
	
	@Autowired
	HostDemandMapper hostDemandMapper; 
    
    @Autowired
    private DemandRepository demandRepository;
    
    @Autowired
    private DemandMapper demandMapper;
    
    @Autowired
    HostDemandService hostDemandService;
    
    @Autowired
    CompanyService companyService;
    
    
    
    Demand demand;

    public DemandCustomResource(DemandService demandService) {
        this.demandService = demandService;
    }

    /**
     * POST  /demands : Create a new demand.
     *
     * @param demandDTO the demandDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new demandDTO, or with status 400 (Bad Request) if the demand has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/demands")
    @Timed
    public ResponseEntity<DemandDTO> createDemand(@RequestBody DemandDTO demandDTO) throws URISyntaxException {
		log.debug("REST request to save Demand : {}", demandDTO);
		if (demandDTO.getId() != null) {
			throw new BadRequestAlertException("A new demand cannot already have an ID", ENTITY_NAME, "idexists");
		}
		Contract contract = companyService.getLastOnGoingContract(demandDTO.getCompanyId());
		if (contract == null) {
			throw new BadRequestAlertException(NO_CONTRACT_FOUND_ERROR_MSG, ENTITY_NAME,"noongoingcontractfound");
		} else {
			demandDTO.setContractId(contract.getId());
		}
		if(!hostDemandService.isNumberOfHostsOk(demandDTO)) {
			throw new BadRequestAlertException(TOO_MANY_HOSTS_ON_DEMAND_ERROR_MSG, ENTITY_NAME,
					"toomanyhostsondemand");
		}
		if (demandDTO.getType() != DemandType.ADD) {
			if (hostDemandService.thereIsANotValidatedDemandOnHost(demandDTO)) {
				throw new BadRequestAlertException(NOT_DONE_DEMAND_ON_HOST_ERROR_MSG, ENTITY_NAME,
						"notvalidateddemandexists");
			}
		}
		if (hostDemandService.isIntegrationDateBeforeToday(demandDTO)) {
			throw new BadRequestAlertException(INTEGRATION_DATE_ERROR_MSG, ENTITY_NAME, "integrationDateIsBeforeToday");
		}
		if (hostDemandService.thereAreTwoIdenticalHostNames(demandDTO)) {
			throw new BadRequestAlertException(SAME_NAMES_ERROR_MSG, ENTITY_NAME, "twoIdenticalHostNames");
		}
		if (hostDemandService.isOneIpAddressInvalid(demandDTO)) {
			throw new BadRequestAlertException(INVALID_IP_ADDRESS_ERROR_MSG, ENTITY_NAME, "ipAddressIsInvalid");
		}
		if (hostDemandService.thereAreTwoIdenticalIpAddresses(demandDTO)) {
			throw new BadRequestAlertException(SAME_IP_ADDRESSES_ERROR_MSG, ENTITY_NAME, "twoIdenticalIpAddresses");
		}
		DemandDTO result = demandService.save(demandDTO);
		demandService.saveInDemandStory(result);
		return ResponseEntity.created(new URI("/api/demands/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT  /demands : Updates an existing demand.
     *
     * @param demandDTO the demandDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated demandDTO,
     * or with status 400 (Bad Request) if the demandDTO is not valid,
     * or with status 500 (Internal Server Error) if the demandDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/demands")
    @Timed
    public ResponseEntity<DemandDTO> updateDemand(@RequestBody DemandDTO demandDTO) throws URISyntaxException {
        log.debug("REST request to update Demand : {}", demandDTO);
        if (demandDTO.getId() == null) {
            return createDemand(demandDTO);
        }
        //demandDTO = demandService.getCheckedDemand(demandDTO, demandDTO.getType(), true);
        DemandDTO result = demandService.update(demandDTO);
        demandService.saveInDemandStory(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, demandDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /demands : get all the demands.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of demands in body
     */
    @GetMapping("/demands")
    @Timed
    public ResponseEntity<List<DemandDTO>> getAllDemands(@PageableDefault(size = 150) @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Demands");
        Page<DemandDTO> page = demandService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/demands");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /demands/:id : get the "id" demand.
     *
     * @param id the id of the demandDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the demandDTO, or with status 404 (Not Found)
     */
    @GetMapping("/demands/{id}")
    @Timed
    public ResponseEntity<DemandDTO> getDemand(@PathVariable Long id) {
        log.debug("REST request to get Demand : {}", id);
        DemandDTO demandDTO = demandService.findOne(id);
        demandDTO = demandService.getCheckedDemand(demandDTO, demandDTO.getType(), false);
        demandService.setHostTypeLabelsForHostsInDemand(demandDTO);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(demandDTO));
    }

    /**
     * DELETE  /demands/:id : delete the "id" demand.
     *
     * @param id the id of the demandDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/demands/{id}")
    @Timed
    public ResponseEntity<Void> deleteDemand(@PathVariable Long id) {
        log.debug("REST request to delete Demand : {}", id);
        demandService.saveDeletionInDemandStory(demandService.findOne(id));
        demandService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    

    /**
     * DELETE multiple demands /demands/:ids : delete the "ids" demand.
     *
     * @param id the id of the demandDTO to delete
     */
    @DeleteMapping("/demands/{ids}")
    @Timed
    public void deleteMultipleDemand(@RequestBody List<Demand> ids,DemandDTO demand, DemandType demandType){
        log.debug("REST request to delete Demand : {}", ids);
        for(Demand str:ids) {
                 if(str.getCurrentStatus() == DemandStatus.REQUESTED){
  		                 demandService.deleteMultipleDemand(ids);
            	}   
                 else  {
            			 throw new BadRequestAlertException("Une des demandes a ete realisee", "demand", "errorKey");

    	   }
        }
     }
    
    /**
     * GET  /demands/by-company-id/:id 
     *
     * @param id the id of the company
     * @return the ResponseEntity with status 200 (OK) and with body the companiesDTO, or with status 404 (Not Found)
     */
    @GetMapping("/demands/by-company-id/{id}")
    @Timed
    public ResponseEntity<List<DemandDTO>> getDemandsByCompanyId(@PathVariable Long id) {
        log.debug("REST request to get Demands of company : {}", id);
        List<Demand> demands = demandRepository.findByCompanyId(id);
		List<DemandDTO> demandsDTO = demandMapper.toDto(demands);
		//return new ResponseEntity<>(demandsDTO, HttpStatus.OK);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(demandsDTO));
    }
    

    /**
     * PUT  /demands/do-demand : Updates an existing demand.
     *
     * @param demandDTO the demandDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated demandDTO,
     * or with status 400 (Bad Request) if the demandDTO is not valid,
     * or with status 500 (Internal Server Error) if the demandDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/demands/do-demand")
    @Timed
    public ResponseEntity<DemandDTO> doDemand(@RequestBody DemandDTO demandDTO) throws URISyntaxException {
        log.debug("REST request to set Demand as done: {}", demandDTO);
        if (demandDTO.getId() == null) {
            return createDemand(demandDTO);
        }
        demandDTO = demandService.getCheckedDemand(demandDTO, demandDTO.getType(), true);
        demandDTO.setCurrentStatus(DemandStatus.DONE);
        DemandDTO result = demandService.update(demandDTO);
        demandService.saveInDemandStory(result);
		for (HostDemandDTO hd : demandDTO.getHostsToAdds()) {
			hostDemandRepository.save(hostDemandMapper.toEntity(hd));
			//hostDemandRepository.save(hostDemandMapper.toEntity(hd));
		}
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, demandDTO.getId().toString()))
            .body(result);
    }
    
}
