package com.kaylene.supervision.web.rest.vm;

public class KeyAndLoginVM {

	private String key;

	private String newLogin, oldLogin;
	

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getNewLogin() {
		return newLogin;
	}

	public void setNewLogin(String newLogin) {
		this.newLogin = newLogin;
	}

	public String getOldLogin() {
		return oldLogin;
	}

	public void setOldLogin(String oldLogin) {
		this.oldLogin = oldLogin;
	}

 
}
