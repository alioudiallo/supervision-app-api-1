package com.kaylene.supervision.web.rest.vm;

import com.kaylene.supervision.service.dto.UserDTO;

import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.Set;

/**
 * View Model extending the UserDTO, which is meant to be used in the user
 * management UI.
 */
public class ManagedUserVM extends UserDTO {

	public static final int PASSWORD_MIN_LENGTH = 4;

	public static final int PASSWORD_MAX_LENGTH = 100;

	@Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
	private String password;

	private String login;

	private String newLogin;

	private Boolean accesFacture;

	public ManagedUserVM() {
		// Empty constructor needed for Jackson.
	}

	public ManagedUserVM(Long id, String login, String password, String firstName, String lastName, String email,
			boolean activated, String imageUrl, String langKey, Long companyId, String createdBy, Instant createdDate,
			String lastModifiedBy, Instant lastModifiedDate, Set<String> authorities) {

		super(id, login, password, firstName, lastName, email, activated, imageUrl, langKey, companyId, createdBy,
				createdDate, lastModifiedBy, lastModifiedDate, authorities);
		this.password = password;
		this.login = login;
	}

	public String getNewLogin() {
		return newLogin;
	}

	public void setNewLogin(String newLogin) {
		this.newLogin = newLogin;
	}

	@Override
	public String toString() {
		return "ManagedUserVM{" + "} " + super.toString();
	}

	public Boolean getAccesFacture() {
		return accesFacture;
	}

	public void setAccesFacture(Boolean accesFacture) {
		this.accesFacture = accesFacture;
	}
}
