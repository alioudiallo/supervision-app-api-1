/**
 * View Models used by Spring MVC REST controllers.
 */
package com.kaylene.supervision.web.rest.vm;
