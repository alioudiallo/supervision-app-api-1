package com.kaylene.supervision.web.websockets;

import com.kaylene.supervision.domain.Dashboard;
import com.kaylene.supervision.domain.DashboardItem;
import com.kaylene.supervision.service.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Function;
import java.util.stream.Collectors;

/** 
 * Démo with crontab upated, decomment @controller  annotation for local test
 * 
 */

// @Controller
public class DashboardController {

	@Autowired
	DashboardService dashboardService;

	@Autowired
	SimpMessagingTemplate template;

	// le client STOMP envoie ses messages sur /server/websocket/dashboard
	// le prefix /server/websocket a été configuré dans WebSocketConfig
	// @MessageMapping("/dashboard") // not used yet
	public void updateDashboard(Long companyId) {
		Dashboard dashboard = dashboardService.getDashboard(companyId);	
		this.template.convertAndSend("/client/websocket/dashboard/" + companyId, simulateDshbordUpdate(dashboard));
	}

	public Dashboard simulateDshbordUpdate(Dashboard dashboard) {
		Function<DashboardItem, DashboardItem> mapper = x -> {
			x.setNumberOfTickets(x.getNumberOfTickets() + ThreadLocalRandom.current().nextInt());
			x.setColor("c-orange");
			return x;
		};
		List<DashboardItem> filteredList = dashboard.getDashBoardItems().stream().map(mapper)
				.collect(Collectors.toList());
		Dashboard UpdatedDashboard = new Dashboard();
		UpdatedDashboard.setDashBoardItems(filteredList);
		System.out.println("================================ " + UpdatedDashboard.getDashBoardItems().get(0).getNumberOfTickets());
		return UpdatedDashboard;
	}

	@Scheduled(fixedRate = 30000)
	public void reportCurrentTime() {
		Long companyId = 2L;
		updateDashboard(companyId);
	}

}
