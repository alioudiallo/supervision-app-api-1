package com.kaylene.supervision.zabbix.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Mamadou Lamine NIANG
 **/
@Data
@NoArgsConstructor
public class ZabbixScreen {

    @JsonProperty("screenid")
    private String id;
    private String name;
    private int hsize;
    private int vize;
    @JsonProperty("userid")
    private int userId;
    @JsonProperty("private")
    private int isPrivate;
}
