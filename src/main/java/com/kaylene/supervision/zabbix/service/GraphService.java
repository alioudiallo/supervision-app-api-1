package com.kaylene.supervision.zabbix.service;

import com.kaylene.supervision.config.ZabbixApiProperties;
import com.kaylene.supervision.zabbix.GraphGetParams;
import com.kaylene.supervision.zabbix.dto.ZabbixScreenDTO;
import com.kaylene.supervision.zabbix.exception.ZabbixApiException;
import com.kaylene.supervision.zabbix.model.ZabbixScreenItem;
import com.kaylene.supervision.zabbix.response.JsonRPCResponse;
import com.kaylene.supervision.zabbix.service.ZabbixApiService;
import com.kaylene.supervision.zabbix.utils.JsonMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Mamadou Lamine NIANG
 **/
@Service
@Slf4j
@RequiredArgsConstructor
public class GraphService {

    private final ZabbixApiService zabbixApiService;
    private final JsonMapper jsonMapper;
    private final ZabbixApiProperties zabbixApiProperties;

    public List<ZabbixScreenDTO> getScreens(String login, String password) throws ZabbixApiException {
        String auth = zabbixApiService.authenticate(login, password);
        GraphGetParams params = new GraphGetParams();
        params.setOutput("extend");

        JsonRPCResponse response = zabbixApiService.call("screen.get", params, auth);
        List<ZabbixScreenDTO> screens = jsonMapper.getList(response.getResult(), ZabbixScreenDTO.class);
        for(ZabbixScreenDTO dto: screens) {
            List<ZabbixScreenItem> items = dto.getItems();
            dto.setItems(items.stream()
                    .filter(item -> item.getResourceType() < 2)
                    .collect(Collectors.toList()));
        }
        return screens;
    }

    public Optional<ZabbixScreenDTO> getScreen(String login, String password, String screenId) throws ZabbixApiException {
        return getScreens(login, password).stream()
            .filter(zabbixScreenDTO -> screenId.equals(zabbixScreenDTO.getId()))
            .findFirst();
    }

    public String getCookie(String login, String password) throws ZabbixApiException {
        //PHPSESSID
        RestTemplate restTemplate = new RestTemplate();
        String url = zabbixApiProperties.getUrl() + "/index.php";
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(url, String.class);
        if(200 != responseEntity.getStatusCodeValue()) {
            throw new ZabbixApiException("Unable to get homepage");
        }
        HttpHeaders headers = responseEntity.getHeaders();
        if(!headers.containsKey("Set-Cookie")) {
            throw new ZabbixApiException("Set-Cookie header not found");
        }
        String setCookieHeader = headers.getFirst("Set-Cookie");
        if(!setCookieHeader.contains("PHPSESSID")) {
            throw new ZabbixApiException("Incorrect Set-Cookie value");
        }
        String phpSessionId = setCookieHeader.split(";")[0];

        //zbx_sessionid
        headers = new HttpHeaders();
        headers.set("Cookie", phpSessionId);
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("name", login);
        params.add("password", password);
        params.add("enter", "Sign in");
        HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(params, headers);
        responseEntity = restTemplate.postForEntity(url, httpEntity, String.class);
        if(302 != responseEntity.getStatusCodeValue()) {
            throw new ZabbixApiException("Unable to login to homepage");
        }
        headers = responseEntity.getHeaders();
        if(!headers.containsKey("Set-Cookie")) {
            throw new ZabbixApiException("Set-Cookie header not found");
        }
        setCookieHeader = headers.getFirst("Set-Cookie");
        if(!setCookieHeader.contains("zbx_sessionid")) {
            throw new ZabbixApiException("Incorrect Set-Cookie value");
        }
        return String.format("%s; %s", phpSessionId, setCookieHeader.split(";")[0]);
    }

    private String buildImgUrl(ZabbixScreenItem item) {
        boolean isPieChart = item.getHeight() == 300;
        String url = zabbixApiProperties.getUrl();
        if(isPieChart) {
            url += "/chart6.php";
        } else {
            url += "/chart2.php";
        }
        url = url + "?graphid=" + item.getResourceId() +
            "&screenid=" + item.getScreenId() +
            "&width=" + item.getWidth() +
            "&height=" + item.getHeight() +
            "&legend=1" +
            "&profileIdx=web.screens.filter" +
            "&profileIdx2=" + item.getScreenId() +
            "&from=now-" + item.getPeriod() +
            "&to=now";

        if(isPieChart) {
            url += "&graph3d=1";
        }
        return url;
    }

    public String getGraphAsBase64(String cookie, ZabbixScreenItem item) throws ZabbixApiException {
        String url = buildImgUrl(item);
        RestTemplate restTemplate = new RestTemplate();
        List<HttpMessageConverter<?>> converters = new ArrayList<>();
        converters.add(new ByteArrayHttpMessageConverter());
        restTemplate.setMessageConverters(converters);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Cookie", cookie);
        headers.setAccept(Collections.singletonList(MediaType.IMAGE_PNG));

        HttpEntity<?> httpEntity = new HttpEntity<>(null, headers);

        ResponseEntity<byte[]> response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, byte[].class);
        if(200 != response.getStatusCodeValue()) {
            throw new ZabbixApiException("Error getting image");
        }
        if(!response.hasBody()) {
            throw new ZabbixApiException("No image found");
        }

        return Base64Utils.encodeToString(response.getBody());
    }

}
