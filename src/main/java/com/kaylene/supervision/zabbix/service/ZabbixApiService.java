package com.kaylene.supervision.zabbix.service;

import com.kaylene.supervision.config.ZabbixApiProperties;
import com.kaylene.supervision.zabbix.dto.ZabbixAuthDTO;
import com.kaylene.supervision.zabbix.exception.ZabbixApiException;
import com.kaylene.supervision.zabbix.request.JsonRPCRequest;
import com.kaylene.supervision.zabbix.response.JsonRPCResponse;
import com.kaylene.supervision.zabbix.utils.JsonMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.Collections;
import java.util.Objects;


/**
 * Main service making requests to Zabbix API.
 * @author Mamadou Lamine NIANG
 **/
@Service
@Slf4j
public class ZabbixApiService {

    public static final String API_ENDPOINT = "/api_jsonrpc.php";

    private final ZabbixApiProperties properties;
    private final JsonMapper jsonMapper;
    private final RestTemplate restTemplate;
    private final HttpHeaders headers;

    public ZabbixApiService(ZabbixApiProperties zabbixApiProperties, JsonMapper jsonMapper, RestTemplate restTemplate) {
        this.properties = zabbixApiProperties;
        this.jsonMapper = jsonMapper;
        this.restTemplate = restTemplate;
        headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
    }

    public JsonRPCResponse call(String method, Object params, String auth) throws ZabbixApiException {
        JsonRPCRequest request = new JsonRPCRequest();
        request.setAuth(auth);
        request.setParams(params);
        request.setMethod(method);
        return call(request);
    }

    /**
     * Makes a call to Zabbix API
     * @param request               The request object.
     * @return                      A JSON RPC 2.0 response object.
     * @throws ZabbixApiException   When the response status is not 200 or the API returned an error.
     */
    public JsonRPCResponse call(@Valid JsonRPCRequest request) throws ZabbixApiException {
        HttpEntity<JsonRPCRequest> httpEntity = new HttpEntity<>(request, headers);
        String apiUrl = properties.getUrl() + API_ENDPOINT;
        log.debug("Making request to {} with body: {}", apiUrl, request);
        try {
            ResponseEntity<JsonRPCResponse> response = restTemplate.postForEntity(apiUrl, httpEntity, JsonRPCResponse.class);
            if(response.getStatusCodeValue() != 200) {
                throw new ZabbixApiException(response.getStatusCodeValue());
            }
            if(!response.hasBody()) {
                throw new ZabbixApiException("Empty body received!");
            }
            JsonRPCResponse body = response.getBody();
            if(Objects.requireNonNull(body).isError()) {
                throw new ZabbixApiException(body.getError());
            }
            return body;
        } catch (RestClientException e) {
            throw new ZabbixApiException("Error making request to Zabbix Server", e);
        }
    }

    /**
     * Gets an authentication token from Zabbix
     * @param user                  The Zabbix user
     * @param password              The password
     * @return                      The auth token
     * @throws ZabbixApiException   When the response status is not 200 or the API returned an error.
     */
    public String authenticate(String user, String password) throws ZabbixApiException {
        JsonRPCRequest request = new JsonRPCRequest();
        request.setMethod("user.login");
        request.setParams(new ZabbixAuthDTO(user, password));

        JsonRPCResponse response = call(request);

        return jsonMapper.getObject(response.getResult(), String.class);
    }
}
