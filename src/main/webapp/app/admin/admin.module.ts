import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SupervisionapiSharedModule } from '../shared';
/* jhipster-needle-add-admin-module-import - JHipster will add admin modules imports here */

import {
    adminState,
    AuditsComponent,
    UserMgmtComponent,
    UserDialogComponent,
    UserDeleteDialogComponent,
    UserMgmtDetailComponent,
    UserMgmtDialogComponent,
    UserMgmtDeleteDialogComponent,
    LogsComponent,
    CustomMetricsMonitoringModalComponent,
    CustomMetricsMonitoringComponent,
    CustomHealthModalComponent,
    CustomHealthCheckComponent,
    CustomConfigurationComponent,
    CustomDocsComponent,
    AuditsService,
    CustomConfigurationService,
    CustomHealthService,
    CustomMetricsService,
    LogsService,
    UserResolvePagingParams,
    UserResolve,
    UserModalService
} from './';

@NgModule({
    imports: [
        SupervisionapiSharedModule,
        RouterModule.forRoot(adminState, { useHash: true }),
        /* jhipster-needle-add-admin-module - JHipster will add admin modules here */
    ],
    declarations: [
        AuditsComponent,
        UserMgmtComponent,
        UserDialogComponent,
        UserDeleteDialogComponent,
        UserMgmtDetailComponent,
        UserMgmtDialogComponent,
        UserMgmtDeleteDialogComponent,
        LogsComponent,
        CustomConfigurationComponent,
        CustomHealthCheckComponent,
        CustomHealthModalComponent,
        CustomDocsComponent,
        CustomMetricsMonitoringComponent,
        CustomMetricsMonitoringModalComponent
    ],
    entryComponents: [
        UserMgmtDialogComponent,
        UserMgmtDeleteDialogComponent,
        CustomHealthModalComponent,
        CustomMetricsMonitoringModalComponent,
    ],
    providers: [
        AuditsService,
        CustomConfigurationService,
        CustomHealthService,
        CustomMetricsService,
        LogsService,
        UserResolvePagingParams,
        UserResolve,
        UserModalService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SupervisionapiAdminModule {}
