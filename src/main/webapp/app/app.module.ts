import './vendor.ts';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2Webstorage } from 'ng2-webstorage';

import { SupervisionapiSharedModule, UserRouteAccessService } from './shared';
import { SupervisionapiHomeModule } from './home/home.module';
import { SupervisionapiAdminModule } from './admin/admin.module';
import { SupervisionapiAccountModule } from './account/account.module';
import { SupervisionapiEntityModule } from './entities/entity.module';

import { customHttpProvider } from './blocks/interceptor/http.provider';
import { PaginationConfig } from './blocks/config/uib-pagination.config';

// jhipster-needle-angular-add-module-import JHipster will add new module here

import {
    CustomMainComponent,
    LayoutRoutingModule,
    NavbarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ErrorComponent
} from './layouts';

@NgModule({
    imports: [
        BrowserModule,
        LayoutRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        SupervisionapiSharedModule,
        SupervisionapiHomeModule,
        SupervisionapiAdminModule,
        SupervisionapiAccountModule,
        SupervisionapiEntityModule,
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        CustomMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        FooterComponent
    ],
    providers: [
        ProfileService,
        customHttpProvider(),
        PaginationConfig,
        UserRouteAccessService
    ],
    bootstrap: [ CustomMainComponent ]
})
export class SupervisionapiAppModule {}
