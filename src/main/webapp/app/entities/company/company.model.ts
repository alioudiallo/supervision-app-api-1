import { BaseEntity } from './../../shared';

export class Company implements BaseEntity {
    constructor(
        public id?: number,
        public login?: string,
        public name?: string,
        public website?: string,
        public address?: string,
        public phone?: string,
        public contactPersons?: BaseEntity[],
        public contracts?: BaseEntity[],
        public sites?: BaseEntity[],
    ) {
    }
}
