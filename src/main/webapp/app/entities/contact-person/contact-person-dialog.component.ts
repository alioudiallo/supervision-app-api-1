import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ContactPerson } from './contact-person.model';
import { ContactPersonPopupService } from './contact-person-popup.service';
import { ContactPersonService } from './contact-person.service';
import { Company, CompanyService } from '../company';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'custom-contact-person-dialog',
    templateUrl: './contact-person-dialog.component.html'
})
export class ContactPersonDialogComponent implements OnInit {

    contactPerson: ContactPerson;
    isSaving: boolean;

    companies: Company[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private contactPersonService: ContactPersonService,
        private companyService: CompanyService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.companyService.query()
            .subscribe((res: ResponseWrapper) => { this.companies = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.contactPerson.id !== undefined) {
            this.subscribeToSaveResponse(
                this.contactPersonService.update(this.contactPerson));
        } else {
            this.subscribeToSaveResponse(
                this.contactPersonService.create(this.contactPerson));
        }
    }

    private subscribeToSaveResponse(result: Observable<ContactPerson>) {
        result.subscribe((res: ContactPerson) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: ContactPerson) {
        this.eventManager.broadcast({ name: 'contactPersonListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackCompanyById(index: number, item: Company) {
        return item.id;
    }
}

@Component({
    selector: 'custom-contact-person-popup',
    template: ''
})
export class ContactPersonPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private contactPersonPopupService: ContactPersonPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.contactPersonPopupService
                    .open(ContactPersonDialogComponent as Component, params['id']);
            } else {
                this.contactPersonPopupService
                    .open(ContactPersonDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
