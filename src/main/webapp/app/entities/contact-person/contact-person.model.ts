import { BaseEntity } from './../../shared';

export class ContactPerson implements BaseEntity {
    constructor(
        public id?: number,
        public firtsname?: string,
        public lastname?: string,
        public email?: string,
        public mobilePhone?: string,
        public fixedPhone?: string,
        public companyId?: number,
    ) {
    }
}
