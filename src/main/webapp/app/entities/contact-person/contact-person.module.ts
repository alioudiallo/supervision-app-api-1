import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SupervisionapiSharedModule } from '../../shared';
import {
    ContactPersonService,
    ContactPersonPopupService,
    ContactPersonComponent,
    ContactPersonDetailComponent,
    ContactPersonDialogComponent,
    ContactPersonPopupComponent,
    ContactPersonDeletePopupComponent,
    ContactPersonDeleteDialogComponent,
    contactPersonRoute,
    contactPersonPopupRoute,
    ContactPersonResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...contactPersonRoute,
    ...contactPersonPopupRoute,
];

@NgModule({
    imports: [
        SupervisionapiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ContactPersonComponent,
        ContactPersonDetailComponent,
        ContactPersonDialogComponent,
        ContactPersonDeleteDialogComponent,
        ContactPersonPopupComponent,
        ContactPersonDeletePopupComponent,
    ],
    entryComponents: [
        ContactPersonComponent,
        ContactPersonDialogComponent,
        ContactPersonPopupComponent,
        ContactPersonDeleteDialogComponent,
        ContactPersonDeletePopupComponent,
    ],
    providers: [
        ContactPersonService,
        ContactPersonPopupService,
        ContactPersonResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SupervisionapiContactPersonModule {}
