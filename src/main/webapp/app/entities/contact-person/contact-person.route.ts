import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ContactPersonComponent } from './contact-person.component';
import { ContactPersonDetailComponent } from './contact-person-detail.component';
import { ContactPersonPopupComponent } from './contact-person-dialog.component';
import { ContactPersonDeletePopupComponent } from './contact-person-delete-dialog.component';

@Injectable()
export class ContactPersonResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const contactPersonRoute: Routes = [
    {
        path: 'contact-person',
        component: ContactPersonComponent,
        resolve: {
            'pagingParams': ContactPersonResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ContactPeople'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'contact-person/:id',
        component: ContactPersonDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ContactPeople'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const contactPersonPopupRoute: Routes = [
    {
        path: 'contact-person-new',
        component: ContactPersonPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ContactPeople'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'contact-person/:id/edit',
        component: ContactPersonPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ContactPeople'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'contact-person/:id/delete',
        component: ContactPersonDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ContactPeople'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
