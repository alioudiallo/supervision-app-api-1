import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { ContactPerson } from './contact-person.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ContactPersonService {

    private resourceUrl = SERVER_API_URL + 'api/kaylene/contact-people';

    constructor(private http: Http) { }

    create(contactPerson: ContactPerson): Observable<ContactPerson> {
        const copy = this.convert(contactPerson);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(contactPerson: ContactPerson): Observable<ContactPerson> {
        const copy = this.convert(contactPerson);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<ContactPerson> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to ContactPerson.
     */
    private convertItemFromServer(json: any): ContactPerson {
        const entity: ContactPerson = Object.assign(new ContactPerson(), json);
        return entity;
    }

    /**
     * Convert a ContactPerson to a JSON which can be sent to the server.
     */
    private convert(contactPerson: ContactPerson): ContactPerson {
        const copy: ContactPerson = Object.assign({}, contactPerson);
        return copy;
    }
}
