import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Contract } from './contract.model';
import { ContractPopupService } from './contract-popup.service';
import { ContractService } from './contract.service';
import { Company, CompanyService } from '../company';
import { Offer, OfferService } from '../offer';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'custom-contract-dialog',
    templateUrl: './contract-dialog.component.html'
})
export class ContractDialogComponent implements OnInit {

    contract: Contract;
    isSaving: boolean;

    companies: Company[];

    offers: Offer[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private contractService: ContractService,
        private companyService: CompanyService,
        private offerService: OfferService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.companyService.query()
            .subscribe((res: ResponseWrapper) => { this.companies = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.offerService
            .query({filter: 'contract-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.contract.offerId) {
                    this.offers = res.json;
                } else {
                    this.offerService
                        .find(this.contract.offerId)
                        .subscribe((subRes: Offer) => {
                            this.offers = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.contract.id !== undefined) {
            this.subscribeToSaveResponse(
                this.contractService.update(this.contract));
        } else {
            this.subscribeToSaveResponse(
                this.contractService.create(this.contract));
        }
    }

    private subscribeToSaveResponse(result: Observable<Contract>) {
        result.subscribe((res: Contract) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Contract) {
        this.eventManager.broadcast({ name: 'contractListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackCompanyById(index: number, item: Company) {
        return item.id;
    }

    trackOfferById(index: number, item: Offer) {
        return item.id;
    }
}

@Component({
    selector: 'custom-contract-popup',
    template: ''
})
export class ContractPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private contractPopupService: ContractPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.contractPopupService
                    .open(ContractDialogComponent as Component, params['id']);
            } else {
                this.contractPopupService
                    .open(ContractDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
