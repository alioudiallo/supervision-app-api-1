import { BaseEntity } from './../../shared';

export class DashboardItem implements BaseEntity {
    constructor(
        public id?: number,
        public hostType?: string,
        public numberOfEquipments?: string,
        public numberOfTickets?: string,
        public color?: string,
        public imgContentType?: string,
        public img?: any,
    ) {
    }
}
