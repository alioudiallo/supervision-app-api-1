import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SupervisionapiSharedModule } from '../../shared';
import {
    DashboardItemService,
    DashboardItemPopupService,
    DashboardItemComponent,
    DashboardItemDetailComponent,
    DashboardItemDialogComponent,
    DashboardItemPopupComponent,
    DashboardItemDeletePopupComponent,
    DashboardItemDeleteDialogComponent,
    dashboardItemRoute,
    dashboardItemPopupRoute,
} from './';

const ENTITY_STATES = [
    ...dashboardItemRoute,
    ...dashboardItemPopupRoute,
];

@NgModule({
    imports: [
        SupervisionapiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DashboardItemComponent,
        DashboardItemDetailComponent,
        DashboardItemDialogComponent,
        DashboardItemDeleteDialogComponent,
        DashboardItemPopupComponent,
        DashboardItemDeletePopupComponent,
    ],
    entryComponents: [
        DashboardItemComponent,
        DashboardItemDialogComponent,
        DashboardItemPopupComponent,
        DashboardItemDeleteDialogComponent,
        DashboardItemDeletePopupComponent,
    ],
    providers: [
        DashboardItemService,
        DashboardItemPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SupervisionapiDashboardItemModule {}
