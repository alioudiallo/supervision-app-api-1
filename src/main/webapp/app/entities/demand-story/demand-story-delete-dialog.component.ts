import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DemandStory } from './demand-story.model';
import { DemandStoryPopupService } from './demand-story-popup.service';
import { DemandStoryService } from './demand-story.service';

@Component({
    selector: 'custom-demand-story-delete-dialog',
    templateUrl: './demand-story-delete-dialog.component.html'
})
export class DemandStoryDeleteDialogComponent {

    demandStory: DemandStory;

    constructor(
        private demandStoryService: DemandStoryService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.demandStoryService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'demandStoryListModification',
                content: 'Deleted an demandStory'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'custom-demand-story-delete-popup',
    template: ''
})
export class DemandStoryDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private demandStoryPopupService: DemandStoryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.demandStoryPopupService
                .open(DemandStoryDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
