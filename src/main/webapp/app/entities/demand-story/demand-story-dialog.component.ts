import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { DemandStory } from './demand-story.model';
import { DemandStoryPopupService } from './demand-story-popup.service';
import { DemandStoryService } from './demand-story.service';
import { User, UserService } from '../../shared';
import { Demand, DemandService } from '../demand';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'custom-demand-story-dialog',
    templateUrl: './demand-story-dialog.component.html'
})
export class DemandStoryDialogComponent implements OnInit {

    demandStory: DemandStory;
    isSaving: boolean;

    users: User[];

    demands: Demand[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private demandStoryService: DemandStoryService,
        private userService: UserService,
        private demandService: DemandService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.userService.query()
            .subscribe((res: ResponseWrapper) => { this.users = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.demandService.query()
            .subscribe((res: ResponseWrapper) => { this.demands = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.demandStory.id !== undefined) {
            this.subscribeToSaveResponse(
                this.demandStoryService.update(this.demandStory));
        } else {
            this.subscribeToSaveResponse(
                this.demandStoryService.create(this.demandStory));
        }
    }

    private subscribeToSaveResponse(result: Observable<DemandStory>) {
        result.subscribe((res: DemandStory) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: DemandStory) {
        this.eventManager.broadcast({ name: 'demandStoryListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }

    trackDemandById(index: number, item: Demand) {
        return item.id;
    }
}

@Component({
    selector: 'custom-demand-story-popup',
    template: ''
})
export class DemandStoryPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private demandStoryPopupService: DemandStoryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.demandStoryPopupService
                    .open(DemandStoryDialogComponent as Component, params['id']);
            } else {
                this.demandStoryPopupService
                    .open(DemandStoryDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
