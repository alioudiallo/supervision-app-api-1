import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DemandStoryComponent } from './demand-story.component';
import { DemandStoryDetailComponent } from './demand-story-detail.component';
import { DemandStoryPopupComponent } from './demand-story-dialog.component';
import { DemandStoryDeletePopupComponent } from './demand-story-delete-dialog.component';

@Injectable()
export class DemandStoryResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const demandStoryRoute: Routes = [
    {
        path: 'demand-story',
        component: DemandStoryComponent,
        resolve: {
            'pagingParams': DemandStoryResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'DemandStories'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'demand-story/:id',
        component: DemandStoryDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'DemandStories'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const demandStoryPopupRoute: Routes = [
    {
        path: 'demand-story-new',
        component: DemandStoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'DemandStories'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'demand-story/:id/edit',
        component: DemandStoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'DemandStories'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'demand-story/:id/delete',
        component: DemandStoryDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'DemandStories'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
