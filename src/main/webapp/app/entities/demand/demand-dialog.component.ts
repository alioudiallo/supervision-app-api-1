import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Demand } from './demand.model';
import { DemandPopupService } from './demand-popup.service';
import { DemandService } from './demand.service';
import { Company, CompanyService } from '../company';
import { Contract, ContractService } from '../contract';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'custom-demand-dialog',
    templateUrl: './demand-dialog.component.html'
})
export class DemandDialogComponent implements OnInit {

    demand: Demand;
    isSaving: boolean;

    companies: Company[];

    contracts: Contract[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private demandService: DemandService,
        private companyService: CompanyService,
        private contractService: ContractService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.companyService.query()
            .subscribe((res: ResponseWrapper) => { this.companies = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.contractService.query()
            .subscribe((res: ResponseWrapper) => { this.contracts = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.demand.id !== undefined) {
            this.subscribeToSaveResponse(
                this.demandService.update(this.demand));
        } else {
            this.subscribeToSaveResponse(
                this.demandService.create(this.demand));
        }
    }

    private subscribeToSaveResponse(result: Observable<Demand>) {
        result.subscribe((res: Demand) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Demand) {
        this.eventManager.broadcast({ name: 'demandListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackCompanyById(index: number, item: Company) {
        return item.id;
    }

    trackContractById(index: number, item: Contract) {
        return item.id;
    }
}

@Component({
    selector: 'custom-demand-popup',
    template: ''
})
export class DemandPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private demandPopupService: DemandPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.demandPopupService
                    .open(DemandDialogComponent as Component, params['id']);
            } else {
                this.demandPopupService
                    .open(DemandDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
