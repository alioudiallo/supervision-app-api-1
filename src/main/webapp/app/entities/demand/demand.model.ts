import { BaseEntity } from './../../shared';

export const enum DemandType {
    'ADD',
    'DELETE',
    'UPDATE'
}

export const enum DemandStatus {
    'REQUESTED',
    'ACCEPTED',
    'REJECTED',
    'DONE',
    'INCOMPLETE'
}

export class Demand implements BaseEntity {
    constructor(
        public id?: number,
        public type?: DemandType,
        public currentStatus?: DemandStatus,
        public description?: string,
        public createdDate?: any,
        public lastModifiedDate?: any,
        public hostsToAdds?: BaseEntity[],
        public companyId?: number,
        public contractId?: number,
    ) {
    }
}
