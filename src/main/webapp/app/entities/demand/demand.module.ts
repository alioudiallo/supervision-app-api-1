import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SupervisionapiSharedModule } from '../../shared';
import {
    DemandService,
    DemandPopupService,
    DemandComponent,
    DemandDetailComponent,
    DemandDialogComponent,
    DemandPopupComponent,
    DemandDeletePopupComponent,
    DemandDeleteDialogComponent,
    demandRoute,
    demandPopupRoute,
    DemandResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...demandRoute,
    ...demandPopupRoute,
];

@NgModule({
    imports: [
        SupervisionapiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DemandComponent,
        DemandDetailComponent,
        DemandDialogComponent,
        DemandDeleteDialogComponent,
        DemandPopupComponent,
        DemandDeletePopupComponent,
    ],
    entryComponents: [
        DemandComponent,
        DemandDialogComponent,
        DemandPopupComponent,
        DemandDeleteDialogComponent,
        DemandDeletePopupComponent,
    ],
    providers: [
        DemandService,
        DemandPopupService,
        DemandResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SupervisionapiDemandModule {}
