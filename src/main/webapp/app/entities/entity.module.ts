import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { SupervisionapiCompanyModule } from './company/company.module';
import { SupervisionapiContactPersonModule } from './contact-person/contact-person.module';
import { SupervisionapiContractModule } from './contract/contract.module';
import { SupervisionapiHostContractModule } from './host-contract/host-contract.module';
import { SupervisionapiOfferModule } from './offer/offer.module';
import { SupervisionapiSiteModule } from './site/site.module';
import { SupervisionapiDemandModule } from './demand/demand.module';
import { SupervisionapiDemandStoryModule } from './demand-story/demand-story.module';
import { SupervisionapiHostDemandModule } from './host-demand/host-demand.module';
import { SupervisionapiTicketModule } from './ticket/ticket.module';
import { SupervisionapiTicketStoryModule } from './ticket-story/ticket-story.module';
import { SupervisionapiHostTypeModule } from './host-type/host-type.module';
import { SupervisionapiDashboardItemModule } from './dashboard-item/dashboard-item.module';
import { SupervisionapiIncidentTypeModule } from './incident-type/incident-type.module';
import { SupervisionapiTicketMessageModule } from './ticket-message/ticket-message.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        SupervisionapiCompanyModule,
        SupervisionapiContactPersonModule,
        SupervisionapiContractModule,
        SupervisionapiHostContractModule,
        SupervisionapiOfferModule,
        SupervisionapiSiteModule,
        SupervisionapiDemandModule,
        SupervisionapiDemandStoryModule,
        SupervisionapiHostDemandModule,
        SupervisionapiTicketModule,
        SupervisionapiTicketStoryModule,
        SupervisionapiHostTypeModule,
        SupervisionapiDashboardItemModule,
        SupervisionapiIncidentTypeModule,
        SupervisionapiTicketMessageModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SupervisionapiEntityModule {}
