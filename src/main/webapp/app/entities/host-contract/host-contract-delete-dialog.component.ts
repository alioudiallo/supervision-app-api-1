import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { HostContract } from './host-contract.model';
import { HostContractPopupService } from './host-contract-popup.service';
import { HostContractService } from './host-contract.service';

@Component({
    selector: 'custom-host-contract-delete-dialog',
    templateUrl: './host-contract-delete-dialog.component.html'
})
export class HostContractDeleteDialogComponent {

    hostContract: HostContract;

    constructor(
        private hostContractService: HostContractService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.hostContractService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'hostContractListModification',
                content: 'Deleted an hostContract'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'custom-host-contract-delete-popup',
    template: ''
})
export class HostContractDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private hostContractPopupService: HostContractPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.hostContractPopupService
                .open(HostContractDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
