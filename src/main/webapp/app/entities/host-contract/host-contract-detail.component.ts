import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { HostContract } from './host-contract.model';
import { HostContractService } from './host-contract.service';

@Component({
    selector: 'custom-host-contract-detail',
    templateUrl: './host-contract-detail.component.html'
})
export class HostContractDetailComponent implements OnInit, OnDestroy {

    hostContract: HostContract;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private hostContractService: HostContractService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInHostContracts();
    }

    load(id) {
        this.hostContractService.find(id).subscribe((hostContract) => {
            this.hostContract = hostContract;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInHostContracts() {
        this.eventSubscriber = this.eventManager.subscribe(
            'hostContractListModification',
            (response) => this.load(this.hostContract.id)
        );
    }
}
