import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { HostContract } from './host-contract.model';
import { HostContractPopupService } from './host-contract-popup.service';
import { HostContractService } from './host-contract.service';
import { Contract, ContractService } from '../contract';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'custom-host-contract-dialog',
    templateUrl: './host-contract-dialog.component.html'
})
export class HostContractDialogComponent implements OnInit {

    hostContract: HostContract;
    isSaving: boolean;

    contracts: Contract[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private hostContractService: HostContractService,
        private contractService: ContractService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.contractService.query()
            .subscribe((res: ResponseWrapper) => { this.contracts = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.hostContract.id !== undefined) {
            this.subscribeToSaveResponse(
                this.hostContractService.update(this.hostContract));
        } else {
            this.subscribeToSaveResponse(
                this.hostContractService.create(this.hostContract));
        }
    }

    private subscribeToSaveResponse(result: Observable<HostContract>) {
        result.subscribe((res: HostContract) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: HostContract) {
        this.eventManager.broadcast({ name: 'hostContractListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackContractById(index: number, item: Contract) {
        return item.id;
    }
}

@Component({
    selector: 'custom-host-contract-popup',
    template: ''
})
export class HostContractPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private hostContractPopupService: HostContractPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.hostContractPopupService
                    .open(HostContractDialogComponent as Component, params['id']);
            } else {
                this.hostContractPopupService
                    .open(HostContractDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
