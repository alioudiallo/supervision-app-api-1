import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { HostDemand } from './host-demand.model';
import { HostDemandPopupService } from './host-demand-popup.service';
import { HostDemandService } from './host-demand.service';

@Component({
    selector: 'custom-host-demand-delete-dialog',
    templateUrl: './host-demand-delete-dialog.component.html'
})
export class HostDemandDeleteDialogComponent {

    hostDemand: HostDemand;

    constructor(
        private hostDemandService: HostDemandService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.hostDemandService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'hostDemandListModification',
                content: 'Deleted an hostDemand'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'custom-host-demand-delete-popup',
    template: ''
})
export class HostDemandDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private hostDemandPopupService: HostDemandPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.hostDemandPopupService
                .open(HostDemandDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
