import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { HostDemand } from './host-demand.model';
import { HostDemandPopupService } from './host-demand-popup.service';
import { HostDemandService } from './host-demand.service';
import { Demand, DemandService } from '../demand';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'custom-host-demand-dialog',
    templateUrl: './host-demand-dialog.component.html'
})
export class HostDemandDialogComponent implements OnInit {

    hostDemand: HostDemand;
    isSaving: boolean;

    demands: Demand[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private hostDemandService: HostDemandService,
        private demandService: DemandService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.demandService.query()
            .subscribe((res: ResponseWrapper) => { this.demands = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.hostDemand.id !== undefined) {
            this.subscribeToSaveResponse(
                this.hostDemandService.update(this.hostDemand));
        } else {
            this.subscribeToSaveResponse(
                this.hostDemandService.create(this.hostDemand));
        }
    }

    private subscribeToSaveResponse(result: Observable<HostDemand>) {
        result.subscribe((res: HostDemand) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: HostDemand) {
        this.eventManager.broadcast({ name: 'hostDemandListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackDemandById(index: number, item: Demand) {
        return item.id;
    }
}

@Component({
    selector: 'custom-host-demand-popup',
    template: ''
})
export class HostDemandPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private hostDemandPopupService: HostDemandPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.hostDemandPopupService
                    .open(HostDemandDialogComponent as Component, params['id']);
            } else {
                this.hostDemandPopupService
                    .open(HostDemandDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
