import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { HostDemand } from './host-demand.model';
import { HostDemandService } from './host-demand.service';

@Injectable()
export class HostDemandPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private hostDemandService: HostDemandService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.hostDemandService.find(id).subscribe((hostDemand) => {
                    hostDemand.dateIntegration = this.datePipe
                        .transform(hostDemand.dateIntegration, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.hostDemandModalRef(component, hostDemand);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.hostDemandModalRef(component, new HostDemand());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    hostDemandModalRef(component: Component, hostDemand: HostDemand): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.hostDemand = hostDemand;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
