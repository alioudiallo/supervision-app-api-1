import { BaseEntity } from './../../shared';

export const enum HostType {
    'ROUTER',
    'SWITCH',
    'FIREWALL',
    'SERVER',
    'WEBSERVER'
}

export class HostDemand implements BaseEntity {
    constructor(
        public id?: number,
        public zabbixId?: string,
        public label?: string,
        public ipAddress?: string,
        public siteId?: string,
        public type?: HostType,
        public codeSnmp?: string,
        public dateIntegration?: any,
        public demandId?: number,
    ) {
    }
}
