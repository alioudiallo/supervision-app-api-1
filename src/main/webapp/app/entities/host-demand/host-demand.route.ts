import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { HostDemandComponent } from './host-demand.component';
import { HostDemandDetailComponent } from './host-demand-detail.component';
import { HostDemandPopupComponent } from './host-demand-dialog.component';
import { HostDemandDeletePopupComponent } from './host-demand-delete-dialog.component';

@Injectable()
export class HostDemandResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const hostDemandRoute: Routes = [
    {
        path: 'host-demand',
        component: HostDemandComponent,
        resolve: {
            'pagingParams': HostDemandResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'HostDemands'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'host-demand/:id',
        component: HostDemandDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'HostDemands'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const hostDemandPopupRoute: Routes = [
    {
        path: 'host-demand-new',
        component: HostDemandPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'HostDemands'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'host-demand/:id/edit',
        component: HostDemandPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'HostDemands'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'host-demand/:id/delete',
        component: HostDemandDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'HostDemands'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
