import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { HostType } from './host-type.model';
import { HostTypeService } from './host-type.service';

@Component({
    selector: 'custom-host-type-detail',
    templateUrl: './host-type-detail.component.html'
})
export class HostTypeDetailComponent implements OnInit, OnDestroy {

    hostType: HostType;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private hostTypeService: HostTypeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInHostTypes();
    }

    load(id) {
        this.hostTypeService.find(id).subscribe((hostType) => {
            this.hostType = hostType;
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInHostTypes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'hostTypeListModification',
            (response) => this.load(this.hostType.id)
        );
    }
}
