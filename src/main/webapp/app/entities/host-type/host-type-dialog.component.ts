import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { HostType } from './host-type.model';
import { HostTypePopupService } from './host-type-popup.service';
import { HostTypeService } from './host-type.service';

@Component({
    selector: 'custom-host-type-dialog',
    templateUrl: './host-type-dialog.component.html'
})
export class HostTypeDialogComponent implements OnInit {

    hostType: HostType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private jhiAlertService: JhiAlertService,
        private hostTypeService: HostTypeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.hostType.id !== undefined) {
            this.subscribeToSaveResponse(
                this.hostTypeService.update(this.hostType));
        } else {
            this.subscribeToSaveResponse(
                this.hostTypeService.create(this.hostType));
        }
    }

    private subscribeToSaveResponse(result: Observable<HostType>) {
        result.subscribe((res: HostType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: HostType) {
        this.eventManager.broadcast({ name: 'hostTypeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'custom-host-type-popup',
    template: ''
})
export class HostTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private hostTypePopupService: HostTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.hostTypePopupService
                    .open(HostTypeDialogComponent as Component, params['id']);
            } else {
                this.hostTypePopupService
                    .open(HostTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
