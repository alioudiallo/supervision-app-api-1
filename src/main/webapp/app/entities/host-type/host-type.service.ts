import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { HostType } from './host-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class HostTypeService {

    private resourceUrl = SERVER_API_URL + 'api/host-types';

    constructor(private http: Http) { }

    create(hostType: HostType): Observable<HostType> {
        const copy = this.convert(hostType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(hostType: HostType): Observable<HostType> {
        const copy = this.convert(hostType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<HostType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to HostType.
     */
    private convertItemFromServer(json: any): HostType {
        const entity: HostType = Object.assign(new HostType(), json);
        return entity;
    }

    /**
     * Convert a HostType to a JSON which can be sent to the server.
     */
    private convert(hostType: HostType): HostType {
        const copy: HostType = Object.assign({}, hostType);
        return copy;
    }
}
