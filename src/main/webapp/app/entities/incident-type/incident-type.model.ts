import { BaseEntity } from './../../shared';

export class IncidentType implements BaseEntity {
    constructor(
        public id?: number,
        public key?: string,
        public label?: string,
    ) {
    }
}
