import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SupervisionapiSharedModule } from '../../shared';
import {
    IncidentTypeService,
    IncidentTypePopupService,
    IncidentTypeComponent,
    IncidentTypeDetailComponent,
    IncidentTypeDialogComponent,
    IncidentTypePopupComponent,
    IncidentTypeDeletePopupComponent,
    IncidentTypeDeleteDialogComponent,
    incidentTypeRoute,
    incidentTypePopupRoute,
} from './';

const ENTITY_STATES = [
    ...incidentTypeRoute,
    ...incidentTypePopupRoute,
];

@NgModule({
    imports: [
        SupervisionapiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        IncidentTypeComponent,
        IncidentTypeDetailComponent,
        IncidentTypeDialogComponent,
        IncidentTypeDeleteDialogComponent,
        IncidentTypePopupComponent,
        IncidentTypeDeletePopupComponent,
    ],
    entryComponents: [
        IncidentTypeComponent,
        IncidentTypeDialogComponent,
        IncidentTypePopupComponent,
        IncidentTypeDeleteDialogComponent,
        IncidentTypeDeletePopupComponent,
    ],
    providers: [
        IncidentTypeService,
        IncidentTypePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SupervisionapiIncidentTypeModule {}
