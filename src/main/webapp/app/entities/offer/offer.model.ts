import { BaseEntity } from './../../shared';

export const enum OfferType {
    'BASIC',
    'GOLD',
    'PRENIUM'
}

export class Offer implements BaseEntity {
    constructor(
        public id?: number,
        public label?: string,
        public maxHost?: number,
        public type?: OfferType,
    ) {
    }
}
