import { BaseEntity } from './../../shared';

export class Site implements BaseEntity {
    constructor(
        public id?: number,
        public zabbixProxyId?: string,
        public label?: string,
        public address?: string,
        public latitude?: string,
        public longitude?: string,
        public companyId?: number,
    ) {
    }
}
