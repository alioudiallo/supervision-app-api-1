import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TicketMessage } from './ticket-message.model';
import { TicketMessageService } from './ticket-message.service';

@Component({
    selector: 'custom-ticket-message-detail',
    templateUrl: './ticket-message-detail.component.html'
})
export class TicketMessageDetailComponent implements OnInit, OnDestroy {

    ticketMessage: TicketMessage;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private ticketMessageService: TicketMessageService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTicketMessages();
    }

    load(id) {
        this.ticketMessageService.find(id).subscribe((ticketMessage) => {
            this.ticketMessage = ticketMessage;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTicketMessages() {
        this.eventSubscriber = this.eventManager.subscribe(
            'ticketMessageListModification',
            (response) => this.load(this.ticketMessage.id)
        );
    }
}
