import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TicketMessage } from './ticket-message.model';
import { TicketMessagePopupService } from './ticket-message-popup.service';
import { TicketMessageService } from './ticket-message.service';

@Component({
    selector: 'custom-ticket-message-dialog',
    templateUrl: './ticket-message-dialog.component.html'
})
export class TicketMessageDialogComponent implements OnInit {

    ticketMessage: TicketMessage;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private ticketMessageService: TicketMessageService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.ticketMessage.id !== undefined) {
            this.subscribeToSaveResponse(
                this.ticketMessageService.update(this.ticketMessage));
        } else {
            this.subscribeToSaveResponse(
                this.ticketMessageService.create(this.ticketMessage));
        }
    }

    private subscribeToSaveResponse(result: Observable<TicketMessage>) {
        result.subscribe((res: TicketMessage) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: TicketMessage) {
        this.eventManager.broadcast({ name: 'ticketMessageListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'custom-ticket-message-popup',
    template: ''
})
export class TicketMessagePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private ticketMessagePopupService: TicketMessagePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.ticketMessagePopupService
                    .open(TicketMessageDialogComponent as Component, params['id']);
            } else {
                this.ticketMessagePopupService
                    .open(TicketMessageDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
