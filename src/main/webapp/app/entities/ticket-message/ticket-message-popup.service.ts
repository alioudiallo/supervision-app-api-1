import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { TicketMessage } from './ticket-message.model';
import { TicketMessageService } from './ticket-message.service';

@Injectable()
export class TicketMessagePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private ticketMessageService: TicketMessageService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.ticketMessageService.find(id).subscribe((ticketMessage) => {
                    ticketMessage.createdDate = this.datePipe
                        .transform(ticketMessage.createdDate, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.ticketMessageModalRef(component, ticketMessage);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.ticketMessageModalRef(component, new TicketMessage());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    ticketMessageModalRef(component: Component, ticketMessage: TicketMessage): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.ticketMessage = ticketMessage;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
