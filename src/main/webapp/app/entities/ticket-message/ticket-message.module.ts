import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SupervisionapiSharedModule } from '../../shared';
import {
    TicketMessageService,
    TicketMessagePopupService,
    TicketMessageComponent,
    TicketMessageDetailComponent,
    TicketMessageDialogComponent,
    TicketMessagePopupComponent,
    TicketMessageDeletePopupComponent,
    TicketMessageDeleteDialogComponent,
    ticketMessageRoute,
    ticketMessagePopupRoute,
} from './';

const ENTITY_STATES = [
    ...ticketMessageRoute,
    ...ticketMessagePopupRoute,
];

@NgModule({
    imports: [
        SupervisionapiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        TicketMessageComponent,
        TicketMessageDetailComponent,
        TicketMessageDialogComponent,
        TicketMessageDeleteDialogComponent,
        TicketMessagePopupComponent,
        TicketMessageDeletePopupComponent,
    ],
    entryComponents: [
        TicketMessageComponent,
        TicketMessageDialogComponent,
        TicketMessagePopupComponent,
        TicketMessageDeleteDialogComponent,
        TicketMessageDeletePopupComponent,
    ],
    providers: [
        TicketMessageService,
        TicketMessagePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SupervisionapiTicketMessageModule {}
