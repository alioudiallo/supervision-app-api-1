import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { TicketMessageComponent } from './ticket-message.component';
import { TicketMessageDetailComponent } from './ticket-message-detail.component';
import { TicketMessagePopupComponent } from './ticket-message-dialog.component';
import { TicketMessageDeletePopupComponent } from './ticket-message-delete-dialog.component';

export const ticketMessageRoute: Routes = [
    {
        path: 'ticket-message',
        component: TicketMessageComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TicketMessages'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'ticket-message/:id',
        component: TicketMessageDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TicketMessages'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const ticketMessagePopupRoute: Routes = [
    {
        path: 'ticket-message-new',
        component: TicketMessagePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TicketMessages'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ticket-message/:id/edit',
        component: TicketMessagePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TicketMessages'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ticket-message/:id/delete',
        component: TicketMessageDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TicketMessages'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
