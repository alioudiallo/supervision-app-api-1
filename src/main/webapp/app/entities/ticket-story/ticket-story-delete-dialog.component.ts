import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TicketStory } from './ticket-story.model';
import { TicketStoryPopupService } from './ticket-story-popup.service';
import { TicketStoryService } from './ticket-story.service';

@Component({
    selector: 'custom-ticket-story-delete-dialog',
    templateUrl: './ticket-story-delete-dialog.component.html'
})
export class TicketStoryDeleteDialogComponent {

    ticketStory: TicketStory;

    constructor(
        private ticketStoryService: TicketStoryService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.ticketStoryService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'ticketStoryListModification',
                content: 'Deleted an ticketStory'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'custom-ticket-story-delete-popup',
    template: ''
})
export class TicketStoryDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private ticketStoryPopupService: TicketStoryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.ticketStoryPopupService
                .open(TicketStoryDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
