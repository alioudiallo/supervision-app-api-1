import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TicketStory } from './ticket-story.model';
import { TicketStoryService } from './ticket-story.service';

@Component({
    selector: 'custom-ticket-story-detail',
    templateUrl: './ticket-story-detail.component.html'
})
export class TicketStoryDetailComponent implements OnInit, OnDestroy {

    ticketStory: TicketStory;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private ticketStoryService: TicketStoryService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTicketStories();
    }

    load(id) {
        this.ticketStoryService.find(id).subscribe((ticketStory) => {
            this.ticketStory = ticketStory;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTicketStories() {
        this.eventSubscriber = this.eventManager.subscribe(
            'ticketStoryListModification',
            (response) => this.load(this.ticketStory.id)
        );
    }
}
