import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SupervisionapiSharedModule } from '../../shared';
import { SupervisionapiAdminModule } from '../../admin/admin.module';
import {
    TicketStoryService,
    TicketStoryPopupService,
    TicketStoryComponent,
    TicketStoryDetailComponent,
    TicketStoryDialogComponent,
    TicketStoryPopupComponent,
    TicketStoryDeletePopupComponent,
    TicketStoryDeleteDialogComponent,
    ticketStoryRoute,
    ticketStoryPopupRoute,
    TicketStoryResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...ticketStoryRoute,
    ...ticketStoryPopupRoute,
];

@NgModule({
    imports: [
        SupervisionapiSharedModule,
        SupervisionapiAdminModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        TicketStoryComponent,
        TicketStoryDetailComponent,
        TicketStoryDialogComponent,
        TicketStoryDeleteDialogComponent,
        TicketStoryPopupComponent,
        TicketStoryDeletePopupComponent,
    ],
    entryComponents: [
        TicketStoryComponent,
        TicketStoryDialogComponent,
        TicketStoryPopupComponent,
        TicketStoryDeleteDialogComponent,
        TicketStoryDeletePopupComponent,
    ],
    providers: [
        TicketStoryService,
        TicketStoryPopupService,
        TicketStoryResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SupervisionapiTicketStoryModule {}
