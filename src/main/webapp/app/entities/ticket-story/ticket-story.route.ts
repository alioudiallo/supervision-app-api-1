import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { TicketStoryComponent } from './ticket-story.component';
import { TicketStoryDetailComponent } from './ticket-story-detail.component';
import { TicketStoryPopupComponent } from './ticket-story-dialog.component';
import { TicketStoryDeletePopupComponent } from './ticket-story-delete-dialog.component';

@Injectable()
export class TicketStoryResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const ticketStoryRoute: Routes = [
    {
        path: 'ticket-story',
        component: TicketStoryComponent,
        resolve: {
            'pagingParams': TicketStoryResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TicketStories'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'ticket-story/:id',
        component: TicketStoryDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TicketStories'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const ticketStoryPopupRoute: Routes = [
    {
        path: 'ticket-story-new',
        component: TicketStoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TicketStories'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ticket-story/:id/edit',
        component: TicketStoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TicketStories'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ticket-story/:id/delete',
        component: TicketStoryDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TicketStories'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
