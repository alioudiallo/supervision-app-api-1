import { BaseEntity } from './../../shared';

export const enum TicketStatus {
    'OPEN',
    'INPROGRESS',
    'CLOSE'
}

export const enum Severity {
    'MAJOR',
    'MINOR'
}

export class Ticket implements BaseEntity {
    constructor(
        public id?: number,
        public incidentType?: string,
        public incidentDate?: any,
        public trackIdZabbix?: string,
        public hostIdzabbix?: string,
        public description?: any,
        public status?: TicketStatus,
        public closeDate?: any,
        public severity?: Severity,
        public createdDate?: any,
        public modifiedDate?: any,
        public adminId?: number,
        public companyId?: number,
    ) {
    }
}
