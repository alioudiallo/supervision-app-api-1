import { NgModule, LOCALE_ID } from '@angular/core';
import { Title } from '@angular/platform-browser';

import {
    SupervisionapiSharedLibsModule,
    CustomAlertComponent,
    CustomAlertErrorComponent
} from './';

@NgModule({
    imports: [
        SupervisionapiSharedLibsModule
    ],
    declarations: [
        CustomAlertComponent,
        CustomAlertErrorComponent
    ],
    providers: [
        Title,
        {
            provide: LOCALE_ID,
            useValue: 'en'
        },
    ],
    exports: [
        SupervisionapiSharedLibsModule,
        CustomAlertComponent,
        CustomAlertErrorComponent
    ]
})
export class SupervisionapiSharedCommonModule {}
