//package com.kaylene.supervision.web.rest;
//
//import static com.kaylene.supervision.web.rest.TestUtil.createFormattingConversionService;
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.hamcrest.Matchers.hasItem;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import java.util.List;
//
//import javax.persistence.EntityManager;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.util.Base64Utils;
//
//import com.kaylene.supervision.SupervisionapiApp;
//import com.kaylene.supervision.domain.DashboardItem;
//import com.kaylene.supervision.repository.DashboardItemRepository;
//import com.kaylene.supervision.web.rest.errors.ExceptionTranslator;
//
///**
// * Test class for the DashboardItemResource REST controller.
// *
// * @see DashboardItemResource
// */
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = SupervisionapiApp.class)
//public class DashboardItemResourceIntTest {
//
//    private static final String DEFAULT_HOST_TYPE = "AAAAAAAAAA";
//    private static final String UPDATED_HOST_TYPE = "BBBBBBBBBB";
//
//    private static final int DEFAULT_NUMBER_OF_EQUIPMENTS = 1;
//    private static final int UPDATED_NUMBER_OF_EQUIPMENTS = 2;
//
//    private static final int DEFAULT_NUMBER_OF_TICKETS = 1;
//    private static final int UPDATED_NUMBER_OF_TICKETS = 2;
//
//    private static final String DEFAULT_COLOR = "AAAAAAAAAA";
//    private static final String UPDATED_COLOR = "BBBBBBBBBB";
//
//    private static final byte[] DEFAULT_IMG = TestUtil.createByteArray(1, "0");
//    private static final byte[] UPDATED_IMG = TestUtil.createByteArray(2, "1");
//    private static final String DEFAULT_IMG_CONTENT_TYPE = "image/jpg";
//    private static final String UPDATED_IMG_CONTENT_TYPE = "image/png";
//
//    @Autowired
//    private DashboardItemRepository dashboardItemRepository;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    private MockMvc restDashboardItemMockMvc;
//
//    private DashboardItem dashboardItem;
//
//    @Before
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final DashboardItemResource dashboardItemResource = new DashboardItemResource(dashboardItemRepository);
//        this.restDashboardItemMockMvc = MockMvcBuilders.standaloneSetup(dashboardItemResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static DashboardItem createEntity(EntityManager em) {
//        DashboardItem dashboardItem = new DashboardItem()
//            .hostType(DEFAULT_HOST_TYPE)
//            .numberOfEquipments(DEFAULT_NUMBER_OF_EQUIPMENTS)
//            .numberOfTickets(DEFAULT_NUMBER_OF_TICKETS)
//            .color(DEFAULT_COLOR);
//        return dashboardItem;
//    }
//
//    @Before
//    public void initTest() {
//        dashboardItem = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createDashboardItem() throws Exception {
//        int databaseSizeBeforeCreate = dashboardItemRepository.findAll().size();
//
//        // Create the DashboardItem
//        restDashboardItemMockMvc.perform(post("/api/dashboard-items")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(dashboardItem)))
//            .andExpect(status().isCreated());
//
//        // Validate the DashboardItem in the database
//        List<DashboardItem> dashboardItemList = dashboardItemRepository.findAll();
//        assertThat(dashboardItemList).hasSize(databaseSizeBeforeCreate + 1);
//        DashboardItem testDashboardItem = dashboardItemList.get(dashboardItemList.size() - 1);
//        assertThat(testDashboardItem.getHostType()).isEqualTo(DEFAULT_HOST_TYPE);
//        assertThat(testDashboardItem.getNumberOfEquipments()).isEqualTo(DEFAULT_NUMBER_OF_EQUIPMENTS);
//        assertThat(testDashboardItem.getNumberOfTickets()).isEqualTo(DEFAULT_NUMBER_OF_TICKETS);
//        assertThat(testDashboardItem.getColor()).isEqualTo(DEFAULT_COLOR);
//    }
//
//    @Test
//    @Transactional
//    public void createDashboardItemWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = dashboardItemRepository.findAll().size();
//
//        
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restDashboardItemMockMvc.perform(post("/api/dashboard-items")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(dashboardItem)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the DashboardItem in the database
//        List<DashboardItem> dashboardItemList = dashboardItemRepository.findAll();
//        assertThat(dashboardItemList).hasSize(databaseSizeBeforeCreate);
//    }
//
//    @Test
//    @Transactional
//    public void getAllDashboardItems() throws Exception {
//        // Initialize the database
//        dashboardItemRepository.saveAndFlush(dashboardItem);
//
//        // Get all the dashboardItemList
//        restDashboardItemMockMvc.perform(get("/api/dashboard-items?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].hostType").value(hasItem(DEFAULT_HOST_TYPE.toString())))
//            .andExpect(jsonPath("$.[*].numberOfEquipments").value(hasItem(DEFAULT_NUMBER_OF_EQUIPMENTS)))
//            .andExpect(jsonPath("$.[*].numberOfTickets").value(hasItem(DEFAULT_NUMBER_OF_TICKETS)))
//            .andExpect(jsonPath("$.[*].color").value(hasItem(DEFAULT_COLOR.toString())))
//            .andExpect(jsonPath("$.[*].imgContentType").value(hasItem(DEFAULT_IMG_CONTENT_TYPE)))
//            .andExpect(jsonPath("$.[*].img").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMG))));
//    }
//
//    @Test
//    @Transactional
//    public void getDashboardItem() throws Exception {
//        // Initialize the database
//        dashboardItemRepository.saveAndFlush(dashboardItem);
//
//        // Get the dashboardItem
//        restDashboardItemMockMvc.perform(get("/api/dashboard-items/{id}", dashboardItem.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.hostType").value(DEFAULT_HOST_TYPE.toString()))
//            .andExpect(jsonPath("$.numberOfEquipments").value(DEFAULT_NUMBER_OF_EQUIPMENTS))
//            .andExpect(jsonPath("$.numberOfTickets").value(DEFAULT_NUMBER_OF_TICKETS))
//            .andExpect(jsonPath("$.color").value(DEFAULT_COLOR.toString()))
//            .andExpect(jsonPath("$.imgContentType").value(DEFAULT_IMG_CONTENT_TYPE))
//            .andExpect(jsonPath("$.img").value(Base64Utils.encodeToString(DEFAULT_IMG)));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingDashboardItem() throws Exception {
//        // Get the dashboardItem
//        restDashboardItemMockMvc.perform(get("/api/dashboard-items/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateDashboardItem() throws Exception {
//        // Initialize the database
//        dashboardItemRepository.saveAndFlush(dashboardItem);
//        int databaseSizeBeforeUpdate = dashboardItemRepository.findAll().size();
//
//        // Update the dashboardItem
//        //DashboardItem updatedDashboardItem = dashboardItemRepository.findOne(dashboardItem.getId());
//        updatedDashboardItem
//            .hostType(UPDATED_HOST_TYPE)
//            .numberOfEquipments(UPDATED_NUMBER_OF_EQUIPMENTS)
//            .numberOfTickets(UPDATED_NUMBER_OF_TICKETS)
//            .color(UPDATED_COLOR)
//            .img(UPDATED_IMG)
//            .imgContentType(UPDATED_IMG_CONTENT_TYPE);
//
//        restDashboardItemMockMvc.perform(put("/api/dashboard-items")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(updatedDashboardItem)))
//            .andExpect(status().isOk());
//
//        // Validate the DashboardItem in the database
//        List<DashboardItem> dashboardItemList = dashboardItemRepository.findAll();
//        assertThat(dashboardItemList).hasSize(databaseSizeBeforeUpdate);
//        DashboardItem testDashboardItem = dashboardItemList.get(dashboardItemList.size() - 1);
//        assertThat(testDashboardItem.getHostType()).isEqualTo(UPDATED_HOST_TYPE);
//        assertThat(testDashboardItem.getNumberOfEquipments()).isEqualTo(UPDATED_NUMBER_OF_EQUIPMENTS);
//        assertThat(testDashboardItem.getNumberOfTickets()).isEqualTo(UPDATED_NUMBER_OF_TICKETS);
//        assertThat(testDashboardItem.getColor()).isEqualTo(UPDATED_COLOR);
//        assertThat(testDashboardItem.getImg()).isEqualTo(UPDATED_IMG);
//        assertThat(testDashboardItem.getImgContentType()).isEqualTo(UPDATED_IMG_CONTENT_TYPE);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingDashboardItem() throws Exception {
//        int databaseSizeBeforeUpdate = dashboardItemRepository.findAll().size();
//
//        // Create the DashboardItem
//
//        // If the entity doesn't have an ID, it will be created instead of just being updated
//        restDashboardItemMockMvc.perform(put("/api/dashboard-items")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(dashboardItem)))
//            .andExpect(status().isCreated());
//
//        // Validate the DashboardItem in the database
//        List<DashboardItem> dashboardItemList = dashboardItemRepository.findAll();
//        assertThat(dashboardItemList).hasSize(databaseSizeBeforeUpdate + 1);
//    }
//
//    @Test
//    @Transactional
//    public void deleteDashboardItem() throws Exception {
//        // Initialize the database
//        dashboardItemRepository.saveAndFlush(dashboardItem);
//        int databaseSizeBeforeDelete = dashboardItemRepository.findAll().size();
//
//        // Get the dashboardItem
//        restDashboardItemMockMvc.perform(delete("/api/dashboard-items/{id}", dashboardItem.getId())
//            .accept(TestUtil.APPLICATION_JSON_UTF8))
//            .andExpect(status().isOk());
//
//        // Validate the database is empty
//        List<DashboardItem> dashboardItemList = dashboardItemRepository.findAll();
//        assertThat(dashboardItemList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//
//    @Test
//    @Transactional
//    public void equalsVerifier() throws Exception {
//        TestUtil.equalsVerifier(DashboardItem.class);
//        DashboardItem dashboardItem1 = new DashboardItem();
//        dashboardItem1.setId(1L);
//        DashboardItem dashboardItem2 = new DashboardItem();
//        dashboardItem2.setId(dashboardItem1.getId());
//        assertThat(dashboardItem1).isEqualTo(dashboardItem2);
//        dashboardItem2.setId(2L);
//        assertThat(dashboardItem1).isNotEqualTo(dashboardItem2);
//        dashboardItem1.setId(null);
//        assertThat(dashboardItem1).isNotEqualTo(dashboardItem2);
//    }
//}
