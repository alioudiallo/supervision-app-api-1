import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Comptable e2e test', () => {

    let navBarPage: NavBarPage;
    let comptableDialogPage: ComptableDialogPage;
    let comptableComponentsPage: ComptableComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Comptables', () => {
        navBarPage.goToEntity('comptable');
        comptableComponentsPage = new ComptableComponentsPage();
        expect(comptableComponentsPage.getTitle()).toMatch(/Comptables/);

    });

    it('should load create Comptable dialog', () => {
        comptableComponentsPage.clickOnCreateButton();
        comptableDialogPage = new ComptableDialogPage();
        expect(comptableDialogPage.getModalTitle()).toMatch(/Create or edit a Comptable/);
        comptableDialogPage.close();
    });

    it('should create and save Comptables', () => {
        comptableComponentsPage.clickOnCreateButton();
        comptableDialogPage.setFirstNameInput('firstName');
        expect(comptableDialogPage.getFirstNameInput()).toMatch('firstName');
        comptableDialogPage.setLastNameInput('lastName');
        expect(comptableDialogPage.getLastNameInput()).toMatch('lastName');
        comptableDialogPage.setEmailInput('email');
        expect(comptableDialogPage.getEmailInput()).toMatch('email');
        comptableDialogPage.setPhoneNumberInput('phoneNumber');
        expect(comptableDialogPage.getPhoneNumberInput()).toMatch('phoneNumber');
        comptableDialogPage.setCreatedDateInput(12310020012301);
        expect(comptableDialogPage.getCreatedDateInput()).toMatch('2001-12-31T02:30');
        comptableDialogPage.setUpdatedDateInput(12310020012301);
        expect(comptableDialogPage.getUpdatedDateInput()).toMatch('2001-12-31T02:30');
        comptableDialogPage.setMailComptableInput('mailComptable');
        expect(comptableDialogPage.getMailComptableInput()).toMatch('mailComptable');
        comptableDialogPage.factureSelectLastOption();
        comptableDialogPage.save();
        expect(comptableDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ComptableComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('custom-comptable div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class ComptableDialogPage {
    modalTitle = element(by.css('h4#myComptableLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    firstNameInput = element(by.css('input#field_firstName'));
    lastNameInput = element(by.css('input#field_lastName'));
    emailInput = element(by.css('input#field_email'));
    phoneNumberInput = element(by.css('input#field_phoneNumber'));
    createdDateInput = element(by.css('input#field_createdDate'));
    updatedDateInput = element(by.css('input#field_updatedDate'));
    mailComptableInput = element(by.css('input#field_mailComptable'));
    factureSelect = element(by.css('select#field_facture'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setFirstNameInput = function (firstName) {
        this.firstNameInput.sendKeys(firstName);
    }

    getFirstNameInput = function () {
        return this.firstNameInput.getAttribute('value');
    }

    setLastNameInput = function (lastName) {
        this.lastNameInput.sendKeys(lastName);
    }

    getLastNameInput = function () {
        return this.lastNameInput.getAttribute('value');
    }

    setEmailInput = function (email) {
        this.emailInput.sendKeys(email);
    }

    getEmailInput = function () {
        return this.emailInput.getAttribute('value');
    }

    setPhoneNumberInput = function (phoneNumber) {
        this.phoneNumberInput.sendKeys(phoneNumber);
    }

    getPhoneNumberInput = function () {
        return this.phoneNumberInput.getAttribute('value');
    }

    setCreatedDateInput = function (createdDate) {
        this.createdDateInput.sendKeys(createdDate);
    }

    getCreatedDateInput = function () {
        return this.createdDateInput.getAttribute('value');
    }

    setUpdatedDateInput = function (updatedDate) {
        this.updatedDateInput.sendKeys(updatedDate);
    }

    getUpdatedDateInput = function () {
        return this.updatedDateInput.getAttribute('value');
    }

    setMailComptableInput = function (mailComptable) {
        this.mailComptableInput.sendKeys(mailComptable);
    }

    getMailComptableInput = function () {
        return this.mailComptableInput.getAttribute('value');
    }

    factureSelectLastOption = function () {
        this.factureSelect.all(by.tagName('option')).last().click();
    }

    factureSelectOption = function (option) {
        this.factureSelect.sendKeys(option);
    }

    getFactureSelect = function () {
        return this.factureSelect;
    }

    getFactureSelectedOption = function () {
        return this.factureSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
