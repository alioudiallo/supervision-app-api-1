import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('ContactPerson e2e test', () => {

    let navBarPage: NavBarPage;
    let contactPersonDialogPage: ContactPersonDialogPage;
    let contactPersonComponentsPage: ContactPersonComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load ContactPeople', () => {
        navBarPage.goToEntity('contact-person');
        contactPersonComponentsPage = new ContactPersonComponentsPage();
        expect(contactPersonComponentsPage.getTitle()).toMatch(/Contact People/);

    });

    it('should load create ContactPerson dialog', () => {
        contactPersonComponentsPage.clickOnCreateButton();
        contactPersonDialogPage = new ContactPersonDialogPage();
        expect(contactPersonDialogPage.getModalTitle()).toMatch(/Create or edit a Contact Person/);
        contactPersonDialogPage.close();
    });

    it('should create and save ContactPeople', () => {
        contactPersonComponentsPage.clickOnCreateButton();
        contactPersonDialogPage.setFirtsnameInput('firtsname');
        expect(contactPersonDialogPage.getFirtsnameInput()).toMatch('firtsname');
        contactPersonDialogPage.setLastnameInput('lastname');
        expect(contactPersonDialogPage.getLastnameInput()).toMatch('lastname');
        contactPersonDialogPage.setEmailInput('email');
        expect(contactPersonDialogPage.getEmailInput()).toMatch('email');
        contactPersonDialogPage.setMobilePhoneInput('mobilePhone');
        expect(contactPersonDialogPage.getMobilePhoneInput()).toMatch('mobilePhone');
        contactPersonDialogPage.setFixedPhoneInput('fixedPhone');
        expect(contactPersonDialogPage.getFixedPhoneInput()).toMatch('fixedPhone');
        contactPersonDialogPage.companySelectLastOption();
        contactPersonDialogPage.save();
        expect(contactPersonDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ContactPersonComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('custom-contact-person div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class ContactPersonDialogPage {
    modalTitle = element(by.css('h4#myContactPersonLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    firtsnameInput = element(by.css('input#field_firtsname'));
    lastnameInput = element(by.css('input#field_lastname'));
    emailInput = element(by.css('input#field_email'));
    mobilePhoneInput = element(by.css('input#field_mobilePhone'));
    fixedPhoneInput = element(by.css('input#field_fixedPhone'));
    companySelect = element(by.css('select#field_company'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setFirtsnameInput = function (firtsname) {
        this.firtsnameInput.sendKeys(firtsname);
    }

    getFirtsnameInput = function () {
        return this.firtsnameInput.getAttribute('value');
    }

    setLastnameInput = function (lastname) {
        this.lastnameInput.sendKeys(lastname);
    }

    getLastnameInput = function () {
        return this.lastnameInput.getAttribute('value');
    }

    setEmailInput = function (email) {
        this.emailInput.sendKeys(email);
    }

    getEmailInput = function () {
        return this.emailInput.getAttribute('value');
    }

    setMobilePhoneInput = function (mobilePhone) {
        this.mobilePhoneInput.sendKeys(mobilePhone);
    }

    getMobilePhoneInput = function () {
        return this.mobilePhoneInput.getAttribute('value');
    }

    setFixedPhoneInput = function (fixedPhone) {
        this.fixedPhoneInput.sendKeys(fixedPhone);
    }

    getFixedPhoneInput = function () {
        return this.fixedPhoneInput.getAttribute('value');
    }

    companySelectLastOption = function () {
        this.companySelect.all(by.tagName('option')).last().click();
    }

    companySelectOption = function (option) {
        this.companySelect.sendKeys(option);
    }

    getCompanySelect = function () {
        return this.companySelect;
    }

    getCompanySelectedOption = function () {
        return this.companySelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
