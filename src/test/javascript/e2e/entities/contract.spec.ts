import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Contract e2e test', () => {

    let navBarPage: NavBarPage;
    let contractDialogPage: ContractDialogPage;
    let contractComponentsPage: ContractComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Contracts', () => {
        navBarPage.goToEntity('contract');
        contractComponentsPage = new ContractComponentsPage();
        expect(contractComponentsPage.getTitle()).toMatch(/Contracts/);

    });

    it('should load create Contract dialog', () => {
        contractComponentsPage.clickOnCreateButton();
        contractDialogPage = new ContractDialogPage();
        expect(contractDialogPage.getModalTitle()).toMatch(/Create or edit a Contract/);
        contractDialogPage.close();
    });

    it('should create and save Contracts', () => {
        contractComponentsPage.clickOnCreateButton();
        contractDialogPage.currentStatusSelectLastOption();
        contractDialogPage.setCreateDateInput(12310020012301);
        expect(contractDialogPage.getCreateDateInput()).toMatch('2001-12-31T02:30');
        contractDialogPage.setBeginDateInput(12310020012301);
        expect(contractDialogPage.getBeginDateInput()).toMatch('2001-12-31T02:30');
        contractDialogPage.setEndDateInput(12310020012301);
        expect(contractDialogPage.getEndDateInput()).toMatch('2001-12-31T02:30');
        contractDialogPage.companySelectLastOption();
        contractDialogPage.offerSelectLastOption();
        contractDialogPage.save();
        expect(contractDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ContractComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('custom-contract div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class ContractDialogPage {
    modalTitle = element(by.css('h4#myContractLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    currentStatusSelect = element(by.css('select#field_currentStatus'));
    createDateInput = element(by.css('input#field_createDate'));
    beginDateInput = element(by.css('input#field_beginDate'));
    endDateInput = element(by.css('input#field_endDate'));
    companySelect = element(by.css('select#field_company'));
    offerSelect = element(by.css('select#field_offer'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setCurrentStatusSelect = function (currentStatus) {
        this.currentStatusSelect.sendKeys(currentStatus);
    }

    getCurrentStatusSelect = function () {
        return this.currentStatusSelect.element(by.css('option:checked')).getText();
    }

    currentStatusSelectLastOption = function () {
        this.currentStatusSelect.all(by.tagName('option')).last().click();
    }
    setCreateDateInput = function (createDate) {
        this.createDateInput.sendKeys(createDate);
    }

    getCreateDateInput = function () {
        return this.createDateInput.getAttribute('value');
    }

    setBeginDateInput = function (beginDate) {
        this.beginDateInput.sendKeys(beginDate);
    }

    getBeginDateInput = function () {
        return this.beginDateInput.getAttribute('value');
    }

    setEndDateInput = function (endDate) {
        this.endDateInput.sendKeys(endDate);
    }

    getEndDateInput = function () {
        return this.endDateInput.getAttribute('value');
    }

    companySelectLastOption = function () {
        this.companySelect.all(by.tagName('option')).last().click();
    }

    companySelectOption = function (option) {
        this.companySelect.sendKeys(option);
    }

    getCompanySelect = function () {
        return this.companySelect;
    }

    getCompanySelectedOption = function () {
        return this.companySelect.element(by.css('option:checked')).getText();
    }

    offerSelectLastOption = function () {
        this.offerSelect.all(by.tagName('option')).last().click();
    }

    offerSelectOption = function (option) {
        this.offerSelect.sendKeys(option);
    }

    getOfferSelect = function () {
        return this.offerSelect;
    }

    getOfferSelectedOption = function () {
        return this.offerSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
