import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Demand e2e test', () => {

    let navBarPage: NavBarPage;
    let demandDialogPage: DemandDialogPage;
    let demandComponentsPage: DemandComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Demands', () => {
        navBarPage.goToEntity('demand');
        demandComponentsPage = new DemandComponentsPage();
        expect(demandComponentsPage.getTitle()).toMatch(/Demands/);

    });

    it('should load create Demand dialog', () => {
        demandComponentsPage.clickOnCreateButton();
        demandDialogPage = new DemandDialogPage();
        expect(demandDialogPage.getModalTitle()).toMatch(/Create or edit a Demand/);
        demandDialogPage.close();
    });

    it('should create and save Demands', () => {
        demandComponentsPage.clickOnCreateButton();
        demandDialogPage.typeSelectLastOption();
        demandDialogPage.currentStatusSelectLastOption();
        demandDialogPage.setDescriptionInput('description');
        expect(demandDialogPage.getDescriptionInput()).toMatch('description');
        demandDialogPage.setCreatedDateInput(12310020012301);
        expect(demandDialogPage.getCreatedDateInput()).toMatch('2001-12-31T02:30');
        demandDialogPage.setLastModifiedDateInput(12310020012301);
        expect(demandDialogPage.getLastModifiedDateInput()).toMatch('2001-12-31T02:30');
        demandDialogPage.companySelectLastOption();
        demandDialogPage.contractSelectLastOption();
        demandDialogPage.save();
        expect(demandDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class DemandComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('custom-demand div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class DemandDialogPage {
    modalTitle = element(by.css('h4#myDemandLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    typeSelect = element(by.css('select#field_type'));
    currentStatusSelect = element(by.css('select#field_currentStatus'));
    descriptionInput = element(by.css('input#field_description'));
    createdDateInput = element(by.css('input#field_createdDate'));
    lastModifiedDateInput = element(by.css('input#field_lastModifiedDate'));
    companySelect = element(by.css('select#field_company'));
    contractSelect = element(by.css('select#field_contract'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setTypeSelect = function (type) {
        this.typeSelect.sendKeys(type);
    }

    getTypeSelect = function () {
        return this.typeSelect.element(by.css('option:checked')).getText();
    }

    typeSelectLastOption = function () {
        this.typeSelect.all(by.tagName('option')).last().click();
    }
    setCurrentStatusSelect = function (currentStatus) {
        this.currentStatusSelect.sendKeys(currentStatus);
    }

    getCurrentStatusSelect = function () {
        return this.currentStatusSelect.element(by.css('option:checked')).getText();
    }

    currentStatusSelectLastOption = function () {
        this.currentStatusSelect.all(by.tagName('option')).last().click();
    }
    setDescriptionInput = function (description) {
        this.descriptionInput.sendKeys(description);
    }

    getDescriptionInput = function () {
        return this.descriptionInput.getAttribute('value');
    }

    setCreatedDateInput = function (createdDate) {
        this.createdDateInput.sendKeys(createdDate);
    }

    getCreatedDateInput = function () {
        return this.createdDateInput.getAttribute('value');
    }

    setLastModifiedDateInput = function (lastModifiedDate) {
        this.lastModifiedDateInput.sendKeys(lastModifiedDate);
    }

    getLastModifiedDateInput = function () {
        return this.lastModifiedDateInput.getAttribute('value');
    }

    companySelectLastOption = function () {
        this.companySelect.all(by.tagName('option')).last().click();
    }

    companySelectOption = function (option) {
        this.companySelect.sendKeys(option);
    }

    getCompanySelect = function () {
        return this.companySelect;
    }

    getCompanySelectedOption = function () {
        return this.companySelect.element(by.css('option:checked')).getText();
    }

    contractSelectLastOption = function () {
        this.contractSelect.all(by.tagName('option')).last().click();
    }

    contractSelectOption = function (option) {
        this.contractSelect.sendKeys(option);
    }

    getContractSelect = function () {
        return this.contractSelect;
    }

    getContractSelectedOption = function () {
        return this.contractSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
