import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('HostContract e2e test', () => {

    let navBarPage: NavBarPage;
    let hostContractDialogPage: HostContractDialogPage;
    let hostContractComponentsPage: HostContractComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load HostContracts', () => {
        navBarPage.goToEntity('host-contract');
        hostContractComponentsPage = new HostContractComponentsPage();
        expect(hostContractComponentsPage.getTitle()).toMatch(/Host Contracts/);

    });

    it('should load create HostContract dialog', () => {
        hostContractComponentsPage.clickOnCreateButton();
        hostContractDialogPage = new HostContractDialogPage();
        expect(hostContractDialogPage.getModalTitle()).toMatch(/Create or edit a Host Contract/);
        hostContractDialogPage.close();
    });

    it('should create and save HostContracts', () => {
        hostContractComponentsPage.clickOnCreateButton();
        hostContractDialogPage.setZabbixHostIdInput('zabbixHostId');
        expect(hostContractDialogPage.getZabbixHostIdInput()).toMatch('zabbixHostId');
        hostContractDialogPage.contractSelectLastOption();
        hostContractDialogPage.save();
        expect(hostContractDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class HostContractComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('custom-host-contract div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class HostContractDialogPage {
    modalTitle = element(by.css('h4#myHostContractLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    zabbixHostIdInput = element(by.css('input#field_zabbixHostId'));
    contractSelect = element(by.css('select#field_contract'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setZabbixHostIdInput = function (zabbixHostId) {
        this.zabbixHostIdInput.sendKeys(zabbixHostId);
    }

    getZabbixHostIdInput = function () {
        return this.zabbixHostIdInput.getAttribute('value');
    }

    contractSelectLastOption = function () {
        this.contractSelect.all(by.tagName('option')).last().click();
    }

    contractSelectOption = function (option) {
        this.contractSelect.sendKeys(option);
    }

    getContractSelect = function () {
        return this.contractSelect;
    }

    getContractSelectedOption = function () {
        return this.contractSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
