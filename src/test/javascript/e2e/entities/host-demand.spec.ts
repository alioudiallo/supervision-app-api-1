import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('HostDemand e2e test', () => {

    let navBarPage: NavBarPage;
    let hostDemandDialogPage: HostDemandDialogPage;
    let hostDemandComponentsPage: HostDemandComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load HostDemands', () => {
        navBarPage.goToEntity('host-demand');
        hostDemandComponentsPage = new HostDemandComponentsPage();
        expect(hostDemandComponentsPage.getTitle()).toMatch(/Host Demands/);

    });

    it('should load create HostDemand dialog', () => {
        hostDemandComponentsPage.clickOnCreateButton();
        hostDemandDialogPage = new HostDemandDialogPage();
        expect(hostDemandDialogPage.getModalTitle()).toMatch(/Create or edit a Host Demand/);
        hostDemandDialogPage.close();
    });

    it('should create and save HostDemands', () => {
        hostDemandComponentsPage.clickOnCreateButton();
        hostDemandDialogPage.setZabbixIdInput('zabbixId');
        expect(hostDemandDialogPage.getZabbixIdInput()).toMatch('zabbixId');
        hostDemandDialogPage.setLabelInput('label');
        expect(hostDemandDialogPage.getLabelInput()).toMatch('label');
        hostDemandDialogPage.setIpAddressInput('ipAddress');
        expect(hostDemandDialogPage.getIpAddressInput()).toMatch('ipAddress');
        hostDemandDialogPage.setSiteIdInput('siteId');
        expect(hostDemandDialogPage.getSiteIdInput()).toMatch('siteId');
        hostDemandDialogPage.typeSelectLastOption();
        hostDemandDialogPage.setCodeSnmpInput('codeSnmp');
        expect(hostDemandDialogPage.getCodeSnmpInput()).toMatch('codeSnmp');
        hostDemandDialogPage.setDateIntegrationInput(12310020012301);
        expect(hostDemandDialogPage.getDateIntegrationInput()).toMatch('2001-12-31T02:30');
        hostDemandDialogPage.demandSelectLastOption();
        hostDemandDialogPage.save();
        expect(hostDemandDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class HostDemandComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('custom-host-demand div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class HostDemandDialogPage {
    modalTitle = element(by.css('h4#myHostDemandLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    zabbixIdInput = element(by.css('input#field_zabbixId'));
    labelInput = element(by.css('input#field_label'));
    ipAddressInput = element(by.css('input#field_ipAddress'));
    siteIdInput = element(by.css('input#field_siteId'));
    typeSelect = element(by.css('select#field_type'));
    codeSnmpInput = element(by.css('input#field_codeSnmp'));
    dateIntegrationInput = element(by.css('input#field_dateIntegration'));
    demandSelect = element(by.css('select#field_demand'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setZabbixIdInput = function (zabbixId) {
        this.zabbixIdInput.sendKeys(zabbixId);
    }

    getZabbixIdInput = function () {
        return this.zabbixIdInput.getAttribute('value');
    }

    setLabelInput = function (label) {
        this.labelInput.sendKeys(label);
    }

    getLabelInput = function () {
        return this.labelInput.getAttribute('value');
    }

    setIpAddressInput = function (ipAddress) {
        this.ipAddressInput.sendKeys(ipAddress);
    }

    getIpAddressInput = function () {
        return this.ipAddressInput.getAttribute('value');
    }

    setSiteIdInput = function (siteId) {
        this.siteIdInput.sendKeys(siteId);
    }

    getSiteIdInput = function () {
        return this.siteIdInput.getAttribute('value');
    }

    setTypeSelect = function (type) {
        this.typeSelect.sendKeys(type);
    }

    getTypeSelect = function () {
        return this.typeSelect.element(by.css('option:checked')).getText();
    }

    typeSelectLastOption = function () {
        this.typeSelect.all(by.tagName('option')).last().click();
    }
    setCodeSnmpInput = function (codeSnmp) {
        this.codeSnmpInput.sendKeys(codeSnmp);
    }

    getCodeSnmpInput = function () {
        return this.codeSnmpInput.getAttribute('value');
    }

    setDateIntegrationInput = function (dateIntegration) {
        this.dateIntegrationInput.sendKeys(dateIntegration);
    }

    getDateIntegrationInput = function () {
        return this.dateIntegrationInput.getAttribute('value');
    }

    demandSelectLastOption = function () {
        this.demandSelect.all(by.tagName('option')).last().click();
    }

    demandSelectOption = function (option) {
        this.demandSelect.sendKeys(option);
    }

    getDemandSelect = function () {
        return this.demandSelect;
    }

    getDemandSelectedOption = function () {
        return this.demandSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
