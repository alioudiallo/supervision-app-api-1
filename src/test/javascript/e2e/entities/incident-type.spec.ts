import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('IncidentType e2e test', () => {

    let navBarPage: NavBarPage;
    let incidentTypeDialogPage: IncidentTypeDialogPage;
    let incidentTypeComponentsPage: IncidentTypeComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load IncidentTypes', () => {
        navBarPage.goToEntity('incident-type');
        incidentTypeComponentsPage = new IncidentTypeComponentsPage();
        expect(incidentTypeComponentsPage.getTitle()).toMatch(/Incident Types/);

    });

    it('should load create IncidentType dialog', () => {
        incidentTypeComponentsPage.clickOnCreateButton();
        incidentTypeDialogPage = new IncidentTypeDialogPage();
        expect(incidentTypeDialogPage.getModalTitle()).toMatch(/Create or edit a Incident Type/);
        incidentTypeDialogPage.close();
    });

    it('should create and save IncidentTypes', () => {
        incidentTypeComponentsPage.clickOnCreateButton();
        incidentTypeDialogPage.setKeyInput('key');
        expect(incidentTypeDialogPage.getKeyInput()).toMatch('key');
        incidentTypeDialogPage.setLabelInput('label');
        expect(incidentTypeDialogPage.getLabelInput()).toMatch('label');
        incidentTypeDialogPage.save();
        expect(incidentTypeDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class IncidentTypeComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('custom-incident-type div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class IncidentTypeDialogPage {
    modalTitle = element(by.css('h4#myIncidentTypeLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    keyInput = element(by.css('input#field_key'));
    labelInput = element(by.css('input#field_label'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setKeyInput = function (key) {
        this.keyInput.sendKeys(key);
    }

    getKeyInput = function () {
        return this.keyInput.getAttribute('value');
    }

    setLabelInput = function (label) {
        this.labelInput.sendKeys(label);
    }

    getLabelInput = function () {
        return this.labelInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
