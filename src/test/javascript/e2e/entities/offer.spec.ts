import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Offer e2e test', () => {

    let navBarPage: NavBarPage;
    let offerDialogPage: OfferDialogPage;
    let offerComponentsPage: OfferComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Offers', () => {
        navBarPage.goToEntity('offer');
        offerComponentsPage = new OfferComponentsPage();
        expect(offerComponentsPage.getTitle()).toMatch(/Offers/);

    });

    it('should load create Offer dialog', () => {
        offerComponentsPage.clickOnCreateButton();
        offerDialogPage = new OfferDialogPage();
        expect(offerDialogPage.getModalTitle()).toMatch(/Create or edit a Offer/);
        offerDialogPage.close();
    });

    it('should create and save Offers', () => {
        offerComponentsPage.clickOnCreateButton();
        offerDialogPage.setLabelInput('label');
        expect(offerDialogPage.getLabelInput()).toMatch('label');
        offerDialogPage.setMaxHostInput('5');
        expect(offerDialogPage.getMaxHostInput()).toMatch('5');
        offerDialogPage.typeSelectLastOption();
        offerDialogPage.save();
        expect(offerDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class OfferComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('custom-offer div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class OfferDialogPage {
    modalTitle = element(by.css('h4#myOfferLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    labelInput = element(by.css('input#field_label'));
    maxHostInput = element(by.css('input#field_maxHost'));
    typeSelect = element(by.css('select#field_type'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setLabelInput = function (label) {
        this.labelInput.sendKeys(label);
    }

    getLabelInput = function () {
        return this.labelInput.getAttribute('value');
    }

    setMaxHostInput = function (maxHost) {
        this.maxHostInput.sendKeys(maxHost);
    }

    getMaxHostInput = function () {
        return this.maxHostInput.getAttribute('value');
    }

    setTypeSelect = function (type) {
        this.typeSelect.sendKeys(type);
    }

    getTypeSelect = function () {
        return this.typeSelect.element(by.css('option:checked')).getText();
    }

    typeSelectLastOption = function () {
        this.typeSelect.all(by.tagName('option')).last().click();
    }
    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
