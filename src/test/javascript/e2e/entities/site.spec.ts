import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Site e2e test', () => {

    let navBarPage: NavBarPage;
    let siteDialogPage: SiteDialogPage;
    let siteComponentsPage: SiteComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Sites', () => {
        navBarPage.goToEntity('site');
        siteComponentsPage = new SiteComponentsPage();
        expect(siteComponentsPage.getTitle()).toMatch(/Sites/);

    });

    it('should load create Site dialog', () => {
        siteComponentsPage.clickOnCreateButton();
        siteDialogPage = new SiteDialogPage();
        expect(siteDialogPage.getModalTitle()).toMatch(/Create or edit a Site/);
        siteDialogPage.close();
    });

    it('should create and save Sites', () => {
        siteComponentsPage.clickOnCreateButton();
        siteDialogPage.setZabbixProxyIdInput('zabbixProxyId');
        expect(siteDialogPage.getZabbixProxyIdInput()).toMatch('zabbixProxyId');
        siteDialogPage.setLabelInput('label');
        expect(siteDialogPage.getLabelInput()).toMatch('label');
        siteDialogPage.setAddressInput('address');
        expect(siteDialogPage.getAddressInput()).toMatch('address');
        siteDialogPage.setLatitudeInput('latitude');
        expect(siteDialogPage.getLatitudeInput()).toMatch('latitude');
        siteDialogPage.setLongitudeInput('longitude');
        expect(siteDialogPage.getLongitudeInput()).toMatch('longitude');
        siteDialogPage.companySelectLastOption();
        siteDialogPage.save();
        expect(siteDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class SiteComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('custom-site div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class SiteDialogPage {
    modalTitle = element(by.css('h4#mySiteLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    zabbixProxyIdInput = element(by.css('input#field_zabbixProxyId'));
    labelInput = element(by.css('input#field_label'));
    addressInput = element(by.css('input#field_address'));
    latitudeInput = element(by.css('input#field_latitude'));
    longitudeInput = element(by.css('input#field_longitude'));
    companySelect = element(by.css('select#field_company'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setZabbixProxyIdInput = function (zabbixProxyId) {
        this.zabbixProxyIdInput.sendKeys(zabbixProxyId);
    }

    getZabbixProxyIdInput = function () {
        return this.zabbixProxyIdInput.getAttribute('value');
    }

    setLabelInput = function (label) {
        this.labelInput.sendKeys(label);
    }

    getLabelInput = function () {
        return this.labelInput.getAttribute('value');
    }

    setAddressInput = function (address) {
        this.addressInput.sendKeys(address);
    }

    getAddressInput = function () {
        return this.addressInput.getAttribute('value');
    }

    setLatitudeInput = function (latitude) {
        this.latitudeInput.sendKeys(latitude);
    }

    getLatitudeInput = function () {
        return this.latitudeInput.getAttribute('value');
    }

    setLongitudeInput = function (longitude) {
        this.longitudeInput.sendKeys(longitude);
    }

    getLongitudeInput = function () {
        return this.longitudeInput.getAttribute('value');
    }

    companySelectLastOption = function () {
        this.companySelect.all(by.tagName('option')).last().click();
    }

    companySelectOption = function (option) {
        this.companySelect.sendKeys(option);
    }

    getCompanySelect = function () {
        return this.companySelect;
    }

    getCompanySelectedOption = function () {
        return this.companySelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
