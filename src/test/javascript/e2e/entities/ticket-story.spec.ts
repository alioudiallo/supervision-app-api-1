import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('TicketStory e2e test', () => {

    let navBarPage: NavBarPage;
    let ticketStoryDialogPage: TicketStoryDialogPage;
    let ticketStoryComponentsPage: TicketStoryComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load TicketStories', () => {
        navBarPage.goToEntity('ticket-story');
        ticketStoryComponentsPage = new TicketStoryComponentsPage();
        expect(ticketStoryComponentsPage.getTitle()).toMatch(/Ticket Stories/);

    });

    it('should load create TicketStory dialog', () => {
        ticketStoryComponentsPage.clickOnCreateButton();
        ticketStoryDialogPage = new TicketStoryDialogPage();
        expect(ticketStoryDialogPage.getModalTitle()).toMatch(/Create or edit a Ticket Story/);
        ticketStoryDialogPage.close();
    });

    it('should create and save TicketStories', () => {
        ticketStoryComponentsPage.clickOnCreateButton();
        ticketStoryDialogPage.setDateInput(12310020012301);
        expect(ticketStoryDialogPage.getDateInput()).toMatch('2001-12-31T02:30');
        ticketStoryDialogPage.statusSelectLastOption();
        ticketStoryDialogPage.ticketSelectLastOption();
        ticketStoryDialogPage.adminSelectLastOption();
        ticketStoryDialogPage.save();
        expect(ticketStoryDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class TicketStoryComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('custom-ticket-story div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class TicketStoryDialogPage {
    modalTitle = element(by.css('h4#myTicketStoryLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    dateInput = element(by.css('input#field_date'));
    statusSelect = element(by.css('select#field_status'));
    ticketSelect = element(by.css('select#field_ticket'));
    adminSelect = element(by.css('select#field_admin'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setDateInput = function (date) {
        this.dateInput.sendKeys(date);
    }

    getDateInput = function () {
        return this.dateInput.getAttribute('value');
    }

    setStatusSelect = function (status) {
        this.statusSelect.sendKeys(status);
    }

    getStatusSelect = function () {
        return this.statusSelect.element(by.css('option:checked')).getText();
    }

    statusSelectLastOption = function () {
        this.statusSelect.all(by.tagName('option')).last().click();
    }
    ticketSelectLastOption = function () {
        this.ticketSelect.all(by.tagName('option')).last().click();
    }

    ticketSelectOption = function (option) {
        this.ticketSelect.sendKeys(option);
    }

    getTicketSelect = function () {
        return this.ticketSelect;
    }

    getTicketSelectedOption = function () {
        return this.ticketSelect.element(by.css('option:checked')).getText();
    }

    adminSelectLastOption = function () {
        this.adminSelect.all(by.tagName('option')).last().click();
    }

    adminSelectOption = function (option) {
        this.adminSelect.sendKeys(option);
    }

    getAdminSelect = function () {
        return this.adminSelect;
    }

    getAdminSelectedOption = function () {
        return this.adminSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
