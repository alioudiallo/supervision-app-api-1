import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Ticket e2e test', () => {

    let navBarPage: NavBarPage;
    let ticketDialogPage: TicketDialogPage;
    let ticketComponentsPage: TicketComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Tickets', () => {
        navBarPage.goToEntity('ticket');
        ticketComponentsPage = new TicketComponentsPage();
        expect(ticketComponentsPage.getTitle()).toMatch(/Tickets/);

    });

    it('should load create Ticket dialog', () => {
        ticketComponentsPage.clickOnCreateButton();
        ticketDialogPage = new TicketDialogPage();
        expect(ticketDialogPage.getModalTitle()).toMatch(/Create or edit a Ticket/);
        ticketDialogPage.close();
    });

    it('should create and save Tickets', () => {
        ticketComponentsPage.clickOnCreateButton();
        ticketDialogPage.setIncidentTypeInput('incidentType');
        expect(ticketDialogPage.getIncidentTypeInput()).toMatch('incidentType');
        ticketDialogPage.setIncidentDateInput(12310020012301);
        expect(ticketDialogPage.getIncidentDateInput()).toMatch('2001-12-31T02:30');
        ticketDialogPage.setTrackIdZabbixInput('trackIdZabbix');
        expect(ticketDialogPage.getTrackIdZabbixInput()).toMatch('trackIdZabbix');
        ticketDialogPage.setHostIdzabbixInput('hostIdzabbix');
        expect(ticketDialogPage.getHostIdzabbixInput()).toMatch('hostIdzabbix');
        ticketDialogPage.setDescriptionInput('description');
        expect(ticketDialogPage.getDescriptionInput()).toMatch('description');
        ticketDialogPage.statusSelectLastOption();
        ticketDialogPage.setCloseDateInput(12310020012301);
        expect(ticketDialogPage.getCloseDateInput()).toMatch('2001-12-31T02:30');
        ticketDialogPage.severitySelectLastOption();
        ticketDialogPage.setCreatedDateInput(12310020012301);
        expect(ticketDialogPage.getCreatedDateInput()).toMatch('2001-12-31T02:30');
        ticketDialogPage.setModifiedDateInput(12310020012301);
        expect(ticketDialogPage.getModifiedDateInput()).toMatch('2001-12-31T02:30');
        ticketDialogPage.adminSelectLastOption();
        ticketDialogPage.companySelectLastOption();
        ticketDialogPage.save();
        expect(ticketDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class TicketComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('custom-ticket div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class TicketDialogPage {
    modalTitle = element(by.css('h4#myTicketLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    incidentTypeInput = element(by.css('input#field_incidentType'));
    incidentDateInput = element(by.css('input#field_incidentDate'));
    trackIdZabbixInput = element(by.css('input#field_trackIdZabbix'));
    hostIdzabbixInput = element(by.css('input#field_hostIdzabbix'));
    descriptionInput = element(by.css('textarea#field_description'));
    statusSelect = element(by.css('select#field_status'));
    closeDateInput = element(by.css('input#field_closeDate'));
    severitySelect = element(by.css('select#field_severity'));
    createdDateInput = element(by.css('input#field_createdDate'));
    modifiedDateInput = element(by.css('input#field_modifiedDate'));
    adminSelect = element(by.css('select#field_admin'));
    companySelect = element(by.css('select#field_company'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setIncidentTypeInput = function (incidentType) {
        this.incidentTypeInput.sendKeys(incidentType);
    }

    getIncidentTypeInput = function () {
        return this.incidentTypeInput.getAttribute('value');
    }

    setIncidentDateInput = function (incidentDate) {
        this.incidentDateInput.sendKeys(incidentDate);
    }

    getIncidentDateInput = function () {
        return this.incidentDateInput.getAttribute('value');
    }

    setTrackIdZabbixInput = function (trackIdZabbix) {
        this.trackIdZabbixInput.sendKeys(trackIdZabbix);
    }

    getTrackIdZabbixInput = function () {
        return this.trackIdZabbixInput.getAttribute('value');
    }

    setHostIdzabbixInput = function (hostIdzabbix) {
        this.hostIdzabbixInput.sendKeys(hostIdzabbix);
    }

    getHostIdzabbixInput = function () {
        return this.hostIdzabbixInput.getAttribute('value');
    }

    setDescriptionInput = function (description) {
        this.descriptionInput.sendKeys(description);
    }

    getDescriptionInput = function () {
        return this.descriptionInput.getAttribute('value');
    }

    setStatusSelect = function (status) {
        this.statusSelect.sendKeys(status);
    }

    getStatusSelect = function () {
        return this.statusSelect.element(by.css('option:checked')).getText();
    }

    statusSelectLastOption = function () {
        this.statusSelect.all(by.tagName('option')).last().click();
    }
    setCloseDateInput = function (closeDate) {
        this.closeDateInput.sendKeys(closeDate);
    }

    getCloseDateInput = function () {
        return this.closeDateInput.getAttribute('value');
    }

    setSeveritySelect = function (severity) {
        this.severitySelect.sendKeys(severity);
    }

    getSeveritySelect = function () {
        return this.severitySelect.element(by.css('option:checked')).getText();
    }

    severitySelectLastOption = function () {
        this.severitySelect.all(by.tagName('option')).last().click();
    }
    setCreatedDateInput = function (createdDate) {
        this.createdDateInput.sendKeys(createdDate);
    }

    getCreatedDateInput = function () {
        return this.createdDateInput.getAttribute('value');
    }

    setModifiedDateInput = function (modifiedDate) {
        this.modifiedDateInput.sendKeys(modifiedDate);
    }

    getModifiedDateInput = function () {
        return this.modifiedDateInput.getAttribute('value');
    }

    adminSelectLastOption = function () {
        this.adminSelect.all(by.tagName('option')).last().click();
    }

    adminSelectOption = function (option) {
        this.adminSelect.sendKeys(option);
    }

    getAdminSelect = function () {
        return this.adminSelect;
    }

    getAdminSelectedOption = function () {
        return this.adminSelect.element(by.css('option:checked')).getText();
    }

    companySelectLastOption = function () {
        this.companySelect.all(by.tagName('option')).last().click();
    }

    companySelectOption = function (option) {
        this.companySelect.sendKeys(option);
    }

    getCompanySelect = function () {
        return this.companySelect;
    }

    getCompanySelectedOption = function () {
        return this.companySelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
