/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { SupervisionapiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { DashboardItemDetailComponent } from '../../../../../../main/webapp/app/entities/dashboard-item/dashboard-item-detail.component';
import { DashboardItemService } from '../../../../../../main/webapp/app/entities/dashboard-item/dashboard-item.service';
import { DashboardItem } from '../../../../../../main/webapp/app/entities/dashboard-item/dashboard-item.model';

describe('Component Tests', () => {

    describe('DashboardItem Management Detail Component', () => {
        let comp: DashboardItemDetailComponent;
        let fixture: ComponentFixture<DashboardItemDetailComponent>;
        let service: DashboardItemService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SupervisionapiTestModule],
                declarations: [DashboardItemDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    DashboardItemService,
                    JhiEventManager
                ]
            }).overrideTemplate(DashboardItemDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DashboardItemDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DashboardItemService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new DashboardItem(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.dashboardItem).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
