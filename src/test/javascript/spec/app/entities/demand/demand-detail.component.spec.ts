/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { SupervisionapiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { DemandDetailComponent } from '../../../../../../main/webapp/app/entities/demand/demand-detail.component';
import { DemandService } from '../../../../../../main/webapp/app/entities/demand/demand.service';
import { Demand } from '../../../../../../main/webapp/app/entities/demand/demand.model';

describe('Component Tests', () => {

    describe('Demand Management Detail Component', () => {
        let comp: DemandDetailComponent;
        let fixture: ComponentFixture<DemandDetailComponent>;
        let service: DemandService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SupervisionapiTestModule],
                declarations: [DemandDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    DemandService,
                    JhiEventManager
                ]
            }).overrideTemplate(DemandDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DemandDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DemandService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Demand(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.demand).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
