/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { SupervisionapiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { HostContractDetailComponent } from '../../../../../../main/webapp/app/entities/host-contract/host-contract-detail.component';
import { HostContractService } from '../../../../../../main/webapp/app/entities/host-contract/host-contract.service';
import { HostContract } from '../../../../../../main/webapp/app/entities/host-contract/host-contract.model';

describe('Component Tests', () => {

    describe('HostContract Management Detail Component', () => {
        let comp: HostContractDetailComponent;
        let fixture: ComponentFixture<HostContractDetailComponent>;
        let service: HostContractService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SupervisionapiTestModule],
                declarations: [HostContractDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    HostContractService,
                    JhiEventManager
                ]
            }).overrideTemplate(HostContractDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(HostContractDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(HostContractService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new HostContract(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.hostContract).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
