/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { SupervisionapiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { HostDemandDetailComponent } from '../../../../../../main/webapp/app/entities/host-demand/host-demand-detail.component';
import { HostDemandService } from '../../../../../../main/webapp/app/entities/host-demand/host-demand.service';
import { HostDemand } from '../../../../../../main/webapp/app/entities/host-demand/host-demand.model';

describe('Component Tests', () => {

    describe('HostDemand Management Detail Component', () => {
        let comp: HostDemandDetailComponent;
        let fixture: ComponentFixture<HostDemandDetailComponent>;
        let service: HostDemandService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SupervisionapiTestModule],
                declarations: [HostDemandDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    HostDemandService,
                    JhiEventManager
                ]
            }).overrideTemplate(HostDemandDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(HostDemandDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(HostDemandService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new HostDemand(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.hostDemand).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
