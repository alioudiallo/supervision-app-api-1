/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { SupervisionapiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { HostTypeDetailComponent } from '../../../../../../main/webapp/app/entities/host-type/host-type-detail.component';
import { HostTypeService } from '../../../../../../main/webapp/app/entities/host-type/host-type.service';
import { HostType } from '../../../../../../main/webapp/app/entities/host-type/host-type.model';

describe('Component Tests', () => {

    describe('HostType Management Detail Component', () => {
        let comp: HostTypeDetailComponent;
        let fixture: ComponentFixture<HostTypeDetailComponent>;
        let service: HostTypeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SupervisionapiTestModule],
                declarations: [HostTypeDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    HostTypeService,
                    JhiEventManager
                ]
            }).overrideTemplate(HostTypeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(HostTypeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(HostTypeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new HostType(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.hostType).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
