/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { SupervisionapiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { IncidentTypeDetailComponent } from '../../../../../../main/webapp/app/entities/incident-type/incident-type-detail.component';
import { IncidentTypeService } from '../../../../../../main/webapp/app/entities/incident-type/incident-type.service';
import { IncidentType } from '../../../../../../main/webapp/app/entities/incident-type/incident-type.model';

describe('Component Tests', () => {

    describe('IncidentType Management Detail Component', () => {
        let comp: IncidentTypeDetailComponent;
        let fixture: ComponentFixture<IncidentTypeDetailComponent>;
        let service: IncidentTypeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SupervisionapiTestModule],
                declarations: [IncidentTypeDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    IncidentTypeService,
                    JhiEventManager
                ]
            }).overrideTemplate(IncidentTypeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(IncidentTypeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(IncidentTypeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new IncidentType(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.incidentType).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
