/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { SupervisionapiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { TicketMessageDetailComponent } from '../../../../../../main/webapp/app/entities/ticket-message/ticket-message-detail.component';
import { TicketMessageService } from '../../../../../../main/webapp/app/entities/ticket-message/ticket-message.service';
import { TicketMessage } from '../../../../../../main/webapp/app/entities/ticket-message/ticket-message.model';

describe('Component Tests', () => {

    describe('TicketMessage Management Detail Component', () => {
        let comp: TicketMessageDetailComponent;
        let fixture: ComponentFixture<TicketMessageDetailComponent>;
        let service: TicketMessageService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SupervisionapiTestModule],
                declarations: [TicketMessageDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    TicketMessageService,
                    JhiEventManager
                ]
            }).overrideTemplate(TicketMessageDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TicketMessageDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TicketMessageService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new TicketMessage(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.ticketMessage).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
