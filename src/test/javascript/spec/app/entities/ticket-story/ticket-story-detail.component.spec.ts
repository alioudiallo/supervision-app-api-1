/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { SupervisionapiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { TicketStoryDetailComponent } from '../../../../../../main/webapp/app/entities/ticket-story/ticket-story-detail.component';
import { TicketStoryService } from '../../../../../../main/webapp/app/entities/ticket-story/ticket-story.service';
import { TicketStory } from '../../../../../../main/webapp/app/entities/ticket-story/ticket-story.model';

describe('Component Tests', () => {

    describe('TicketStory Management Detail Component', () => {
        let comp: TicketStoryDetailComponent;
        let fixture: ComponentFixture<TicketStoryDetailComponent>;
        let service: TicketStoryService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SupervisionapiTestModule],
                declarations: [TicketStoryDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    TicketStoryService,
                    JhiEventManager
                ]
            }).overrideTemplate(TicketStoryDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TicketStoryDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TicketStoryService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new TicketStory(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.ticketStory).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
